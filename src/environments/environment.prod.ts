export const environment = {
  production: true,
  apiUrl: 'http://oficina.netaccede.com:48080/Construdata21',
  api: {
    login: '/rest/publico/loginHacer.json',
    logout: '/rest/privado/logout',
    idiomas: '/rest/comun/selects/selectIdiomas.json',
    cadenas: '/rest/publico/todosTextos.json',
    userData: '/rest/cliente/sesion/ver.json',
    common: {
      divisionEstatal1: '/rest/comun/selects/selectDivisionesEstatalesNivel1.json',
      divisionEstatal2: '/rest/comun/selects/selectDivisionesEstatalesNivel2.json'
    },
    folders: {
      addWork: '/rest/cliente/carpeta/agregarElementos.json',
      verCarpeta: '/rest/cliente/carpeta/ver.json',
      guardarCarpeta: '/rest/cliente/carpeta/guardar.json',
      eliminarCarpeta: '/rest/cliente/carpeta/eliminar.json',
      deleteWorkFromFolder: '/rest/cliente/carpeta/eliminarElementos.json',
      list: '/rest/cliente/carpeta/listado.json',
      listWorks: '/rest/cliente/carpeta/listadosObras.json',
      lastWorks: '/rest/cliente/carpeta/listadoUltimasObras.json',
      selectLastWorks: '/rest/cliente/carpeta/selectUltimasObras.json',
      exportExcel: '/rest/cliente/carpeta/excelGestiones.action',
      exportPDF: '/rest/cliente/carpeta/pdfGestiones.action'
    },
    alertas: {
      home: {
        selectDestacadas: '/rest/cliente/alertas/selectDestacadas.json',
        destacadas: '/rest/cliente/alertas/destacadas.json'
      },
      myAlerts: {
        selectListado: '/rest/cliente/alertas/selectListado.json',
        listado: '/rest/cliente/alertas/listado.json',
        ver: '/rest/cliente/alertas/ver.json',
        verParaBuscador: '/rest/cliente/alertas/verBuscador.json',
        delete: '/rest/cliente/alertas/eliminar.json'
      },
      assistant: {
        frecuencia: '/rest/cliente/alertas/selectFrecuencia.json',
        tipoEnvioPerfil: '/rest/cliente/alertas/selectTipoEnvio.json',
        selectActividad: '/rest/cliente/alertas/selectQueTeDedicas.json',
        selectMercado: '/rest/comun/selects/selectMercado.json',
        selectPais: '/rest/cliente/alertas/selectPaisesAsistente.json',
        selectTipoObra: '/rest/comun/selects/selectTiposGrupoObra.json',

        crearAlerta: '/rest/cliente/alertas/guardar.json'
      },
      send: {
        selectUltimasAlertasEnviadas: '/rest/cliente/alertas/selectUltimasAlertasEnviadas.json',
        ultimasAlertasEnviadas: '/rest/cliente/alertas/ultimasAlertasEnviadas.json',
        reenviarAlerta: '/rest/cliente/alertas/reenviarAlerta.json',
        listadoObras: '/rest/cliente/alertas/listadoObras.json',
        download: '/rest/cliente/alertas/descargaInforme.action'
      },
      myFiles: {
        selectListadoArchivos: '/rest/cliente/alertas/selectListadoArchivos.json',
        listadoArchivos: '/rest/cliente/alertas/listadoArchivos.json',
        enlaceDescargaFichero: '/rest/cliente/alertas/download.json',
      }
    },
    obra: {
      // search: '/rest/cliente/obras/listado.json',
      search: '/rest/cliente/buscador/listado.json',
      searchByProvider: '/rest/cliente/buscador/listadoHistorialObrasProveedor.json',
      filtro: {
        mercado: '/rest/comun/selects/selectMercado.json',
        paises: '/rest/comun/selects/selectDivisionesEstatalesPais.json',
        localidades: '/rest/comun/selects/selectLocalidades.json',
        calles: '/rest/comun/selects/selectCalles.json',
        fases: '/rest/comun/selects/selectTiposHito.json',
        tipoGrupoObra: '/rest/comun/selects/selectTiposGrupoObra.json',
        tipoObra: '/rest/comun/selects/selectTiposObra.json',
        tipoProveedor: '/rest/comun/selects/selectTiposProveedor.json',
        tipoTrabajoObra: '/rest/comun/selects/selectTiposTrabajoObra.json',
        superficieRango: '/rest/comun/selects/selectSuperficieObraRango.json',
        rangosPresupuesto: '/rest/comun/selects/selectRangosPresupuesto.json ',
        faseObras: '/rest/comun/selects/selectTiposGrupoHito.json',
        faseLicitaciones: '/rest/cliente/buscador/selectFasesLicitacion.json',
        otrosPublicacionPerfilEstado: '/rest/cliente/buscador/selectEstados.json',
        otrosTipoTrabajoObra: '/rest/comun/selects/selectTiposTrabajoObra.json',
        selectRangosSuperficieTerreno: '/rest/comun/selects/selectRangosSuperficieTerreno.json',
        selectRangosSuperficieConstruccion: '/rest/comun/selects/selectRangosSuperficieConstruccion.json',
        selectPalabrasClave: '/rest/cliente/buscador/selectPalabrasClave.json',
        codigoConOSin: '/rest/cliente/buscador/selectConOSin.json',
        divisionEstatal: '/rest/comun/selects/selectDivisionesEstatalesMatriz.json',
        tipoGrupoObraExcluir: '/rest/comun/selects/selectTiposGrupoObraExcluir.json',
        tiposPublicaPrivada: '/rest/comun/selects/selectTiposPublicaPrivada.json',
        tiposLicitacion: '/rest/comun/selects/selectTiposLicitacion.json',
        tiposClaseTrabajo: '/rest/comun/selects/selectTiposClaseTrabajo.json'

      },
      worksheet: {
        selectIdiomas: '/rest/comun/selects/selectIdiomas.json',
        ver: '/rest/cliente/elemento/ver.json',
        traducir: '/rest/cliente/elemento/traducir.json',
        guardarError: '/rest/cliente/elemento/error/guardar.json',
        solicitarActualizacion: '/rest/cliente/elemento/solicitarActualizacion.json',
        comprar: '/rest/cliente/elemento/comprar.json',
        generarPdf: '/rest/cliente/elemento/generarPdf.json',
        generarRTF: '/rest/cliente/elemento/generarRtf.json',
        guardarComentario: '/rest/cliente/elemento/comentario/guardar.json',
        eliminarComentario: '/rest/cliente/elemento/comentario/eliminar.json',
        miEmpresaParticipa: '/rest/cliente/elemento/solicitarParticipacion.json'
      }

    },
    schedule: {
      selectTiposAgenda: '/rest/cliente/agenda/selectTiposAgenda.json',
      selectListadoAgenda: '/rest/cliente/agenda/selectListadoAgenda.json',
      scheduleList: '/rest/cliente/agenda/listado.json',
      exportList: '/rest/cliente/agenda/exportarListado.json',
      delete: '/rest/cliente/agenda/eliminar.json',
      save: '/rest/cliente/agenda/guardar.json',
    },
    faq: {
      list: '/rest/cliente/faq/listado.json'
    },
    webshop: {
      selectTipoProducto: '/rest/cliente/webshop/selectTipoProducto.json',
      listadoProductos: '/rest/cliente/webshop/listadoProductos.json',
      verProducto: '/rest/cliente/webshop/verProducto.json',
      selectFormasPago: '/rest/cliente/webshop/selectFormasPago.json',
      verCesta: '/rest/cliente/webshop/cesta/ver.json',
      addProduct: '/rest/cliente/webshop/cesta/agregarProducto.json',
      removeProduct: '/rest/cliente/webshop/cesta/eliminarProducto.json',
      changePayMethod: '/rest/cliente/webshop/cesta/cambiarFormaPago.json',
      clearCesta: '/rest/cliente/webshop/cesta/limpiar.json',
      finalizar: '/rest/cliente/webshop/cesta/finalizar.json',
      help: '/rest/cliente/webshop/ayuda/ver.action',
      helpAsk: '/rest/cliente/webshop/ayuda/solicitar.action',
      selectProvincias: '/rest/cliente/webshop/selectProvincias.action',
      selectRegiones: '/rest/cliente/webshop/selectRegiones.action',
      textoCondiciones: '/rest/publico/textoCondiciones.action',
      textoPrivacidad: '/rest/publico/textoPrivacidad.action'
    },
    account: {
      ver: '/rest/cliente/cuenta/ver.json',
      guardarUsuario: '/rest/cliente/cuenta/guardarUsuario.json',
      empresaVer: '/rest/cliente/cuenta/empresa/ver.json',
      empresaSave: '/rest/cliente/cuenta/empresa/guardar.json',
      invoicesList: '/rest/cliente/cuenta/factura/listado.json',
      invoiceDownload: '/rest/cliente/cuenta/factura/download.json',
      productsList: '/rest/cliente/cuenta/producto/listado.json',
      consumoList: '/rest/cliente/cuenta/producto/consumo.json',
      userList: '/rest/cliente/cuenta/listado.json',
      elimiarUsuario: '/rest/cliente/cuenta/bajaUsuario.json',
      rolSelect: '/rest/cliente/cuenta/selectRol.json',
      actividadComerciales: '/rest/cliente/home/actividadComerciales.json',
      comentarios: '/rest/cliente/home/comentarios.json',
      estadisticas: '/rest/cliente/home/estadisticas.json'
    },
    notifications: {
      notificationsSelect: '/rest/cliente/notificacion/selectNotificaciones.json',
      listado: '/rest/cliente/notificacion/listado.json',
      count: '/rest/cliente/notificacion/contador.json'

    },
    home: {
      select: '/rest/cliente/home/selectActividad.json',
      activity: '/rest/cliente/home/actividadUsuario.json'
    },
    business: {
      verEmpresa: '/rest/cliente/construcontact/ver.action',
      save: '/rest/cliente/construcontact/guardar.action',
      selectIdioma: '/rest/cliente/construcontact/selectIdioma.action',
      saveIdioma: '/rest/cliente/construcontact/guardarTraduccion.action',
      selectTipoProveedor: '/rest/cliente/construcontact/selectTipoProveedor.action',
      logoSave: '/rest/cliente/construcontact/guardarLogo.action',
      logoEliminar: '/rest/cliente/construcontact/eliminarLogo.action',
      imagenListado: '/rest/cliente/construcontact/imagenListado.action',
      imagenBorrar: '/rest/cliente/construcontact/imagenBorrar.action',
      imagenGuardar: '/rest/cliente/construcontact/imagenGuardar.action',
      resend: '/rest/cliente/construcontact/enviarEmail.action'
    },
    contact: {
      info: '/rest/cliente/contacto/ver.action',
      send: '/rest/cliente/contacto/guardar.action'
    }
  }
};
