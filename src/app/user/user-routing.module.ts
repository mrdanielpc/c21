import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Parent route
import { UserComponent } from './user.component';
// Children routes
import { UserDataComponent } from './user-data/user-data.component';
import { UserRouteAccessService } from '../shared/auth';
import { UserProductsComponent } from './user-products/user-products.component';
import { UserInvoicesComponent } from './user-invoices/user-invoices.component';
import { UserUsageComponent } from './user-usage/user-usage.component';
import { UserUsersComponent } from './user-users/user-users.component';

const userRoutes: Routes = [
    {
        path: 'user',
        component: UserComponent,
        canActivate: [UserRouteAccessService],
        children: [
            { path: '', redirectTo: 'user-data', pathMatch: 'full' },
            { path: 'user-data', component: UserDataComponent },
            { path: 'user-products', component: UserProductsComponent },
            { path: 'user-usage', component: UserUsageComponent },
            { path: 'user-invoices', component: UserInvoicesComponent },
            { path: 'user-users', component: UserUsersComponent },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(userRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class UserRoutingModule { }
