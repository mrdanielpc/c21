import { Component, OnInit } from '@angular/core';
import { Main } from '../shared/auth';
import { IMapPermisos } from '../shared/models/generic.model';

@Component({
  selector: 'c21-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  mapPermisos: IMapPermisos;
  constructor(
    main: Main
  ) {
    main.identity().then((ident) => {
      this.mapPermisos = ident.permisos.mapPermisos;
    });
  }
  ngOnInit() {

  }

}
