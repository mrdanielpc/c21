import { Component, OnInit } from '@angular/core';
import { Main } from '../../shared/auth';
import { AccountService } from '../../shared/services/account.service';
import { IProductosUsuario } from '../../shared/models/account.model';

@Component({
  selector: 'c21-user-products',
  templateUrl: './user-products.component.html',
  styleUrls: ['./user-products.component.scss']
})
export class UserProductsComponent implements OnInit {
  columnas = ['nombreProducto', 'fechaInicio', 'fechaFin', 'precioString', 'estado'];
  data: IProductosUsuario[];
  lo;
  constructor(
    private accountService: AccountService,
    main: Main,
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  ngOnInit() {
    this.accountService.getProducts().then((dataRes) => {
      this.data = dataRes;
    });
  }

}
