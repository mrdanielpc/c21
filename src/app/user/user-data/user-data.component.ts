import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ICompany, IUserAccount } from '../../shared/models/account.model';
import { AccountService } from '../../shared/services/account.service';
import { UserBusinessComponent } from '../user-business/user-business.component';
import { Main } from '../../shared/auth';
import { IUser, ILanguage } from '../../shared/models/generic.model';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { WorksheetService } from '../../shared/services/worksheet.service';

@Component({
  selector: 'c21-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent implements OnInit {

  company: ICompany;
  @ViewChild('c21UserBusiness') c21UserBusiness: UserBusinessComponent;
  @ViewChild('myForm') myForm: NgForm;
  @ViewChild('myFormUser') myFormUser: NgForm;
  user: IUser;
  usuario: IUserAccount;
  languages: ILanguage[];
  userPreviousLanguage: ILanguage;
  constructor(
    private accountService: AccountService,
    private ref: ChangeDetectorRef,
    private main: Main,
    private translate: TranslateService,
    private message: MessageService,
    private worksheetService: WorksheetService

  ) { }

  ngOnInit() {
    this.main.identity().then((account) => {
      this.user = account;
      if (this.user.permisos.mapPermisos.puedeVerEmpresa) {
        this.accountService.getCompany().then((data) => {
          this.company = data;
          this.ref.detectChanges();
          this.c21UserBusiness.getAfterBusiness();
        });
      }

      this.accountService.getUser().then((userAcc) => {
        this.usuario = userAcc;
        this.usuario.emailNuevo = userAcc.email;
        this.worksheetService.getSelectLanguages().then((dataLang) => {
          this.languages = dataLang;
          this.usuario.idioma = this.languages.find(
            (idio) => idio.codigo === this.usuario.idioma.codigo);
        });
        this.userPreviousLanguage = this.usuario.idioma;
      });

    });
  }
  saveUserData() {
    if (this.myFormUser.form.invalid) {
      this.message.add({
        severity: 'warn',
        summary: this.translate.instant('account.notification.user.invalid.summary'),
        detail: this.translate.instant('account.notification.user.invalid.detail')
      });
    } else {
      if (this.checkPasswords()) {
        this.accountService.saveUser(this.usuario).then((bool) => {
          if (bool) {
            this.message.add({
              severity: 'success',
              summary: this.translate.instant('account.notification.user.saveSuccess.summary'),
              detail: this.translate.instant('account.notification.user.saveSuccess.detail')
            });
            if (this.usuario.idioma !== this.userPreviousLanguage) {
              this.translate.use(this.usuario.idioma.prefijoLocale);
              this.userPreviousLanguage = this.usuario.idioma;
              window.location.reload(true);
              // No me está devolviendo el api al hacer el refresh correctamente el prefijoLocale
              // Esto se hace para borrar las llamadas de observables que devuelven solo una vez el valor del API y no se vuelven a consultar (Ejemplo: selectRol) .
            }
          }
        });
      } else {
        this.message.add({
          severity: 'warn',
          summary: this.translate.instant('account.notification.user.verifyPassword.summary'),
          detail: this.translate.instant('account.notification.user.verifyPassword.detail')
        });
      }
    }
  }
  checkPasswords(): boolean {
    return (this.usuario.password === this.usuario.confirmacionPassword);
  }
  saveCompanyData() {
    if (this.myForm.form.invalid) {
      this.message.add({
        severity: 'warn',
        summary: this.translate.instant('account.notification.company.invalid.summary'),
        detail: this.translate.instant('account.notification.company.invalid.detail')
      });
    } else {
      this.accountService.saveCompany(this.company).then(() => {
        this.message.add({
          severity: 'success',
          summary: this.translate.instant('account.notification.company.saveSuccess.summary'),
          detail: this.translate.instant('account.notification.company.saveSuccess.detail')
        });

      });
    }
  }

}
