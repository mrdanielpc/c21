import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { IUserAccount } from '../../shared/models/account.model';
import { WorksheetService } from '../../shared/services/worksheet.service';
import { IDropdown, ILanguage } from '../../shared/models/generic.model';
import { AccountService } from '../../shared/services/account.service';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { Main } from '../../shared/auth';
import { ConfirmationService } from 'primeng/primeng';
import { Router } from '@angular/router';

export enum IRoles {
  Gestor = 102,
  Comercial = 103
}
@Component({
  selector: 'c21-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {

  @Input() usuario: IUserAccount;
  @ViewChild('myFormUser') myFormUser: NgForm;
  @Output() changed: EventEmitter<void> = new EventEmitter<void>();

  languages: ILanguage[];
  status: IDropdown[];
  rols: IDropdown[];
  usuarioActivo: IDropdown;
  lo;
  header: string;
  permisos: any;
  collapsed = false;

  constructor(
    private accountService: AccountService,
    private confirmationService: ConfirmationService,
    private worksheetService: WorksheetService,
    private translate: TranslateService,
    private message: MessageService,
    private router: Router,
    main: Main
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });

    this.permisos = [
      {
        codes: [IRoles.Gestor],
        nombre: this.translate.instant('account.user.roles.search')
      },
      {
        codes: [IRoles.Gestor],
        nombre: this.translate.instant('account.user.roles.alerts')
      },
      {
        codes: [IRoles.Gestor, IRoles.Comercial],
        nombre: this.translate.instant('account.user.roles.folders')
      },
      {
        codes: [IRoles.Gestor, IRoles.Comercial],
        nombre: this.translate.instant('account.user.roles.agenda')
      },
      {
        codes: [],
        nombre: this.translate.instant('account.user.roles.webshop')
      },
      {
        codes: [],
        nombre: this.translate.instant('account.user.roles.ficha')
      },
      {
        codes: [IRoles.Gestor],
        nombre: this.translate.instant('account.user.roles.administration')
      },
      {
        codes: [],
        nombre: this.translate.instant('account.user.roles.facturacion')
      }
    ];
  }

  ngOnInit() {
    if (!this.usuario) {
      this.header = this.translate.instant('account.user.newUser');
    } else {
      const ape = this.usuario.apellidos ? this.usuario.apellidos : '';
      this.header = `${this.usuario.nombre} ${ape}`;
    }
    this.accountService.getRoles().then((roles) => {
      this.rols = roles;
      if (!!this.usuario && !!this.usuario.rol) {
        this.usuario.rol = this.rols.find(
          (rol) => rol.codigo === this.usuario.rol.codigo);
      }
    });
    this.accountService.getStatus().then((statusList) => {
      this.status = statusList;
      if (!!this.usuario) {
        this.usuarioActivo = this.status.find(
          (st) => st.codigo === this.usuario.activo);
      }
    });
    this.worksheetService.getSelectLanguages().then((dataLang) => {
      this.languages = dataLang;
      if (!!this.usuario && !!this.usuario.idioma) {
        this.usuario.idioma = this.languages.find(
          (idio) => idio.codigo === this.usuario.idioma.codigo);
      }
    });

  }
  userStatusChange() {
    this.usuario.activo = <boolean>this.usuarioActivo.codigo;
  }

  saveUser() {
    if (this.myFormUser.form.invalid) {
      this.message.add({
        severity: 'warn',
        summary: this.translate.instant('account.notification.user.invalid.summary'),
        detail: this.translate.instant('account.notification.user.invalid.detail')
      });
    } else {
      if (this.checkPasswords()) {
        this.accountService.saveUser(this.usuario).then((bool) => {
          if (bool) {
            this.message.add({
              severity: 'success',
              summary: this.translate.instant('account.notification.user.saveSuccess.summary'),
              detail: this.translate.instant('account.notification.user.saveSuccess.detail')
            });
            this.changed.emit();
          }
        });
      } else {
        this.message.add({
          severity: 'warn',
          summary: this.translate.instant('account.notification.user.verifyPassword.summary'),
          detail: this.translate.instant('account.notification.user.verifyPassword.detail')
        });
      }
    }
  }
  checkPasswords(): boolean {
    return (this.usuario.password === this.usuario.confirmacionPassword);
  }
  redirectToFolder(code: number) {
    this.router.navigate(['folders', code]);
  }
  delete() {
    this.confirmationService.confirm({
      header: this.translate.instant('account.user.deleteUser.header'),
      message: this.translate.instant('account.user.deleteUser.message'),
      icon: 'fa fa-trash',
      accept: () => {
        this.accountService.deleteUser(this.usuario.codigo).then(() => {
          this.message.add({
            severity: 'success',
            summary: this.translate.instant('account.notification.user.deleteSuccess.summary'),
            detail: this.translate.instant('account.notification.user.deleteSuccess.detail')
          });
          this.changed.emit();
        });
      }
    });
  }
}
