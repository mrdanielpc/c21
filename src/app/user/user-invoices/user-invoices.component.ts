import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { IFactura } from '../../shared/models/account.model';
import { Main } from '../../shared/auth';

@Component({
  selector: 'c21-user-invoices',
  templateUrl: './user-invoices.component.html',
  styleUrls: ['./user-invoices.component.scss']
})
export class UserInvoicesComponent implements OnInit {
  columnas = ['numeroFactura', 'fechaEmision', 'formaPagoString', 'totalString', 'cobroPendienteString', 'download'];
  data: IFactura[];
  lo;
  constructor(
    private accountService: AccountService,
    main: Main,
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  ngOnInit() {

    this.accountService.getInvoices().then((dataRes) => {
      this.data = dataRes;
    });
  }

  downloadInvoice(code: number) {
    this.accountService.downloadInvoice(code).then(() => {
      // this.data = dataRes;
    });
  }

}
