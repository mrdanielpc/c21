import { Component, OnInit } from '@angular/core';
import { Main } from '../../shared/auth';
import { AccountService } from '../../shared/services/account.service';
import { IDropdown } from '../../shared/models/generic.model';
import { IUserAccount } from '../../shared/models/account.model';

@Component({
  selector: 'c21-user-users',
  templateUrl: './user-users.component.html',
  styleUrls: ['./user-users.component.scss']
})
export class UserUsersComponent implements OnInit {

  lo: any;
  rolesList: IDropdown[];
  userList: IUserAccount[] = [];
  userNew: IUserAccount;
  constructor(
    private accountService: AccountService,
    main: Main,
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  ngOnInit() {
    this.accountService.getRoles().then((roles) => {
      this.rolesList = roles;
    });
    this.changeUserList();
  }
  createNewUser() {
    this.userNew = {};
  }

  cancelNewUser() {
    this.userNew = undefined;
  }

  changeUserList() {
    this.accountService.getUsers().then((users) => {
      this.userList = users;
      this.userList.forEach((user) => {
        user.emailNuevo = user.email;
      });
    });
  }
  changeUserNew() {
    this.userNew = undefined;
    this.changeUserList();
  }
}
