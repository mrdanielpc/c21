import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { Main } from '../../shared/auth';
import { ICompra } from '../../shared/models/account.model';

@Component({
  selector: 'c21-user-usage',
  templateUrl: './user-usage.component.html',
  styleUrls: ['./user-usage.component.scss']
})
export class UserUsageComponent implements OnInit {
  columnas = ['nombreProducto', 'fechaInicio', 'fechaFin', 'numero'];
  columnasCompra = ['referencia', 'nombre', 'fechaCompra'];
  data: ICompra[];
  lo;
  constructor(
    private accountService: AccountService,
    main: Main,
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  ngOnInit() {
    this.accountService.getUsage().then((dataRes) => {
      this.data = dataRes;
    });
  }
}
