import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Forms
// import { FormsModule } from '@angular/forms';

// Import  Component
import { SharedModule } from '../../shared/modules/shared.module';
import { UserBusinessComponent } from './user-business.component';

@NgModule({
    imports: [
        // FormsModule,
        CommonModule,
        SharedModule
    ],
    declarations: [
        UserBusinessComponent
    ],
    providers: [
    ],
    exports: [
        UserBusinessComponent
    ]
})
export class UserBusinessModule { }
