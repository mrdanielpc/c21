import { Component, OnInit, Input } from '@angular/core';
import { ICompany } from '../../shared/models/account.model';
import { IDropdown } from '../../shared/models/generic.model';
import { CommonService } from '../../shared/services/common.service';
import { NgForm, ControlContainer } from '@angular/forms';

@Component({
  selector: 'c21-user-business',
  templateUrl: './user-business.component.html',
  styleUrls: ['./user-business.component.scss'],
  viewProviders: [{
    provide: ControlContainer,
    useExisting: NgForm
  }]
})
export class UserBusinessComponent implements OnInit {
  @Input() company: ICompany;
  @Input() faxVisible = false;
  division1List: IDropdown[] = [];
  division2List: IDropdown[] = [];

  countryList: IDropdown[];

  constructor(
    private commonService: CommonService
  ) { }

  ngOnInit() {
  }

  public getAfterBusiness() {

    this.commonService.getCountries().then((countries) => {
      this.countryList = countries;
      if (!!this.company) {
        if (!!this.company.divisionEstatalPais) {
          this.company.divisionEstatalPais = countries.find((coun) => coun.codigo === this.company.divisionEstatalPais.codigo);
          if (!!this.company.divisionEstatalPais) {
            this.getDivision1(+this.company.divisionEstatalPais.codigo).then((dataDiv1) => {
              if (!!this.company.divisionEstatalNivel1) {
                this.company.divisionEstatalNivel1 = dataDiv1.find((coun) => coun.codigo === this.company.divisionEstatalNivel1.codigo);
                if (!!this.company.divisionEstatalNivel1) {
                  this.getDivision2(+this.company.divisionEstatalNivel1.codigo).then((dataDiv2) => {
                    if (!!this.company.divisionEstatalNivel2) {
                      this.company.divisionEstatalNivel2 = dataDiv2.find((coun) => coun.codigo === this.company.divisionEstatalNivel2.codigo);
                    }
                  });
                }
              }

            });
          }
        }
      }
    });

  }

  getDivision1(codigo: number) {
    const division1$ = this.commonService.getCountriesDivision1(codigo);
    division1$.then((data) => {
      this.division1List = data;
    });
    return division1$;
  }
  getDivision2(codigo: number) {
    const division2$ = this.commonService.getCountriesDivision2(codigo);
    division2$.then((data) => {
      this.division2List = data;
    });
    return division2$;
  }
  onCountryChange() {
    if (!!this.company.divisionEstatalPais) {
      this.getDivision1(+this.company.divisionEstatalPais.codigo).then(() => {
        this.division2List = [];
        this.company.divisionEstatalNivel2 = null;
        this.company.divisionEstatalNivel1 = null;
      });
    }
  }

  onDivision1Change() {
    if (!!this.company.divisionEstatalNivel1) {
      this.getDivision2(+this.company.divisionEstatalNivel1.codigo).then(() => {
        this.company.divisionEstatalNivel2 = null;
      });
    }
  }
}
