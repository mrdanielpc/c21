import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Frames
import { SharedModule } from '../shared/modules/shared.module';

// Forms
import { FormsModule } from '@angular/forms';

// Import Services
import { CountriesService } from '../shared/services/countries.service';

// Import  Component
import { UserComponent } from './user.component';
import { UserDataComponent } from './user-data/user-data.component';
import { UserRoutingModule } from './user-routing.module';
import { UserProductsComponent } from './user-products/user-products.component';
import { UserBusinessModule } from './user-business/user-business.module';
import { UserInvoicesComponent } from './user-invoices/user-invoices.component';
import { UserUsageComponent } from './user-usage/user-usage.component';
import { UserUsersComponent } from './user-users/user-users.component';
import { UserAccountComponent } from './user-account/user-account.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    UserRoutingModule,
    UserBusinessModule
  ],
  declarations: [
    UserComponent,
    UserDataComponent,
    UserProductsComponent,
    UserInvoicesComponent,
    UserUsageComponent,
    UserUsersComponent,
    UserAccountComponent,
  ],
  providers: [
    CountriesService,
  ]
})
export class UserModule { }
