import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/modules/index';
import { CreateNewFolderComponent } from './create-new-folder.component';
@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    CreateNewFolderComponent
  ],
  exports: [
    CreateNewFolderComponent
  ]
})
export class CreateNewFolderModule { }
