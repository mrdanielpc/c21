import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';

import { FolderService } from '../../shared/services/folder.service';
import { IFolderModal, IFolders } from '../../shared/models/folder.model';

@Component({
  selector: 'c21-create-new-folder',
  templateUrl: './create-new-folder.component.html',
  styleUrls: ['./create-new-folder.component.scss']
})
export class CreateNewFolderComponent implements OnInit {
  @Input() folder: IFolderModal = {};
  @Output() displayNone: EventEmitter<number> = new EventEmitter<number>();

  parentFolder: IFolders = {};
  translation: { modalTitle?: string, buttonSubmit?: string } = {};

  constructor(
    private folderService: FolderService,
    private messageService: MessageService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.initTranslation();
  }

  initFolder(): IFolderModal {
    const folder: IFolderModal = {};
    folder.visible = false;
    folder.carpeta = {
      codigo: null,
      nombre: null,
      avisoEmail: false
    };
    return folder;
  }

  initTranslation() {
    if (!this.folder.carpeta.codigo) {
      if (!this.folder.carpeta.codigoCarpetaPadre) {
        this.translation.modalTitle = this.translate.instant('folders.createNewFolder.modal.title');
        this.translation.buttonSubmit = this.translate.instant('folders.createNewFolder.modal.buttonSubmit');
      } else {
        this.translation.modalTitle = this.translate.instant('folders.createSubFolder.modal.title');
        this.translation.buttonSubmit = this.translate.instant('folders.createNewFolder.modal.buttonSubmit');
      }
    } else {
      this.translation.modalTitle = this.translate.instant('folders.editFolder.modal.title');
      this.translation.buttonSubmit = this.translate.instant('folders.createNewFolder.modal.buttonSubmit');
    }

    this.dialogShow();

  }

  submitClick() {
    this.saveFolder().then(() => this.dialogClose());
  }

  saveFolder(): Promise<void> {
    return this.folderService
      .saveFolder(this.folder.carpeta)
      .then(() => {
        if (!this.folder.carpeta.codigo) {
          this.messageService
            .add({
              severity: 'success',
              summary: this.translate.instant('folders.createNewFolder.notifications.saveSuccess.summary'),
              detail: this.translate.instant('folders.createNewFolder.notifications.saveSuccess.detail')
            });
        } else {
          this.messageService
            .add({
              severity: 'success',
              summary: this.translate.instant('folders.editFolder.notifications.saveSuccess.summary'),
              detail: this.translate.instant('folders.editFolder.notifications.saveSuccess.detail')
            });
        }
      });
  }

  saveSubFolder() {
    this.folderService
      .saveFolder(this.folder.carpeta)
      .then(() => {
        this.messageService
          .add({
            severity: 'success',
            summary: this.translate.instant('folders.createSubFolder.notifications.saveSuccess.summary'),
            detail: this.translate.instant('folders.createSubFolder.notifications.saveSuccess.detail')
          });
      });
  }

  editFolder() {
    this.messageService
      .add({
        severity: 'success',
        summary: this.translate.instant('folders.editFolder.notifications.saveSuccess.summary'),
        detail: this.translate.instant('folders.editFolder.notifications.saveSuccess.detail')
      });
  }

  dialogShow() {
    this.folder.visible = true;
  }

  dialogClose() {
    this.folder.visible = false;
    this.displayNone.emit(this.folder.carpeta.codigo);
  }

}
