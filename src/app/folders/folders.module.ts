import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/modules/shared.module';
import { VisibleTableColumnsModule } from '../shared/modals/visible-table-columns/visible-table-columns.module';

import { FoldersComponent } from './folders.component';
import { FolderService } from '../shared/services/folder.service';

import { MiniPaginatorModule } from '../alerts/mini-search-viewer/mini-paginator/mini-paginator.module';
import { CreateNewFolderModule } from './create-new-folder/create-new-folder.module';
import { WorksToFolderModule } from './works-to-folder/works-to-folder.module';
import { FolderExportComponent } from './folder-export/folder-export.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        MiniPaginatorModule,
        CreateNewFolderModule,
        WorksToFolderModule,
        VisibleTableColumnsModule
    ],
    declarations: [
        FoldersComponent,
        FolderExportComponent
    ],
    providers: [
        FolderService
    ],
    exports: [
        FoldersComponent
    ]
})
export class FoldersModule { }
