import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderExportComponent } from './folder-export.component';

describe('FolderExportComponent', () => {
  let component: FolderExportComponent;
  let fixture: ComponentFixture<FolderExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
