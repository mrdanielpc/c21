import { Component, OnInit, Input } from '@angular/core';
import { IFolderModal } from '../../shared/models/folder.model';
import { FolderService } from '../../shared/services/folder.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'c21-folder-export',
  templateUrl: './folder-export.component.html',
  styleUrls: ['./folder-export.component.scss']
})
export class FolderExportComponent implements OnInit {
  @Input() folderModal: IFolderModal;
  @Input() lo;
  dateStart: Date;
  dateEnd: Date;
  constructor(
    private folderService: FolderService,
    private messageService: MessageService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
  }
  generatePdf() {
    this.folderService.exportPDF(this.folderModal.carpeta.codigo, this.dateStart, this.dateEnd).then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('comun.notifications.downloaded.summary'),
        detail: this.translate.instant('comun.notifications.downloaded.detail')
      });
      this.folderModal.visible = false;
    });

  }
  generateXls() {
    this.folderService.exportXLS(this.folderModal.carpeta.codigo, this.dateStart, this.dateEnd).then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('comun.notifications.downloaded.summary'),
        detail: this.translate.instant('comun.notifications.downloaded.detail')
      });
      this.folderModal.visible = false;
    });
  }
  clear() {
    this.dateEnd = undefined;
    this.dateStart = undefined;
  }

}
