import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { FoldersComponent } from './folders.component';
import { UserRouteAccessService } from '../shared/auth';

@Injectable()
export class FoldersParams implements Resolve<any> {

    constructor() { }

    resolve(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot) {
        const param = Object.assign({}, route.queryParams);
        param['page'] = param['page'] ? param['page'] : '1';
        return param;
    }
}

const searchRoutes: Routes = [
    {
        path: 'folders',
        component: FoldersComponent,
        /*  children: [ // rutas hijas, se verán dentro del componente padre
            { path: '', redirectTo: '', pathMatch: 'full' }
          ]*/
        resolve: { 'filters': FoldersParams },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'folders/:folder', component: FoldersComponent,
        canActivate: [UserRouteAccessService],
        resolve: { 'filters': FoldersParams }
    },
];

@NgModule({
    providers: [FoldersParams],
    imports: [RouterModule.forChild(searchRoutes)],
    exports: [RouterModule]
})
export class FoldersRoutingModule { }
