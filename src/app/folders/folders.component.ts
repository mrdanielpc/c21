import { Component, OnInit } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';

import { FolderService } from '../shared/services/folder.service';
import { IFolders, IWorksToFolder, IFolderModal } from '../shared/models/folder.model';
import { VisibleTableColumnsModel } from '../shared/models/visible-table-columns.model';
import { IWorkColumns, IPaginator } from '../shared/models/search-work.model';
import { ConfirmationService } from 'primeng/primeng';
import { SvsEventManager } from '../shared/event-manager/event-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Main } from '../shared/auth';
import { SessionStorageService } from 'ngx-webstorage';
import { IDropdown } from '../shared/models/generic.model';

@Component({
  selector: 'c21-folders',
  templateUrl: './folders.component.html',
  styleUrls: ['./folders.component.scss']
})
export class FoldersComponent implements OnInit {
  folderList: IFolders[];
  constructionProvidersElemento: any;
  folderActual: IFolders;
  paginator: IPaginator = {};
  currentViewMosaic = false;
  dialogExport = false;
  checkedAll = false;
  dialogExportFolder: IFolderModal = { visible: false };

  keyword: string;
  txtSearchChanged: Subject<string> = new Subject<string>();
  mosaicSortedBy: { field: string, order: number } = { field: '', order: 0 };
  worksSelection: [{
    codigo?: number,
    carpeta?: IFolders,
    tipoElemento?: { codigo?: number }
  }];
  // Table Columns
  sortingColumn: IWorkColumns;
  sortingDirectionCombo: IDropdown;
  sortingList: IDropdown[];
  columnsVisibleTable: IWorkColumns[];
  visibleTableColumnsData: VisibleTableColumnsModel.IIncoming = {
    display: false,
    cookieName: 'foldersVisibleColumns',
    availableColumns: [],
    defaultVisibleColumns: []
  };
  createNewFolderModal: IFolderModal = {
    visible: false
  };

  worksTofolderModal: IWorksToFolder = {
    display: false
  };
  lo: any;

  promiseFolder$: Promise<void>;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private folderService: FolderService,
    private messageService: MessageService,
    private translate: TranslateService,
    private confirmationService: ConfirmationService,
    private eventManager: SvsEventManager,
    main: Main,
    private sessionStorage: SessionStorageService
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
    this.txtSearchChanged
      .debounceTime(300) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe((model) => {
        this.keyword = model;
        this.getFolderWorks();
      });
    this.sortingList = [{
      codigo: 1,
      nombre: this.translate.instant('search.mosaic.order.up')
    }, {
      codigo: 2,
      nombre: this.translate.instant('search.mosaic.order.down')
    }];
  }

  ngOnInit() {
    this.getTableVisibleColumns();
    // this.columnsVisibleTable = this.folderService.getAllColumns();
    this.getFolders();
    this.route.data.subscribe((data) => {
      const params = data['filters'];
      if (!!params) {
        this.paginator.sortingColumnCode = params['sortColumn'];
        this.paginator.sortingDirection = params['sortDir'];
        this.paginator.pagina = params['page'];
      }
    });
    this.route.params.subscribe((params) => {
      if (!!params['folder'] && typeof +params['folder'] === 'number') {
        // this.getFolders(+params['folder']);
        this.promiseFolder$.then(() => {
          this.selectFolder(+params['folder']);
          if (!!this.folderActual) {
            this.onChangeFolder();
            this.getFolderWorks(this.paginator.pagina);
          }
        });
      } else {
        this.router.navigate(['/folders', 0]);
      }
    });

    // this.getFolders(this.sessionStorage.retrieve('folderSelected'));
    this.eventManager.subscribe('newFolder', () => {
      this.getFolders(this.folderActual.codigo);
    });
  }

  getFolders(codigo?: number): Promise<void> {
    this.promiseFolder$ = this.folderService.getFolders().then((foldersRes) => {
      this.folderList = foldersRes;
      this.selectFolder(codigo);
      // this.getFolderWorks();
    });
    return this.promiseFolder$;
  }
  selectFolder(codigo = 0) {
    let fold = 0;
    fold = this.folderList.findIndex((folde) => folde.codigo === codigo);
    if (fold === -1) {
      this.router.navigate(['/folders', 0]);
    }
    this.folderActual = this.folderList[fold];
  }
  changeView() {
    this.currentViewMosaic = !this.currentViewMosaic;
  }

  deleteFolder() {
    let msg = 'folders.deleteFolder.warning';
    if (this.folderActual.numeroElementos === 0) {
      msg = 'folders.deleteFolder.confirm';
      //      this.folderService.deleteFolder(this.folderActual.codigo);
    }

    this.confirmationService.confirm({
      header: this.translate.instant('folders.deleteFolder.modal.title'),
      message: this.translate.instant(msg),
      icon: 'fa fa-warning',
      accept: () => {
        this.folderService.deleteFolder(this.folderActual.codigo).then(() => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('folders.deleteFolder.notifications.saveSuccess.summary'),
            detail: this.translate.instant('folders.deleteFolder.notifications.saveSuccess.detail')
          });
          this.getFolders();
        });

      }

    });
  }
  foldersSort(event) {
    // this.paginator.pagina = 1;
    // this.paginator.sortingColumnCode = this.folderService.getColumnCode(event.field);
    // this.paginator.sortingDirection = event.order === 1 ? 1 : 2;
    // this.getFolderWorks(this.paginator.pagina);

    if (!this.currentViewMosaic) {
      this.paginator.sortingColumnCode = this.folderService.getColumnCode(event.field);
      this.paginator.sortingDirection = event.order === 1 ? 1 : 2;

    } else {
      if (this.sortingColumn) {
        this.paginator.sortingColumnCode = this.sortingColumn.code;
        this.paginator.sortingDirection = +this.sortingDirectionCombo.codigo;
      }
    }
    // this.paginator.pagina = 1;
    // this.filtersParams.codigoTipoOrden = this.paginator.sortingColumnCode;
    // this.filtersParams.codigoOrdenAscendenteDescendente = this.paginator.sortingDirection;
    this.getFolderWorks(this.paginator.pagina);
  }
  updateCombosOrder() {
    this.sortingDirectionCombo = this.sortingList.find((srt) => {
      return srt.codigo === this.paginator.sortingDirection;
    });
    this.sortingColumn = this.columnsVisibleTable.find((col) => {
      return col.code === this.paginator.sortingColumnCode;
    });
  }

  deleteWorkFromFolder() {
    if (this.constructionProvidersElemento.filter((elem) => elem.checked).length > 0) {

      this.confirmationService.confirm({
        header: this.translate.instant('folders.notifications.deleteFromWork.summary'),
        message: this.translate.instant('folders.deleteWorkFromFolder'),
        icon: 'fa fa-warning',
        accept: () => {

          const carpeta = {};
          for (const works of this.constructionProvidersElemento.filter((elem) => elem.checked)) {
            if (!carpeta[works.carpeta.codigo]) {
              carpeta[works.carpeta.codigo] = [`${works.tipoElemento.codigo}_${works.codigo}`];
            } else {
              carpeta[works.carpeta.codigo].push(`${works.tipoElemento.codigo}_${works.codigo}`);
            }
          }
          const promises = [];
          for (const fold of Object.keys(carpeta)) {
            promises.push(this.folderService.deleteWorkFromFolder(+fold, carpeta[fold]));
          }
          Promise.all(promises).then(() => {
            this.getFolders(this.folderActual.codigo).then(() => this.getFolderWorks());
            this.worksSelection = null;
            this.messageService.add({
              severity: 'success',
              summary: this.translate.instant('folders.notifications.deleteFromWork.summary'),
              detail: this.translate.instant('folders.notifications.deleteFromWork.detail')
            });
          });
        }
      });
    }
  }

  onChangeFolder() {
    if (this.folderActual.codigo > 0) {
      this.columnsVisibleTable = this.columnsVisibleTable.filter((vi) => vi.attribute !== 'carpeta.nombre');
    } else {
      this.getTableVisibleColumns();
    }
    this.router.navigate(['/folders', this.folderActual.codigo]);
    // this.getFolderWorks();
  }
  onChangeKeywordText(text: string) {
    this.txtSearchChanged.next(text);
  }
  updateUrl() {
    let queryParams;
    queryParams = {
      page: this.paginator.pagina,
      sortColumn: this.paginator.sortingColumnCode,
      sortDir: this.paginator.sortingDirection
    };
    this.router.navigate(['folders', this.folderActual.codigo], {
      queryParams
    });
  }
  getFolderWorks(page = 1) {
    this.paginator.pagina = page;
    this.updateUrl();
    this.folderService.getFolderWorks(this.folderActual.codigo, this.paginator, this.keyword).then((data) => {
      this.sessionStorage.store('folderSelected', this.folderActual.codigo);
      this.constructionProvidersElemento = data.elementos;
      this.paginator = {
        pagina: data.model.pagina,
        numeroPaginas: data.model.numeroPaginas,
        numeroResultados: data.model.numeroResultados,
        sortingColumnCode: data.model.codigoTipoOrden,
        sortingDirection: data.model.codigoOrdenAscendenteDescendente
      };
      this.updateCombosOrder();
    });
  }
  editFolder() {
    this.createNewFolderModal.carpeta = Object.assign({}, this.folderActual);
    this.createNewFolderModal.visible = true;
  }
  //#region "modal"
  displayCreateNewFolder() {
    if (this.folderActual.codigo > 0) {
      this.createNewFolderModal.carpeta = {
        codigoCarpetaPadre: this.folderActual.codigo
      };
    } else {
      this.createNewFolderModal.carpeta = {};
    }
    this.createNewFolderModal.visible = true;
  }
  closeCreateNewFolder() {
    this.getFolders(this.folderActual.codigo);
    this.createNewFolderModal.visible = false;
  }

  showFolderPopup() {
    this.worksTofolderModal.codigosElementosSelecionados = this.folderService.concatenateWorkCodesForFolders(
      this.constructionProvidersElemento.filter((elem) => elem.checked)
    );

    this.worksTofolderModal.display = (this.worksTofolderModal.codigosElementosSelecionados.length > 0);
  }
  closeWorksToFolder() {
    this.getFolders(this.folderActual.codigo);
    this.constructionProvidersElemento.forEach((elem) => elem.checked = false);
  }

  //#endregion "modal"

  //#region "VisibleTableColumns"
  displayVisibleTableColumns(show) {
    if (show) {
      this.visibleTableColumnsData.display = show;
      this.visibleTableColumnsData.availableColumns = this.folderService.getAllColumns();
      this.visibleTableColumnsData.defaultVisibleColumns = this.folderService.getDefaultVisibleColumns();
    } else {
      this.visibleTableColumnsData.display = show;
      this.getTableVisibleColumns();
    }
  }

  redirectToWorksheet(work: any) {
    if (!!work.tieneProductoVisualizacion) {
      this.router.navigate(['work', work.tipoElemento.codigo, work.codigo]);
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('search.notifications.noProduct.summary'),
        detail: this.translate.instant('search.notifications.noProduct.detail')
      });
    }
  }

  redirectToWorksheetWithData(event) {
    const work = event.data;
    if (!this.checkCellSelected(event.originalEvent)) {
      this.redirectToWorksheet(work);
    }
  }
  private checkCellSelected(event): boolean {
    return event.target.parentElement.attributes['data-checkselection'] ||
      event.target.parentElement.parentElement.attributes['data-checkselection'] ||
      event.target.attributes['data-checkselection'] || (event.target.children.length > 0 &&
        (!!event.target.children[0].attributes['data-checkselection'] || event.target.children.length > 1 && event.target.children[1].children.length > 0 &&
          !!event.target.children[1].children[0].attributes['data-checkselection']));
  }

  getTableVisibleColumns() {
    // guardamos las columnas visibles en una Cookie
    if (!Cookie.get(this.visibleTableColumnsData.cookieName) || Cookie.get(this.visibleTableColumnsData.cookieName) === 'undefined') {
      Cookie.set(this.visibleTableColumnsData.cookieName, JSON.stringify(this.folderService.getDefaultVisibleColumns()));
    }
    this.columnsVisibleTable = this.folderService.getTableVisibleColumns(JSON.parse(Cookie.get(this.visibleTableColumnsData.cookieName)));
  }
  //#endregion "VisibleTableColumns"
  openDialogExport() {
    this.dialogExport = true;
  }

  checkAll(event) {
    const boolEvent = event;
    for (const providers of this.constructionProvidersElemento) {
      providers.checked = boolEvent;
    }
    this.checkedAll = boolEvent;
  }
  checked(carValue) {
    let boolValue = true;
    if (carValue.checked) {
      for (const providers of this.constructionProvidersElemento) {

        if (!providers.checked) {
          boolValue = false;
          break;
        }

      }
      this.checkedAll = boolValue;
    } else {
      this.checkedAll = false;
    }
    // event.preventDefault();
    // event.stopPropagation();
  }

  exportFolder() {
    this.dialogExportFolder.carpeta = this.folderActual;
    this.dialogExportFolder.visible = true;
  }

}
