import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorksToFolderComponent } from './works-to-folder.component';

describe('WorksToFolderComponent', () => {
  let component: WorksToFolderComponent;
  let fixture: ComponentFixture<WorksToFolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorksToFolderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorksToFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
