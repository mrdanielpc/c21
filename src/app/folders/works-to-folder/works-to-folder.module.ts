import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/modules/index';
import { WorksToFolderComponent } from './works-to-folder.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
  ],
  declarations: [
    WorksToFolderComponent
  ],
  exports: [
    WorksToFolderComponent
  ]
})
export class WorksToFolderModule { }
