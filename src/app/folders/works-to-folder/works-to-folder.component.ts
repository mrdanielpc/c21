import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { FolderService } from '../../shared/services/folder.service';

import { IFolders, IWorksToFolder } from '../../shared/models/folder.model';

@Component({
  selector: 'c21-works-to-folder',
  templateUrl: './works-to-folder.component.html',
  styleUrls: ['./works-to-folder.component.scss']
})
export class WorksToFolderComponent implements OnInit {
  @Input() worksData: IWorksToFolder;
  @Output() displayNone = new EventEmitter();

  folderList: IFolders[];
  folderSelected: IFolders;
  constructor(
    private folderService: FolderService,
    private messageService: MessageService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.worksData.display = true;
    this.getFolders();
  }

  getFolders() {
    this.folderService.getFolders().then((data) => {
      this.folderList = data.filter((fold) => fold.sePuedeAgregarElemento);
    });
  }

  addWorksToFolder() {
    this.worksData.codigo = this.folderSelected.codigo;

    this.folderService
      .addWorksToFolder(this.worksData.codigo, this.worksData.codigosElementosSelecionados)
      .then(() => {
        this.dialogClose();
        this.messageService
          .add({
            severity: 'success',
            summary: this.translate.instant('folders.notifications.addToFolder.summary'),
            detail: this.translate.instant('folders.notifications.addToFolder.detail')
          });
      });
  }

  dialogClose() {
    this.worksData.display = false;
    this.displayNone.emit(this.worksData.display);
  }

}
