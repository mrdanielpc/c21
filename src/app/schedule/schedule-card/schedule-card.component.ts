import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ISchedule, IScheduleModal } from '../../shared/models/schedule.model';
import { Main } from '../../shared/auth';
import { ScheduleService } from '../../shared/services/schedule.service';
import { ScheduleEditorComponent } from '../../shared/components/schedule-editor/schedule-editor.component';
import { Router } from '@angular/router';

@Component({
  selector: 'c21-schedule-card',
  templateUrl: './schedule-card.component.html',
  styleUrls: ['./schedule-card.component.scss']
})
export class ScheduleCardComponent implements OnInit {

  @Input() public schedule: ISchedule;
  @Output() public onDelete: EventEmitter<number> = new EventEmitter<number>();
  @Output() public onSave: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('c21ScheduleEditor') c21Schedule: ScheduleEditorComponent;
  @Input() public isShortView = false;
  lo: any;
  moreInfo = false;
  showEdit: IScheduleModal = { visible: false };
  constructor(main: Main,
    private router: Router,
    private scheduleService: ScheduleService,

  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  ngOnInit() {
    this.getScheduleTitle().then((data) => this.schedule.title = data);
  }

  toggleMoreInfo() {
    this.moreInfo = !this.moreInfo;
  }
  getScheduleTitle(): Promise<string> {
    return this.scheduleService.getScheduleAction().then((scheduleList) => {
      const IDrop = scheduleList.find((list) => list.codigo === this.schedule.tipoAgenda.codigo);
      let title = '';
      if (!!IDrop) {
        title = IDrop.nombre;
      }
      if (!!this.schedule.contacto) {
        title = `${title}  ${this.schedule.contacto}`;
      } else if (!!this.schedule.telefonos) {
        title = `${title}  ${this.schedule.telefonos}`;
      }
      return title;
    });
  }
  deleteSchedule() {
    this.onDelete.emit(this.schedule.codigo);
  }
  editSchedule() {
    this.showEdit.schedule = Object.assign({}, this.schedule);
    this.showEdit.schedule['fechaAviso2'] = this.showEdit.schedule.fechaAviso;
    this.c21Schedule.selectAction();
    this.showEdit.visible = true;

  }
  onEdit() {
    this.onSave.emit();
  }
  redirectToWork(tipoElemento: number, codigo: number) {
    this.router.navigate(['work', tipoElemento, codigo]);
  }
}
