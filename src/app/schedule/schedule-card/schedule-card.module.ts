import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/modules';
import { ScheduleCardComponent } from './schedule-card.component';
import { ScheduleEditorModule } from '../../shared/components/schedule-editor/schedule-editor.module';

@NgModule({
    imports: [
        // FormsModule,
        CommonModule,
        SharedModule,
        ScheduleEditorModule
    ],
    declarations: [
        ScheduleCardComponent
    ],
    exports: [
        ScheduleCardComponent
    ]
})
export class ScheduleCardModule { }
