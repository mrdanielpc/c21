import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ScheduleService } from '../shared/services/schedule.service';
import { IDropdown, ILocale } from '../shared/models/generic.model';
import { Main } from '../shared/auth';
import { ISchedule } from '../shared/models/schedule.model';
import { ConfirmationService } from 'primeng/primeng';
import * as moment from 'moment';
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'c21-schedule-component',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {
  selectSchedule: IDropdown[];
  selectScheduleSelected: IDropdown;
  scheduleList: ISchedule[];
  lo: ILocale;
  currentMonth: IDropdown;
  currentYear: IDropdown;
  months: IDropdown[] = [];
  years: IDropdown[] = [];
  scheduleCalendar: ISchedule[];
  dateCalendar: Date = new Date();
  dateTooltip = {};

  constructor(
    private scheduleService: ScheduleService,
    private ref: ChangeDetectorRef,
    main: Main,
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    private messageService: MessageService,
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  ngOnInit() {
    this.initCalendar();

    this.getSelectSchedule().then(() => {
      this.getSchedule();
      this.onDateChange();
    });

  }
  initCalendar() {
    const date = new Date();
    // this.currentMonth = date.getMonth();
    // this.currentYear = date.getFullYear();

    for (const val of Object.keys(this.lo.monthNames)) {
      this.months.push(
        {
          codigo: +val,
          nombre: this.lo.monthNames[val]
        }
      );
    }
    this.currentMonth = this.months.find((month) => month.codigo === date.getMonth());
    for (let i = date.getFullYear() + 3; i > date.getFullYear() - 10; i--) {
      this.years.push(
        {
          codigo: i,
          nombre: i.toString()
        }
      );
      this.currentYear = this.years.find((year) => year.codigo === date.getFullYear());
    }

  }
  getSelectSchedule(): Promise<void> {
    return this.scheduleService.getScheduleSelect().then((select) => {
      this.selectSchedule = select;
    });
  }

  getSchedule() {
    const selectedTemp = (!!this.selectScheduleSelected ? +this.selectScheduleSelected.codigo : 0);

    this.scheduleService.getSchedule(selectedTemp).then((data) => {
      this.scheduleList = data.elementos;
      this.ref.detectChanges();
    });
  }
  onChangeselectSchedule() {
    this.getSchedule();
  }
  downloadSchedule() {
    const selectedTemp = (!!this.selectScheduleSelected ? +this.selectScheduleSelected.codigo : 0);

    this.scheduleService.downloadSchedule(selectedTemp);
  }

  onDateChange() {
    this.dateCalendar = new Date(+this.currentYear.codigo, +this.currentMonth.codigo, 1);

    this.scheduleService.getScheduleMonth(+this.currentMonth.codigo + 1, +this.currentYear.codigo).
      then((data) => {
        this.scheduleCalendar = data.elementos;
        this.dateTooltip = {};
        if (!!this.scheduleCalendar) {
          this.scheduleCalendar.forEach((schedule) => {
            this.scheduleService.getScheduleAction().then((action) => {
              const IDrop = action.find((act) => act.codigo === schedule.tipoAgenda.codigo);
              schedule.tooltip = '';
              if (!!IDrop) {
                schedule.tooltip = `<p>${IDrop.nombre}</p>`;

              }
              if (!!schedule.contacto) {
                schedule.tooltip = `${schedule.tooltip}<p>${schedule.contacto}</p>`;
              } else if (!!schedule.telefonos) {
                schedule.tooltip = `${schedule.tooltip}<p>${schedule.telefonos}</p>`;
              } else if (!!schedule.fechaAviso) {
                schedule.tooltip = `${schedule.tooltip}<p>${moment(schedule.fechaAviso).format(this.lo.momentFormatDateWithHour)} </p>`;
              }
              if (!this.dateTooltip[+moment(schedule.fechaAviso).date()]) {
                this.dateTooltip[+moment(schedule.fechaAviso).date()] = schedule.tooltip;
              } else {
                this.dateTooltip[+moment(schedule.fechaAviso).date()] = this.dateTooltip[+moment(schedule.fechaAviso).date()] + '  <br/> ' + schedule.tooltip;
              }
            });
          });
        }
      });
  }

  onEdit() {
    this.getSchedule();
    this.onDateChange();
  }

  onDelete(scheduleCode: number) {
    this.confirmationService.confirm({
      header: this.translate.instant('schedule.card.delete.header'),
      message: this.translate.instant('schedule.card.delete.message'),
      icon: 'fa fa-trash',
      accept: () => {
        this.scheduleService.delete(scheduleCode).then((data) => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('schedule.card.delete.header'),
            detail: data.actionMessages[0]
          });
          this.getSchedule();
          this.onDateChange();
        });
      },

    });
  }

}
