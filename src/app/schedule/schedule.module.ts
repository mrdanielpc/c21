import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/modules/shared.module';

import { ScheduleService } from '../shared/services/schedule.service';
import { ScheduleComponent } from './schedule.component';
import { ScheduleEditorModule } from '../shared/components/schedule-editor/schedule-editor.module';
import { ScheduleEditorComponent } from '../shared/components/schedule-editor/schedule-editor.component';
import { ScheduleCardModule } from './schedule-card/schedule-card.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ScheduleEditorModule,
        ScheduleCardModule,
    ],
    declarations: [
        ScheduleComponent,
    ],
    providers: [
        ScheduleService,
    ],
    exports: [
        ScheduleComponent,
        ScheduleEditorComponent
    ]
})
export class SchedulesModule { }
