import { Pipe, PipeTransform, ChangeDetectorRef, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Injectable()
@Pipe({ name: 'translateLang', pure: false })
export class TranslateLangPipe implements PipeTransform {
    value = '';
    lastKey: string;
    lastLang: string;
    lastGetTrans: Observable<any>;
    constructor(
        public translate: TranslateService,
        private _ref: ChangeDetectorRef
    ) { }

    transform(query: string, lang: string, getTrans: Observable<any>): string {
        if (!query || query.length === 0) {
            return query;
        }
        if (query === this.lastKey && lang === this.lastLang && getTrans === this.lastGetTrans) {
            return this.value;
        }
        this.lastKey = query;
        this.lastLang = lang;
        this.lastGetTrans = getTrans;
        if (!!getTrans) {
            getTrans.subscribe(
                (res) => {
                    let resp = res;
                    for (const k of query.split('.')) {
                        if (!!resp[k]) {
                            resp = resp[k];
                        } else {
                            resp = this.translate.instant(query);
                            break;
                        }
                    }
                    // query.split('.').forEach((asd) => resp = resp[asd]);
                    // resp = query;
                    this.value = resp;
                    this._ref.markForCheck();
                });
        }
        return this.value;
        // this.translate.get(query, interpolateParams).subscribe(onTranslation);

    }

}
