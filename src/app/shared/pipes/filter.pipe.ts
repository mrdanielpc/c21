import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Injectable()
@Pipe({ name: 'filterParentName', pure: false })
export class FilterParentNamePipe implements PipeTransform {
    value = [];
    lastKey = [];
    constructor(
        // private _ref: ChangeDetectorRef
    ) { }

    transform(value: any[]): any[] {
        if (!value || value.length === 0) {
            return value;
        }
        if (value === this.lastKey) {
            return this.value;
        }
        this.lastKey = value;

        this.value = value.map((cd) => cd.parentName).filter((item, i, ar) => ar.indexOf(item) === i);
        // this._ref.markForCheck();

        return this.value;
        // this.translate.get(value, interpolateParams).subscribe(onTranslation);

    }

}
