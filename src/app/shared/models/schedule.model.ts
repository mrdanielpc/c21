import { IModal } from './generic.model';

export interface IObra {
    codigo: number;
    nombre: string;
    referencia: string;
}

export interface ICodigo {
    codigo: number;
}
export interface IEmpresa {
    codigo: number;
    empresa: string;
}

export interface ISchedule {
    avisoEmail?: boolean;
    codigo?: number;
    codigoElemento?: number;
    codigoProveedor?: number;
    codigoTipoElemento?: number;
    contacto?: string;
    descripcion?: string;
    fechaAviso?: Date;
    obra?: IObra;
    proveedor?: IEmpresa;
    telefonos?: string;
    tipoAgenda?: ICodigo;
    title?: string;
    tooltip?: string;
}

export interface IModel {
    codigoPeriodo: number;
    numeroPaginas: number;
    numeroResultados: number;
    pagina: number;
    resultadosPagina: number;
    tiempoConsulta: number;
}

export interface IScheduleObject {
    elementos: ISchedule[];
    model: IModel;
}

export interface IScheduleModal extends IModal {
    schedule?: ISchedule;
}
