
export interface IBudgetRangesSelected {
    selectedFrom: string;
    selectedTo: string;
    zeroBudget: boolean;
}
export declare module BudgetRangesModel {

    export interface Result {
        codigo: number;
        nombre: string;
    }

    export interface RootObject {
        result: Result[];
    }

}
