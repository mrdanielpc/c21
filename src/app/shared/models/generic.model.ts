
export interface IDropdown {
    codigo: number | boolean;
    nombre: string;
}
export interface IDropdownSubtype extends IDropdown {
    subTipos: IDropdown;
}
export interface ICodigoNombre {
    codigo: string;
    nombre: string;
}
export interface ILanguage extends IDropdown {
    prefijoLocale: string;
}
export interface ILocale {
    firstDayOfWeek: number;
    dayNames?: string[];
    dayNamesShort?: string[];
    dayNamesMin?: string[];
    monthNames?: string[];
    monthNamesShort?: string[];
    momentFormatDate?: string;
    momentFormatDateWithHour?: string;
    formatDate?: 'mm/dd/yy' | 'dd/mm/yy';
    formatDateWithHour: string;
    today?: string;
    clear?: string;
}
export interface IUser {
    usuario?: { nombre?: string; idioma?: ILanguage; email?: string; login?: string };
    authorities?: any;
    imageUrl?: any;
    localeData?: ILocale;
    permisos: IPermisos;
}

export interface IModal {
    visible?: boolean;

}

export interface IPermisos {
    codigoCartera: number;
    codigoFichaEmpresa: number;
    creditoRestante: number;
    fichaEmpresaVisible: boolean;
    mapPermisos: IMapPermisos;
    numeroAlertas: number;
    numeroDiasLogin: number;
    obrasRestantes: number;
    registroNuevo: boolean;
    usuarioMoroso: boolean;
    usuarioParticular: boolean;
    usuarioPrincipal: boolean;
}

export interface IMapPermisos {
    puedeVerNotificaciones: boolean;
    puedeGuardarEmpresa: boolean;
    puedeCrearUsuarioGestor: boolean;
    puedeVerUsuario: boolean;
    puedeBuscar: boolean;
    puedeVerEmpresa: boolean;
    puedeGuardarUsuario: boolean;
    puedeGestionarUsuarios: boolean;
    puedeCrearUsuarioComercial: boolean;
    puedeVerArchivos: boolean;
    puedeComprar: boolean;
    puedeVerBloqueAlertas: boolean;
    puedeVerBloqueComerciales: boolean;
    puedeVerCuentaFacturas: boolean;
    puedeVerAlertas: boolean;
    puedeCrearCarpeta: boolean;
    puedeVerProductos: boolean;
    puedeVerConsumo: boolean;
}

export interface IActividadBaseDatos {
    estadisticasConcursosPublicos: IEstadisticas;
    estadisticasTipoHito: IEstadisticas;
    estadisticasTipoObra: IEstadisticas;
    numAdjudicaciones: number;
    numFasesFinales: number;
    numFasesIniciales: number;
    numInstalaciones: number;
    numLicitaciones: number;
    numObrasActualizadas: number;
    numObrasActualizadasIberica: number;
    numObrasActualizadasInternacional: number;
    numObrasNuevas: number;
    numObrasNuevasIberica: number;
    numObrasNuevasInternacional: number;
}

export interface IEstadisticas {
    [propName: string]: number;
}
