export declare module VisibleTableColumnsModel {

    export interface IIncoming {
        display: boolean;
        cookieName: string;
        availableColumns: IColumn[];
        defaultVisibleColumns: Array<string>;
    }

    export interface IColumn {
        code: number;
        attribute: string;
        text: string;
        cssStyle?: object;
    }

}
