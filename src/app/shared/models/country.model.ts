export declare module CountryModel {
    export interface Result {
        codigo: number;
        nombre: string;
        subtipo?: string;
        communities?: any;
    }

    export interface RootObject {
        result: Result[];
    }
}
