export declare module ProvinceModel {
    export interface Result {
        codigo: number;
        nombre: string;
    }

    export interface RootObject {
        result: Result[];
    }

}
