import { IDropdown } from './generic.model';

export declare module WorksheetModel {

    //#region "custom"
    export interface ISubmitParams {
        codigoElemento?: number;
        codigoTipoElemento?: number;
        request_only_locale?: number;
        comentario?: string;
        correoRespuesta?: string;
    }

    export interface IWorkComment {
        codigoElemento?: number; // Identificador de la obra
        codigoTipoElemento?: number;
        comentario?: string;
    }

    //#endregion "custom"

    export interface IWorksheetModel {
        actionErrors: any[];
        actionMessages: any[];
        elemento: IElementoWork;
        fieldErrors: IFieldErrors;
    }

    export interface IFieldErrors {
    }

    export interface IElementoWork {
        boletin?: string;
        codigo: number;
        codigoFase: number;
        descripcionFase?: string;
        direcciones: string[];
        divisionEstatalPais: string;
        duracion?: string;
        etiquetaFechaFase: string;
        fase: string;
        fechaBoletin?: string;
        fechaCreacionObra: string;
        fechaFase: string;
        fechaPlazoPresentacionOfertas?: string;
        licenciaExpediente: string;
        nombre: string;
        observaciones?: string;
        permalink: string;
        presupuesto: string;
        proveedores: IProveedor[];
        referencia: string;
        tipoContratacionOConcurso: string;
        tipoObraOLicitacion: string;
        tipoProteccionViviendaOTramitacion: string;
        tiposObra: string;
        urls: IUrl[];
    }

    export interface IUrl {
        url: string;
        tipoUrl?: string;
    }

    export interface IProveedor {
        agenda?: { tooltip?: string };
        codigo: number;
        contactoEsUrl: boolean;
        nombre: string;
        stringDivisionesEstatales: string;
        stringTiposProveedor: string;
        tipoProveedorEstado: ITipoProveedorEstado;
    }

    export interface ITipoProveedorEstado {
        codigo: number;
    }
    export interface IComentario {
        codigo: number;
        comentario: string;
        fecha: string;
        obra: IDropdown;
    }
}
export interface IComentario {
    autor: string;
    codigo: number;
    codigoTipoElemento: number;
    comentario: string;
    fecha: string;
    obra: IDropdown;
}
