import { IDropdown } from './generic.model';

export interface IPayMethod extends IDropdown {
    esTransferencia: boolean;
}
export interface IWebProduct {
    codigo: number;
    descripcion: string;
    listarPaises: boolean;
    listarProvincias: boolean;
    listarRegiones: boolean;
    listarTiposObra: boolean;
    nombre: string;
    precioBase: number;
    precioBaseString: string;
    productoDinamico: boolean;
    selectedWorkType?: IDropdown;
    selectedCountry?: IDropdown;
    codigoAdd?: number;
    modifiedCombo?: boolean;
}

export interface IBasketProducts {
    codigoProducto: number;
    descripcionProducto: string;
    neto: number;
    netoString: string;
    nombreProducto: string;
}

export interface IBasketModel {
    descuento: number;
    descuentoString: string;
    iva: number;
    ivaString: string;
    productos: IBasketProducts[];
    subTotal: number;
    subTotalString: string;
    total: number;
    totalString: string;
}
