import { IDropdown, ILanguage } from './generic.model';

export interface IUsuario {
    codigo?: number;
    email?: string;
    login?: string;

    apellidos?: string;
    nombre?: string;
    nombreCompleto?: string;
    telefonoContacto?: string;

}

export interface ICompany {
    cif: string;
    cp: string;
    direccion: string;
    divisionEstatalNivel1: IDropdown;
    divisionEstatalNivel2: IDropdown;
    divisionEstatalPais: IDropdown;
    empresa: string;
    telefonosEmpresa: string;
    localidad: string;
    faxEmpresa?: string;
}

export interface IUserAccount {
    activo?: boolean;
    apellidos?: string;
    login?: string;
    codigo?: number;
    email?: string;
    emailNuevo?: string;
    fechaConfirmacionCondiciones?: string;
    fechaConfirmacionCondicionesGenerales?: string;
    fechaConfirmacionComunicacionesComerciales?: string;
    fechaUltimoAcceso?: string;
    idioma?: ILanguage;
    nombre?: string;
    numeroAccesos?: number;
    rol?: IDropdown;
    telefonoUsuario?: string;
    password?: string;
    confirmacionPassword?: string;
    online?: boolean;
    codigoCarpeta?: number;
}

export interface IFactura {
    cobroPendienteString: string;
    codigo: number;
    fechaEmision: string;
    formaPagoString: string;
    numeroFactura: string;
    totalString: string;
}
export interface IProductosUsuario {
    codigo: number;
    estado: string;
    fechaFin: string;
    fechaInicio: string;
    precioString: string;
    nombreProducto: string;
}

export interface IProductoUsage {
    codigo: number;
    compras: ICompra[];
    fechaInicio: string;
    nombreProducto: string;
    numero: number;
    sePuedeDesplegar: boolean;
    fechaFin: string;
}

export interface ICompra {
    codigoElemento: number;
    fechaCompra: string;
    nombre: string;
    referencia: string;

}

export interface IActividadUsuario {
    fechaPenultimoAcceso: string;
    fechaUltimoAcceso: string;
    numAccesosTotales: number;
    numAccesosTotalesPorcentaje: number;
    numAlertasEnviadas: number;
    numAlertasEnviadasPorcentaje: number;
    numComentariosInternos: number;
    numComentariosInternosPorcentaje: number;
    numErroresNotificados: number;
    numErroresNotificadosPorcentaje: number;
    numObrasAgendadasMes: number;
    numObrasAgendadasMesPorcentaje: number;
    numObrasConsultadas: number;
    numObrasConsultadasPorcentaje: number;
    numObrasGuardadasCartera: number;
    numObrasGuardadasCarteraPorcentaje: number;
    numProveedoresConsultados: number;
    numProveedoresConsultadosPorcentaje: number;
    online: boolean;
    usuario: any;
    expanded?: boolean;
}

export interface IMyBusiness extends ICompany {
    codigoTipoProveedor: number;
    emailEmpresa: string;
    faxEmpresa: string;
    estado: string;
    imagenesFicha: IImagen[];
    logoFicha: IImagen;
    personas: IPersona[];
    traduccionesFicha: ITraduccionesFicha[];
    web: string;
    fechaActualizacion: string;
}

export interface IPersona {
    codigo: number;
    emailsString: string;
    nombre: string;
    telefonosString: string;
    faxString: string;
    observaciones: string;
}

export interface ITraduccionesFicha {
    actividad: string;
    codigoIdioma: number;
    productos: string;
}

export interface IImagen {
    codigo: number;
    fechaCreacion: string;
    nombre: string;
    urlDescarga: string;
}
