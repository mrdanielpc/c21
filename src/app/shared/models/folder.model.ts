
import { IModal, IDropdown } from './generic.model';
import { IUsuario } from './account.model';

interface IFolder {
    codigo?: number;
    nombre?: string;
    numeroElementos?: number;
    sePuedeAgregarElemento?: boolean;
    sePuedeEliminar?: boolean;
    codigoCarpetaPadre?: number;
    avisoEmail?: boolean;
}

export interface IFolders extends IFolder {
    tipoCarpeta?: { codigo?: number };
    usuarioDelegado?: IUsuario;
    subCarpetas?: [IFolder];
    nameWithNumbers?: string;
}

/**
 *
 *
 * @export
 * @interface IWorksToFolder
 * codigo: codigo de carpeta
 * codigosElementosSelecionados: concatenación de tipoElemento.codigo + "_" + elemento.codigo
 */
export interface IWorksToFolder {
    display?: boolean;
    codigo?: number;
    codigosElementosSelecionados?: string[];
}

export interface IFolderModal extends IModal {
    carpeta?: IFolders;
}

export interface IFolderWorkHome {
    codigo: number;
    codigoFase: number;
    constructora: string;
    divisionEstatal1: string;
    divisionEstatal2: string;
    divisionEstatalPais: string;
    duracion: string;
    fase: string;
    fechaFase: string;
    nombre: string;
    permalink: string;
    presupuesto: string;
    promotora: string;
    tieneProductoVisualizacion: boolean;
    tipo: string;
    tipoElemento: IDropdown;
    urlImagen: string;
    urlImagenGrande: string;
    arquitecto: string;
    numeroPisos: string;
    numeroViviendas: string;
    superficie: string;
}
