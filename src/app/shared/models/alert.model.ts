// Creacion no automatizada

import { ISearchParams } from './search-work.model';
import { IDropdown } from './generic.model';

interface ItipoEnvioPerfil {
    codigo: number | IDropdown;
}
export interface IAlertlHome {
    codigo: number;
    frecuencia: number;
    nombrePerfil: string;
    notificacion: boolean;
    tipoElemento: { codigo?: number };
}

export class PerfilAlert {
    constructor(
        public codigo?: number,
        public nombrePerfil?: string,
        public emails?: string,
        public frecuencia?: string | IDropdown,
        public frecuenciaNombre?: string,
        public html?: boolean | IDropdown,
        public tipoEnvioPerfil?: ItipoEnvioPerfil,
        public notificacion?: IDropdown,
        public tipoElemento?: { codigo?: number }
    ) {
        if (!tipoEnvioPerfil) {
            this.tipoEnvioPerfil = { codigo: null };
        }
        if (!tipoElemento) {
            this.tipoElemento = { codigo: null };
        }
    }
}

export class Alert {
    constructor(public codigoTipoElementoBusqueda?: number,
        public codigoPerfil?: number,
        public perfil?: PerfilAlert,
        public buscar?: ISearchParams) {
        if (!perfil) {
            this.perfil = new PerfilAlert();
        }
        if (!buscar) {
            this.buscar = {};
        }
    }
}
