export declare module ComunityModel {

    export interface Result {
        codigo: number;
        nombre: string;
    }

    export interface RootObject {
        result: Result[];
    }

}
