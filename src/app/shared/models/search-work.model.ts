export interface IPaginator {
    numeroPaginas?: number;
    numeroResultados?: number;
    pagina?: number;
    resultadosPagina?: number;
    sortingColumnCode?: number;
    sortingDirection?: number;
}

//
// DataTable Colums
export interface IWorkColumns {
    code: number;
    attribute: string;
    text: string;
    cssStyle?: object;
}
//
// Search Params
export interface ISearchParams {
    pagina?: number;
    codigoTipoOrden?: number;
    codigoOrdenAscendenteDescendente?: number;
    presupuestoCero?: number;
    presupuestoMin?: any;
    presupuestoMax?: any;
    codigoMercado?: any;
    codigosPais?: any[];
    codigosDivisionEstatalPrimerNivel?: any[];
    codigosDivisionEstatalSegundoNivel?: any[];
    codigoTipoHitoObra?: any[];
    codigoTipoElementoBusqueda?: any;
    codigoFaseLicitacion?: any[];
    localidad?: any;
    codigoPostal?: any;
    codigoPostalMaximo?: any;
    nombreObra?: any;
    referencia?: any;
    codigoConOSinEmail?: any;
    codigoConOSinContratista?: any;
    codigoConOSinArquitecto?: any;
    codigoConOSinIngeniero?: any;
    codigosDivisionEstatalMatriz?: any;
    codigosTipoProveedor?: any;
    palabrasClave?: string[];
    palabrasClaveExcluir?: string[];
    adjudicatario?: string;
    organismoLicitador?: string;
    codigoActividad?: number;
    codigoTipoObra?: number | number[];

}
//
// for 'minisearchviewer'
export declare module MiniSearchView {

    export interface IMiniSearchView {
        paginator?: IPaginator;
        table?: ITable;
    }

    export interface IPaginator {
        pagina?: number;
        numeroPaginas?: number;
        numeroResultados?: number;
    }
    export interface ITable {
        columnas?: IWorkColumns[];
        elementos?: ISearchParams[];
    }

}
//
//
// http://json2ts.com/
export declare module SearchWorkModel {

    export interface TipoElemento {
        codigo: number;
    }

    export interface Elemento {
        codigo: number;
        constructora: string;
        divisionEstatal1: string;
        divisionEstatal2: string;
        divisionEstatalPais: string;
        fase: string;
        fechaFase: Date;
        nombre: string;
        permalink: string;
        presupuesto: number;
        promotora: string;
        superficie: number;
        tipo: string;
        tipoElemento: TipoElemento;
        urlImagen: string;
        checked: boolean;
        carpeta: any;
    }

    export interface Model {
        codigoBusquedaDefecto: number;
        codigoConOSinArquitecto: number;
        codigoConOSinContratista: number;
        codigoConOSinIngeniero: number;
        codigoFaseLicitacion: number;
        codigoFechaPlazoPresentacionOferta: number;
        codigoFiltroPerfilEstado: number;
        codigoFiltroPerfilPeriodo: number;
        codigoMercado: number;
        codigoPerfil: number;
        codigoRangoSuperficieConstruccion: number;
        codigoRangoSuperficieTerreno: number;
        codigoTipoClaseTrabajo: number;
        codigoTipoElementoBusqueda: number;
        codigoTipoHitoObra: any[];
        codigoTipoLicitacion: number;
        codigoTipoObraExcluir: any[];
        codigoTipoObra: number;
        codigoTipoTrabajoObra: number;
        codigosPais: number[];
        codigosTipoProveedor: any[];
        incluirClasificacionNoRequerida: boolean;
        numeroPaginas: number;
        numeroPlantasMaximo: number;
        numeroPlantasMinimo: number;
        numeroResultados: number;
        numeroViviendasMaximo: number;
        numeroViviendasMinimo: number;
        pagina: number;
        palabrasClaveExcluir: any[];
        presupuestoCero: boolean;
        presupuestoMax: number;
        presupuestoMin: number;
        resultadosPagina: number;
        tiempoConsulta: number;
    }

    export interface RootObject {
        actionErrors: any[];
        actionMessages: any[];
        elementos: Elemento[];
        fieldErrors: {};
        model: Model;
    }

}
