import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/modules/index';
import { VisibleTableColumnsComponent } from './visible-table-columns.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    VisibleTableColumnsComponent
  ],
  exports: [
    VisibleTableColumnsComponent
  ]
})
export class VisibleTableColumnsModule { }
