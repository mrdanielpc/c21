import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibleTableColumnsComponent } from './visible-table-columns.component';

describe('VisibleTableColumnsComponent', () => {
  let component: VisibleTableColumnsComponent;
  let fixture: ComponentFixture<VisibleTableColumnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisibleTableColumnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibleTableColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
