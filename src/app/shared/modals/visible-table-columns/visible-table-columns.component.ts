import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { VisibleTableColumnsModel } from '../../models/visible-table-columns.model';

@Component({
  selector: 'c21-visible-table-columns',
  templateUrl: './visible-table-columns.component.html',
  styleUrls: ['./visible-table-columns.component.scss']
})
export class VisibleTableColumnsComponent implements OnInit, AfterViewChecked {
  @Input() incoming: VisibleTableColumnsModel.IIncoming;
  @Output() save: EventEmitter<boolean> = new EventEmitter<boolean>();
  columnsUserConfig: any[];

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }
  constructor(private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.getUserVisibleColumns();
    this.dialogShow();
  }

  //#region "columnas"
  setDefaultVisibleColumns() {
    // guardamos las columnas visibles por defecto en una Cookie

    this.columnsUserConfig = this.incoming.defaultVisibleColumns;

    // this.dialogClose();
  }

  getUserVisibleColumns() {
    if (!Cookie.get(this.incoming.cookieName) || Cookie.get(this.incoming.cookieName) === 'undefined') {
      // aquí no debería entrar, la cookie debe estar creada antes de llamar a esta ventana
      // guardamos las columnas visibles por defecto en la Cookie
      Cookie.set(this.incoming.cookieName, JSON.stringify(this.incoming.defaultVisibleColumns));
    }
    this.columnsUserConfig = JSON.parse(Cookie.get(this.incoming.cookieName));
  }

  setUserVisibleColumns() {
    Cookie.set(this.incoming.cookieName, JSON.stringify(this.columnsUserConfig));
  }
  //#endregion "columnas"

  //#region "modal"
  dialogShow() {
    this.incoming.display = true;
  }
  saveColumns() {
    this.setUserVisibleColumns();
    this.save.emit(false);
    this.dialogClose();
  }

  dialogClose() {
    this.incoming.display = false;
  }
  //#endregion "modal"
}
