import { Observable } from 'rxjs';
import { RequestOptionsArgs, Response } from '@angular/http';
import { SvsHttpInterceptor } from './svshttp.interceptor';

export class CacheInterceptor extends SvsHttpInterceptor {

    constructor(
    ) {
        super();
    }

    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        options.headers.append('Cache-Control', 'no-cache');
        options.headers.append('Pragma', 'no-cache');
        options.headers.append('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT');
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        return observable; // by pass
    }

}
