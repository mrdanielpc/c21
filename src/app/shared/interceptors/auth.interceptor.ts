import { Observable } from 'rxjs';
import { RequestOptionsArgs, Response } from '@angular/http';
import { LocalStorageService } from 'ngx-webstorage';
import { SvsHttpInterceptor } from './svshttp.interceptor';
import { Cookie } from 'ng2-cookies/ng2-cookies';

export class AuthInterceptor extends SvsHttpInterceptor {

    constructor(
        private localStorage: LocalStorageService,
    ) {
        super();
    }

    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        const token = this.localStorage.retrieve('authenticationToken') || Cookie.get('authenticationToken');
        if (!!token) {
            options.headers.append('x-auth-token', `${token}`);
        }
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        return observable; // by pass
    }

}
