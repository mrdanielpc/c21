
import {throwError as observableThrowError,  Observable } from 'rxjs';

import { SvsHttpInterceptor } from './svshttp.interceptor';
import { SvsEventManager } from '../event-manager/event-manager.service';
import { RequestOptionsArgs, Response } from '@angular/http';

export class ErrorHandlerInterceptor extends SvsHttpInterceptor {

    constructor(private eventManager: SvsEventManager) {
        super();
    }

    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        return <Observable<Response>>observable.catch((error) => {
            if (!(error.status === 401 && (error.text() === '' ||
                (error.json().path && error.json().path.indexOf('/api/account') === 0)))) {
                console.log('Error-handler->', error);
                this.eventManager.broadcast({ name: 'svsApp.httpError', content: error });
            }
            return observableThrowError(error);
        });
    }
}
