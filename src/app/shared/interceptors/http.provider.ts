import { Injector } from '@angular/core';
import { Http, XHRBackend, RequestOptions } from '@angular/http';

import { SvsInterceptableHttp } from './interceptable-http';

import { AuthInterceptor } from './auth.interceptor';
import { LocalStorageService } from 'ngx-webstorage';

import { AuthExpiredInterceptor } from './auth-expired.interceptor';
// import { ErrorHandlerInterceptor } from './errorhandler.interceptor';
// import { NotificationInterceptor } from './notification.interceptor';
import { SpinnerInterceptor } from './spinner/spinner.interceptor';
import { SvsEventManager } from '../event-manager/event-manager.service';
import { SvsHttpInterceptor } from './svshttp.interceptor';
import { ErrorHandlerInterceptor } from './error-handler.interceptor';
import { Router } from '@angular/router';
import { CacheInterceptor } from './cache.interceptor';

export function interceptableFactory(
    backend: XHRBackend,
    defaultOptions: RequestOptions,
    localStorage: LocalStorageService,
    injector: Injector,
    eventManager: SvsEventManager,
    router: Router
) {
    const interceptors: SvsHttpInterceptor[] = [
        new AuthInterceptor(localStorage),
        new AuthExpiredInterceptor(router, injector),
        new ErrorHandlerInterceptor(eventManager),
        new SpinnerInterceptor(eventManager),
        new CacheInterceptor()
    ];
    return new SvsInterceptableHttp(
        backend,
        defaultOptions,
        interceptors
    );
}

export function customHttpProvider() {
    return {
        provide: Http,
        useFactory: interceptableFactory,

        deps: [
            XHRBackend,
            RequestOptions,
            LocalStorageService,
            Injector,
            SvsEventManager,
            Router

        ]
    };
}
