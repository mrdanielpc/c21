
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { SvsHttpInterceptor } from './svshttp.interceptor';
import { RequestOptionsArgs, Response } from '@angular/http';
import { Injector } from '@angular/core';
import { LoginService } from '../../login/login.service';
import { Router } from '@angular/router';

export class AuthExpiredInterceptor extends SvsHttpInterceptor {

    constructor(private router: Router,
        private injector: Injector) {
        super();
    }

    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        return <Observable<Response>>observable.catch((error) => {
            if (error.status === 401) {
                const loginService: LoginService = this.injector.get(LoginService);
                loginService.logout();
                this.router.navigate(['/login']);
            }
            return observableThrowError(error);
        });
    }
}
