
import { Component, Input } from '@angular/core';
import { SvsEventManager } from '../../event-manager/event-manager.service';

@Component({
    selector: 'svs-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['spinner.component.css']
})

export class SpinnerComponent {

    @Input() isVisible: boolean;
    lastTime: Date;
    counter: number;

    constructor(private eventManager: SvsEventManager) {
        this.counter = 0;
        this.eventManager.subscribe('svsApp.httpStart', () => {
            this.counter++;
            setTimeout(() => {
                if (this.counter > 0) {
                    this.isVisible = true;
                }
            }, 150);
        });
        this.eventManager.subscribe('svsApp.httpStop', () => {
            this.counter--;
            if (this.counter === 0) {
                this.isVisible = false;
            }
        });
    }

}
