
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { SvsHttpInterceptor, } from '../svshttp.interceptor';
import { SvsEventManager } from '../../event-manager/event-manager.service';
import { RequestOptionsArgs, Response } from '@angular/http';

export class SpinnerInterceptor extends SvsHttpInterceptor {
    constructor(private eventManager: SvsEventManager) {
        super();
    }

    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        this.eventManager.broadcast({ name: 'svsApp.httpStart' });
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        return observable.map((response: Response) => {
            this.eventManager.broadcast({ name: 'svsApp.httpStop' });
            return response;
        }).catch((error) => {
            this.eventManager.broadcast({ name: 'svsApp.httpStop' });
            return observableThrowError(error); // here, response is an error
        });
    }
}
