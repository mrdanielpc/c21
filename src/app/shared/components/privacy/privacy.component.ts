import { Component, OnInit, Input } from '@angular/core';
import { WebshopService } from '../../services/webshop.service';

@Component({
  selector: 'c21-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent implements OnInit {
  textoHtmlCondiciones = '';
  @Input() privacyVisible = { visible: false };
  constructor(
    private webshopService: WebshopService
  ) {

  }

  ngOnInit() {
    this.webshopService.getPrivacyText().then((texto) => {
      this.textoHtmlCondiciones = texto;
    });
  }

}
