import { Component, OnInit, Input } from '@angular/core';
import { WebshopService } from '../../services/webshop.service';

@Component({
  selector: 'c21-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.scss']
})
export class ConditionsComponent implements OnInit {

  textoHtmlCondiciones = '';
  @Input() conditionsVisible = { visible: false };
  constructor(
    private webshopService: WebshopService
  ) {

  }

  ngOnInit() {
    this.webshopService.getConditionsText().then((texto) => {
      this.textoHtmlCondiciones = texto;
    });
  }
}
