import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IScheduleModal } from '../../models/schedule.model';
import { Main } from '../../auth';
import { ScheduleService } from '../../services/schedule.service';
import { IDropdown, } from '../../models/generic.model';
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'c21-schedule-editor',
  templateUrl: './schedule-editor.component.html',
  styleUrls: ['./schedule-editor.component.scss']
})
export class ScheduleEditorComponent implements OnInit {

  @Input() modal: IScheduleModal;
  @Output() onSave: EventEmitter<void> = new EventEmitter();
  action: IDropdown[];
  selectedAction: IDropdown;
  lo: any;
  constructor(
    private main: Main,
    private shceduleService: ScheduleService,
    private messageService: MessageService,
    private translate: TranslateService
  ) {
    this.main.identity().then((ident) => {
      this.lo = ident.localeData;
    });

  }

  public selectAction() {
    this.shceduleService.getScheduleAction().then((actions) => {
      this.action = actions;
      if (!!this.modal.schedule && !!this.modal.schedule.tipoAgenda && !!this.modal.schedule.tipoAgenda.codigo) {
        this.selectedAction = this.action.find((ac) => ac.codigo === this.modal.schedule.tipoAgenda.codigo);
        if (!this.selectedAction) {
          this.selectedAction = undefined;
        }
      } else {
        this.selectedAction = undefined;
      }
    });
  }
  ngOnInit() {
  }
  submitClick() {
    // if (!this.selectedAction) {
    //   this.messageService
    //     .add({
    //       severity: 'warn',
    //       summary: this.translate.instant('schedule.editor.notifications.noAction.summary'),
    //       detail: this.translate.instant('schedule.editor.notifications.noAction.detail')
    //     });
    // } else {

    if (!!this.selectedAction) {
      this.modal.schedule.tipoAgenda = Object.assign({});
      this.modal.schedule.tipoAgenda.codigo = +this.selectedAction.codigo;
    }
    this.shceduleService.saveSchedule(this.modal.schedule).then(() => {
      this.messageService
        .add({
          severity: 'success',
          summary: this.translate.instant('schedule.editor.notifications.saveSuccess.summary'),
          detail: this.translate.instant('schedule.editor.notifications.saveSuccess.detail')
        });
      this.onSave.emit();
      this.modal.visible = false;
    });
    // }
  }
}
