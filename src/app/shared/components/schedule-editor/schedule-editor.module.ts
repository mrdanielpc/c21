import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules';
import { ScheduleService } from '../../services/schedule.service';
import { ScheduleEditorComponent } from './schedule-editor.component';

@NgModule({
    imports: [
        // FormsModule,
        CommonModule,
        SharedModule,
        // ScheduleEditorModule
    ],
    declarations: [
        ScheduleEditorComponent
    ],
    providers: [
        ScheduleService,
    ],
    exports: [
        ScheduleEditorComponent
    ]
})
export class ScheduleEditorModule { }
