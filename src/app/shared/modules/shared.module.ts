import { NgModule } from '@angular/core';
import {
    ButtonModule, DropdownModule, PanelModule, ChartModule,
    DataTableModule, DataListModule, AccordionModule, TooltipModule, PaginatorModule,
    DataGridModule, CheckboxModule, DialogModule, ListboxModule, SplitButtonModule,
    CalendarModule, InputTextModule, ConfirmDialogModule,
    GrowlModule, RadioButtonModule, InputTextareaModule, GalleriaModule, ScrollPanelModule, ProgressBarModule, FileUploadModule, SidebarModule
} from 'primeng/primeng';

import { TableModule } from 'primeng/table';

import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { LoginService } from '../../login/login.service';
import { UserRouteAccessService, AuthServerProvider, Main, StateStorageService } from '../auth/';
import { HtmlService } from '../services/html.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';

import { MomentModule } from 'angular2-moment';
import { PrivacyComponent } from '../components/privacy/privacy.component';
import { ConditionsComponent } from '../components/conditions/conditions.component';

const CONTROLS_PRIMENG = [ButtonModule, DropdownModule, PanelModule, ChartModule,
    TableModule, DataTableModule, DataListModule, AccordionModule, TooltipModule, PaginatorModule,
    DataGridModule, CheckboxModule, DialogModule, ListboxModule, SplitButtonModule,
    CalendarModule, InputTextModule, ConfirmDialogModule, GrowlModule, RadioButtonModule,
    InputTextareaModule, GalleriaModule,
    ScrollPanelModule, ProgressBarModule, FileUploadModule, SidebarModule];

@NgModule({
    providers: [
        LoginService,
        Main,
        StateStorageService,
        AuthServerProvider,
        TranslateService,
        UserRouteAccessService,
        HtmlService,
        MessageService,
        ConfirmationService
    ],
    declarations: [
        ConditionsComponent,
        PrivacyComponent,
    ],
    imports: [
        CONTROLS_PRIMENG,
        MomentModule
    ],
    exports: [
        TranslateModule,
        MomentModule,
        PrivacyComponent,
        ConditionsComponent,
        CONTROLS_PRIMENG,
    ]
})
export class SharedModule { }
