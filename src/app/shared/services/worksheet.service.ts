
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType, Headers } from '@angular/http';

import { Message } from 'primeng/components/common/message';
import { TranslateService } from '@ngx-translate/core';

import { WorksheetModel } from '../models/worksheet.model';

import { CommonService } from './common.service';
//
// Rest-URL's
import { environment } from '../../../environments/environment';

import { ILanguage } from '../models/generic.model';

@Injectable()
export class WorksheetService {

  constructor(
    private http: Http,
    private translate: TranslateService,
  ) { }

  //#region "Validations"
  public validFormNotifyError(formObj: any): Message[] {
    const message: Message[] = [];
    if (formObj.form.invalid) {
      message.push({
        severity: 'warn',
        summary: this.translate.instant('worksheet.notifyError.notifications.validation.required.summary'),
        detail: this.translate.instant('worksheet.notifyError.notifications.validation.required.detail')
      });
    }
    // Validacion Email
    if (!!formObj.value.email) {
      const msg = this.validateEmail(formObj.value.email);
      if (!!msg) {
        message.push(msg);
      }
    }
    return message;
  }

  public validateEmail(email: string): Message {
    let message: Message;
    const pattern = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$');
    if (pattern.test(email) === false) {
      message = {
        severity: 'warn',
        summary: this.translate.instant('worksheet.notifyError.notifications.validation.email.summary'),
        detail: this.translate.instant('worksheet.notifyError.notifications.validation.email.detail')
      };
    }
    return message;
  }
  //#endregion "Validations"

  //#region "get"

  getWorksheet(params: WorksheetModel.ISubmitParams): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.ver), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }

  getWorksheetTranslation(params: WorksheetModel.ISubmitParams): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.traducir), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }

  private _selectLanguages$: Promise<ILanguage[]>;

  getSelectLanguages(): Promise<ILanguage[]> {
    if (!this._selectLanguages$) {
      this._selectLanguages$ = this._getSelectLanguages();
    }
    return this._selectLanguages$.then<ILanguage[]>((data) => {
      return data;
    }).catch((err) => {
      return err;
    });
  }

  private _getSelectLanguages(): Promise<ILanguage[]> {
    return new Promise<ILanguage[]>((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.obra.worksheet.selectIdiomas)).toPromise()
        .then((dataRes) => {
          resolve(dataRes.json().result);
        })
        .catch((error) => reject(error));
    });
  }

  getBuy(params: { codigoElemento: number, codigoTipoElemento: number }): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.comprar), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }

  generatePdf(params: { codigoElemento: number, codigoTipoElemento: number }): Observable<any> {
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/pdf'
    });

    const data = {
      params,
      responseType: ResponseContentType.Blob,
      headers
    };

    return this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.generarPdf), data)
      .map((response: Response) => {
        const fileBlob = response.blob();
        CommonService.downloadBlob(fileBlob, `work_${params.codigoTipoElemento}_${params.codigoElemento}.pdf`, 'application/pdf');
      });
  }
  generateRtf(params: { codigoElemento: number, codigoTipoElemento: number }): Observable<any> {
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/rtf'
    });

    const data = {
      params,
      responseType: ResponseContentType.Blob,
      headers
    };

    return this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.generarRTF), data)
      .map((response: Response) => {
        const fileBlob = response.blob();
        CommonService.downloadBlob(fileBlob, `work_${params.codigoTipoElemento}_${params.codigoElemento}.rtf`, 'application/rtf');
      });
  }
  //#endregion "get"

  //#region "set"
  setComment(params: WorksheetModel.IWorkComment): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.guardarComentario), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }
  setMyCompanyParticipates(params: { codigoElemento: number, codigoTipoElemento: number }): Promise<any> {
    const data = {
      params
    };

    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.miEmpresaParticipa), data).toPromise().
      then((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }
  //#endregion "set"

  //#region "remove"
  removeComment(params: { codigoElemento: number, codigoTipoElemento: number, codigo: number }): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.eliminarComentario), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }
  //#region "remove"

  //#region "menu"
  sendError(params: WorksheetModel.ISubmitParams): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.guardarError), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }

  updateStatus(params: WorksheetModel.ISubmitParams): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.worksheet.solicitarActualizacion), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }
  //#endregion "menu"

}
