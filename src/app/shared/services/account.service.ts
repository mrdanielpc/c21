
import { throwError as observableThrowError } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, ResponseContentType, Headers, Response } from '@angular/http';
//
import { LocalStorageService } from 'ngx-webstorage';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { environment } from '../../../environments/environment';
import { CommonService } from './common.service';
import { ICompany, IUserAccount, IFactura, IProductosUsuario, ICompra, IActividadUsuario, IMyBusiness, IImagen } from '../models/account.model';
import { IDropdown, IActividadBaseDatos } from '../models/generic.model';
import { TranslateService } from '@ngx-translate/core';
import { IComentario } from '../models/worksheet.model';

@Injectable()
export class AccountService {

    constructor(
        private http: Http,
        private localStorage: LocalStorageService,
        private translate: TranslateService
    ) { }

    getCurrentUser(): Promise<any> {
        const token = this.localStorage.retrieve('authenticationToken') || Cookie.get('authenticationToken');
        if (!!token) {
            return this.http.get(environment.apiUrl + environment.api.userData)
                .toPromise()
                .then((obj) => {
                    return Promise.resolve(obj);
                })
                .catch((error) => observableThrowError(error));
        } else {
            return new Promise<any>((resolve) => { resolve(); });
        }
    }

    getUser(): Promise<IUserAccount> {
        return new Promise<IUserAccount>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.ver)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                })
                .catch((error) => reject(error));
        });
    }

    getMyBusiness(): Promise<IMyBusiness> {
        return new Promise<IMyBusiness>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.business.verEmpresa)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                })
                .catch((error) => reject(error));
        });
    }
    getSelectLanguagesBusiness(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.business.selectIdioma)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    getSelectProvidersType(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.business.selectTipoProveedor)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }
    getStatus(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve) => {
            resolve(
                [{ codigo: true, nombre: this.translate.instant('account.user.statusObj.active') },
                { codigo: false, nombre: this.translate.instant('account.user.statusObj.inactive') }]);
        });
    }
    saveLogo(file: File): Promise<IImagen> {
        const formData: FormData = new FormData();
        formData.append('descriptor', file, file.name);
        return new Promise<IImagen>((resolve) => {
            this.http
                .post(CommonService.getUrl(environment.api.business.logoSave), formData).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                });
        });
    }
    saveUser(userAccount: IUserAccount): Promise<boolean> {
        const data = {
            params: {}
        };
        if (userAccount.codigo) {
            data.params['codigo'] = userAccount.codigo;
        }
        data.params['nombre'] = userAccount.nombre;
        data.params['apellidos'] = userAccount.apellidos;
        data.params['telefonoUsuario'] = userAccount.telefonoUsuario;
        if (!!userAccount.emailNuevo) {
            data.params['emailNuevo'] = userAccount.emailNuevo;
        }
        data.params['idioma.codigo'] = +userAccount.idioma.codigo;
        data.params['activo'] = userAccount.activo;
        data.params['codigoRol'] = userAccount.rol.codigo;
        data.params['login'] = userAccount.login;
        if (!!userAccount.password && !!userAccount.confirmacionPassword) {
            data.params['password'] = userAccount.password;
            data.params['confirmacionPassword'] = userAccount.confirmacionPassword;
        }
        return new Promise<boolean>((resolve) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.guardarUsuario), data).toPromise()
                .then(() => {
                    resolve(true);
                });
        });
    }

    getCompany(): Promise<ICompany> {
        return new Promise<ICompany>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.empresaVer)).toPromise()
                .then((dataRes) => {
                    const comp: ICompany = dataRes.json().model;
                    if (comp.divisionEstatalNivel2.codigo === 0) {
                        comp.divisionEstatalNivel2 = null;
                    }
                    if (comp.divisionEstatalNivel1.codigo === 0) {
                        comp.divisionEstatalNivel1 = null;
                    }
                    if (comp.divisionEstatalPais.codigo === 0) {
                        comp.divisionEstatalPais = null;
                    }
                    resolve(comp);
                })
                .catch((error) => reject(error));
        });
    }
    saveCompany(company: ICompany): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const data = {
                params: {}
            };
            data.params['empresa'] = company.empresa;
            data.params['cif'] = company.cif;
            data.params['telefonosEmpresa'] = company.telefonosEmpresa;
            data.params['direccion'] = company.direccion;
            data.params['localidad'] = company.localidad;
            data.params['cp'] = company.cp;
            data.params['divisionEstatalPais.codigo'] = company.divisionEstatalPais.codigo;
            data.params['divisionEstatalNivel1.codigo'] = company.divisionEstatalNivel1.codigo;
            data.params['divisionEstatalNivel2.codigo'] = company.divisionEstatalNivel2.codigo;

            this.http
                .get(CommonService.getUrl(environment.api.account.empresaSave), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch(() => reject(false));
        });
    }
    saveMyBusiness(company: IMyBusiness): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const data = {
                params: {}
            };
            data.params['empresa'] = company.empresa;
            data.params['cif'] = company.cif;
            data.params['telefonosEmpresa'] = company.telefonosEmpresa;
            data.params['direccion'] = company.direccion;
            data.params['localidad'] = company.localidad;
            data.params['cp'] = company.cp;
            data.params['divisionEstatalPais.codigo'] = company.divisionEstatalPais.codigo;
            data.params['divisionEstatalNivel1.codigo'] = company.divisionEstatalNivel1.codigo;
            data.params['divisionEstatalNivel2.codigo'] = company.divisionEstatalNivel2.codigo;

            data.params['codigoTipoProveedor'] = company.codigoTipoProveedor;
            data.params['web'] = company.web;
            data.params['faxEmpresa'] = company.faxEmpresa;
            data.params['emailEmpresa'] = company.emailEmpresa;
            const promis: Promise<any>[] = [];
            this.http
                .get(CommonService.getUrl(environment.api.business.save), data).toPromise()
                .then(() => {

                    company.traduccionesFicha.forEach(
                        (trad) => {
                            if (trad) {
                                const data1 = {
                                    params: {
                                        productos: trad.productos,
                                        actividad: trad.actividad,
                                        codigoIdioma: trad.codigoIdioma
                                    }
                                };
                                promis.push(this.http.get(CommonService.getUrl(environment.api.business.saveIdioma), data1).toPromise());
                            }
                        });
                    Promise.all(promis).then(() => {
                        resolve(true);
                    });

                })
                .catch(() => reject(false));
        });
    }
    getProducts(): Promise<IProductosUsuario[]> {
        return new Promise<IProductosUsuario[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.productsList)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().productosUsuario);
                })
                .catch((error) => reject(error));
        });
    }

    getUsage(): Promise<ICompra[]> {
        return new Promise<ICompra[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.consumoList)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().productos);
                })
                .catch((error) => reject(error));
        });
    }

    getUsers(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.userList)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().usuarios);
                })
                .catch((error) => reject(error));
        });
    }

    private _selectRoles$: Promise<IDropdown[]>;

    getRoles(): Promise<IDropdown[]> {
        if (!this._selectRoles$) {
            this._selectRoles$ = this._getRoles();
        }
        return this._selectRoles$.then<IDropdown[]>((data) => {
            return data;
        }).catch((err) => {
            return err;
        });
    }

    private _getRoles(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.rolSelect)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    getInvoices(): Promise<IFactura[]> {
        return new Promise<IFactura[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.invoicesList)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().facturas);
                })
                .catch((error) => reject(error));
        });
    }

    getSelectActivity(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.home.select)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }
    deleteMyBusinessLogo(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.business.logoEliminar)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json());
                })
                .catch((error) => reject(error));
        });
    }
    deleteMyBusinessImg(code: number): Promise<IImagen[]> {
        const data = {
            params: { codigo: code }
        };
        return new Promise<IImagen[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.business.imagenBorrar), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().imagenes);
                })
                .catch((error) => reject(error));
        });
    }

    saveMyBusinessImg(file: File): Promise<IImagen[]> {
        const formData: FormData = new FormData();
        formData.append('descriptor', file, file.name);
        return new Promise<IImagen[]>((resolve) => {
            this.http
                .post(CommonService.getUrl(environment.api.business.imagenGuardar), formData).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().imagenes);
                });
        });
    }
    getActivity(code: number): Promise<IActividadUsuario> {
        const data = {
            params: { codigoPeriodo: code }
        };
        return new Promise<IActividadUsuario>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.home.activity), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().actividadUsuario);
                })
                .catch((error) => reject(error));
        });
    }

    getAccountsComercialActivity(code: number): Promise<IActividadUsuario[]> {
        const data = {
            params: { codigoPeriodo: code }
        };
        return new Promise<IActividadUsuario[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.actividadComerciales), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().actividadComerciales);
                })
                .catch((error) => reject(error));
        });
    }
    getComments(code: number): Promise<IComentario[]> {
        const data = {
            params: { codigoPeriodo: code }
        };
        return new Promise<IComentario[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.comentarios), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().comentarios);
                })
                .catch((error) => reject(error));
        });
    }

    getData(code: number): Promise<IActividadBaseDatos> {
        const data = {
            params: { codigoPeriodo: code }
        };
        return new Promise<IActividadBaseDatos>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.estadisticas), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().actividadBaseDatos);
                })
                .catch((error) => reject(error));
        });
    }

    deleteUser(code: number): Promise<boolean> {
        const data = {
            params: { codigo: code }
        };
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.account.elimiarUsuario), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    resendMail(email: string) {
        const data = {
            params: { emails: email }
        };
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.business.resend), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    downloadInvoice(code: number) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/pdf'
        });
        const params = { codigo: code };
        const data = {
            params,
            responseType: ResponseContentType.Blob,
            headers
        };

        return this.http
            .get(CommonService.getUrl(environment.api.account.invoiceDownload), data).toPromise().
            then((response: Response) => {
                const fileBlob = response.blob();
                CommonService.downloadBlob(fileBlob, `invoice_${code}.pdf`, 'application/pdf');
            });
    }

}
