
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType, Headers } from '@angular/http';

import { TranslateService } from '@ngx-translate/core';

import { CommonService } from './common.service';
//
// Rest-URL's
import { environment } from '../../../environments/environment';
//
// Model
import { Alert, IAlertlHome } from '../../shared/models/alert.model';
import { IDropdown } from '../../shared/models/generic.model';
import { Message } from 'primeng/components/common/message';

@Injectable()
export class AlertService {
  frequency: IDropdown[];
  typeProfile: IDropdown[];

  constructor(
    private http: Http,
    private translate: TranslateService,
  ) { }

  getFrequency(): Promise<IDropdown[]> {
    return new Promise<IDropdown[]>((resolve, reject) => {
      if (!!this.frequency) {
        resolve(this.frequency);
      } else {
        this._getFrequencyAjax().toPromise()
          .then((data) => {
            this.frequency = data.result;
            resolve(data.result);
          })
          .catch((err) => {
            reject(err);
          });
      }
    });
  }

  getHTML(): Promise<IDropdown[]> {
    return new Promise<IDropdown[]>((resolve) => {
      resolve(this._getHTML());
    });
  }

  getTypeProfile(): Promise<IDropdown[]> {

    return new Promise<IDropdown[]>((resolve, reject) => {
      if (!!this.typeProfile) {
        resolve(this.typeProfile);
      } else {
        this._getTypeProfile().toPromise()
          .then((data) => {
            this.typeProfile = data.result;
            resolve(data.result);
          })
          .catch((err) => {
            reject(err);
          });
      }
    });
  }

  getNotification(): Promise<IDropdown[]> {
    return new Promise<IDropdown[]>((resolve) => {
      resolve(this._getNotification());
    });
  }

  //
  //
  //#region "Alert-Params"
  private _getFrequencyAjax(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.frecuencia))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
  }

  private _getHTML(): IDropdown[] {
    return [
      { codigo: false, nombre: this.translate.instant('alerts.saved.assistant.params.profileHtmlFalse') },
      { codigo: true, nombre: this.translate.instant('alerts.saved.assistant.params.profileHtmlTrue') }
    ];
  }

  private _getTypeProfile(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.tipoEnvioPerfil))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
  }

  private _getNotification(): IDropdown[] {
    return [
      { codigo: false, nombre: this.translate.instant('alerts.saved.assistant.params.profileNotificationFalse') },
      { codigo: true, nombre: this.translate.instant('alerts.saved.assistant.params.profileNotificationTrue') }
    ];
  }

  // #endregion "Alert-Params"
  //
  //
  // #region "Assistant"
  getActivity(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.selectActividad))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));

  }
  getMarket(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.selectMercado))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
  }

  getCountries(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.selectPais))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
  }

  getTypesOfWork(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.selectTipoObra))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
  }
  getParamsAlert(alert: Alert): any {
    const params = {};
    params['perfil.nombrePerfil'] = alert.perfil.nombrePerfil;
    params['perfil.frecuencia'] = alert.perfil.frecuencia['codigo'];
    params['perfil.html'] = alert.perfil.html['codigo'];
    params['perfil.tipoEnvioPerfil.codigo'] = alert.perfil.tipoEnvioPerfil.codigo['codigo'];
    params['perfil.notificacion'] = alert.perfil.notificacion['codigo'];

    if (alert.perfil.emails) {
      params['perfil.emails'] = alert.perfil.emails.split(';');
    }

    if (alert.codigoTipoElementoBusqueda) {
      params['codigoTipoElementoBusqueda'] = alert.codigoTipoElementoBusqueda;
    }

    if (alert.codigoPerfil) {
      params['codigoPerfil'] = alert.codigoPerfil;
    }

    return params;
  }
  createAlertSearch(paramsSearch: any, alert: Alert) {
    let params = this.getParamsAlert(alert);
    params = Object.assign(paramsSearch, params);

    const data = {
      params
    };
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.crearAlerta), data)
      .map((dataRes) => dataRes.json())
      .catch((error) => observableThrowError(error));
  }
  createAlert(alert: Alert): Observable<any> {
    const data = {
      params: {
        'perfil.nombrePerfil': alert.perfil.nombrePerfil,
        'perfil.frecuencia': alert.perfil.frecuencia['codigo'],
        'perfil.html': alert.perfil.html['codigo'],
        'perfil.tipoEnvioPerfil.codigo': alert.perfil.tipoEnvioPerfil.codigo['codigo'],
        'perfil.notificacion': alert.perfil.notificacion['codigo'],
        // revisar: •	model de campos buscar  Ver servicio 2.1.3.2.1
        'codigoActividad': alert.buscar.codigoActividad['codigo'],
        'codigosTipoGrupoObra': alert.buscar.codigoTipoObra,
      }
    };

    if (alert.codigoTipoElementoBusqueda) {
      data.params['codigoTipoElementoBusqueda'] = alert.codigoTipoElementoBusqueda;
    } else {
      data.params['codigoTipoElementoBusqueda'] = 1;
    }

    if (alert.codigoPerfil) {
      data.params['codigoPerfil'] = alert.codigoPerfil;
    }

    if (alert.perfil.tipoEnvioPerfil.codigo) {
      data.params['perfil.tipoEnvioPerfil.codigo'] = alert.perfil.tipoEnvioPerfil.codigo['codigo'];
    }

    if (alert.buscar.codigoMercado) {
      data.params['codigoMercado'] = alert.buscar.codigoMercado['codigo'];
    } else if (alert.buscar.codigosPais) {
      data.params['codigosPais'] = alert.buscar.codigosPais['codigo'];
    }

    if (alert.perfil.emails) {
      data.params['perfil.emails'] = alert.perfil.emails.split(';');
    }

    return this.http
      .get(CommonService.getUrl(environment.api.alertas.assistant.crearAlerta), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
  }
  getAlerts(pagina: number, codigoTipoBusqueda: number, term?: string): Observable<any> {
    const data = {
      params: {
        pagina,
        codigoTipoBusqueda,
        term
      }
    };
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.myAlerts.listado), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
  }
  getAlert(codigoTipoElementoBusqueda: number, codigoPerfil: number): Observable<any> {
    const data = {
      params: {
        codigoTipoElementoBusqueda,
        codigoPerfil
      }
    };
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.myAlerts.ver), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
  }
  getAlertForSearch(codigoTipoElementoBusqueda: number, codigoPerfil: number): Observable<any> {
    const data = {
      params: {
        codigoTipoElementoBusqueda,
        codigoPerfil
      }
    };
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.myAlerts.verParaBuscador), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
  }
  deleteAlert(codigoTipoElementoBusqueda: number, codigoPerfil: number): Observable<any> {
    const data = {
      params: {
        codigoTipoElementoBusqueda,
        codigoPerfil
      }
    };
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.myAlerts.delete), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
  }
  getSelectAlerts(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.myAlerts.selectListado))
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
  }
  //#endregion "Assistant"
  //
  //
  //#region "Send-alerts"
  getSelectLastAlertsSent(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.send.selectUltimasAlertasEnviadas))
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
  }

  getSelectAlertHome(): Promise<IDropdown[]> {
    return new Promise<IDropdown[]>((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.alertas.home.selectDestacadas)).toPromise()
        .then((dataRes) => {
          resolve(dataRes.json().result);
        })
        .catch((error) => reject(error));
    });
  }

  getAlertsHome(code: number): Promise<IAlertlHome[]> {
    const data = {
      params: {
        codigoPeriodo: code
      }
    };
    return new Promise<IAlertlHome[]>((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.alertas.home.destacadas), data).toPromise()
        .then((dataRes) => {
          resolve(dataRes.json().elementos);
        })
        .catch((error) => reject(error));
    });
  }

  getLastAlertsSent(params: any): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.alertas.send.ultimasAlertasEnviadas), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }

  //#endregion "Send-alerts"
  //
  //
  //#region "Files-alerts"
  getSelectListFiles(): Observable<any> {
    return this.http
      .get(CommonService.getUrl(environment.api.alertas.myFiles.selectListadoArchivos))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
  }

  getListFiles(params: any): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.alertas.myFiles.listadoArchivos), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }

  resendAlert(params: any): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.alertas.send.reenviarAlerta), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }

  sendDownloadLinkToEmail(params: any): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.alertas.myFiles.enlaceDescargaFichero), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }
  //#endregion "Files-alerts"

  //#region "Validations"
  public validFormAlertPerfil(formAlertPerfil: any): Message[] {
    const message: Message[] = [];
    if (formAlertPerfil.form.invalid) {
      message.push({
        severity: 'warn',
        summary: this.translate.instant('alerts.notifications.validation.required.summary'),
        detail: this.translate.instant('alerts.notifications.validation.required.detail')
      });
    }
    // Validacion Email
    if (!!formAlertPerfil.value.alertPerfil.emails) {
      const emails = formAlertPerfil.value.alertPerfil.emails.split(';');
      emails.forEach((mail) => {
        const msg = this.validateEmail(mail);
        if (!!msg) {
          message.push(msg);
        }
      });
    }
    return message;
  }

  public validateEmail(email: string): Message {
    let message: Message;
    const pattern = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$');
    if (pattern.test(email) === false) {
      message = {
        severity: 'warn',
        summary: this.translate.instant('alerts.notifications.validation.email.summary'),
        detail: this.translate.instant('alerts.notifications.validation.email.detail')
      };
    }
    return message;
  }

  public downloadAlertsSent() {
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/pdf'
    });
    const data = {
      responseType: ResponseContentType.Blob,
      headers
    };

    return this.http
      .get(CommonService.getUrl(environment.api.alertas.send.download), data)
      .toPromise().then((response: Response) => {
        const fileBlob = response.blob();
        CommonService.downloadBlob(fileBlob, `alerts.xls`, 'application/xls');

      });
  }
  //#endregion "Validations"
}
