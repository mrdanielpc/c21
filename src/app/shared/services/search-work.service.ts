
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
// Model
import { IWorkColumns, ISearchParams } from '../../shared/models/search-work.model';
// Rest-URL's
import { environment } from '../../../environments/environment';
import { CommonService } from './common.service';
import { TranslateService } from '@ngx-translate/core';

export enum codigoOrdenAscendenteDescendente {
  Ascendente = 1,
  Descendente = 2,
}
enum tableSortingDirection {
  Ascendente = 1,
  Descendente = -1,
}

@Injectable()
export class SearchWorkService {

  defaultVisibleColumns: string[] = ['urlImagen', 'divisionEstatal2', 'fase', 'tipo', 'nombre', 'promotora'];

  allCols: IWorkColumns[] = [
    // { code: 6, attribute: 'codigo', text: 'Código', cssStyle: { 'width': '100px' } },
    { code: 1007, attribute: 'urlImagen', text: 'imagen', cssStyle: { 'width': '80px' } },
    { code: 1012, attribute: 'nombre', text: 'Nombre', cssStyle: { '': '' } },

    { code: 1008, attribute: 'divisionEstatal1', text: 'Comunidad', cssStyle: { 'width': '120px' } },
    { code: 1009, attribute: 'divisionEstatal2', text: 'Provincia', cssStyle: { 'width': '120px' } },
    { code: 1010, attribute: 'divisionEstatalPais', text: 'País', cssStyle: { 'width': '120px' } },

    { code: 1013, attribute: 'fase', text: 'Fase', cssStyle: { '': '' } },

    { code: 1011, attribute: 'tipo', text: 'Tipo', cssStyle: { '': '' } },
    { code: 1004, attribute: 'promotora', text: 'Promotores', cssStyle: { '': '' } },

    { code: 1003, attribute: 'constructora', text: 'Contratista', cssStyle: { '': '' } },

    { code: 1005, attribute: 'arquitecto', text: 'Arquitecto', cssStyle: { '': '' } },
    { code: 1006, attribute: 'ingenieria', text: 'Ingeniero', cssStyle: { '': '' } },
    { code: 1001, attribute: 'presupuesto', text: 'Presupuesto', cssStyle: { '': '' } },

    { code: 1002, attribute: 'superficie', text: 'Superficie de Construcción', cssStyle: { '': '' } }

    // { code: null, attribute: 'permalink', text: 'Permalink', cssStyle: { '': '' } }

  ];

  constructor(
    private http: Http,
    private translate: TranslateService
  ) {
    this.translate.get([
      'comun.attribute.urlImagen',
      'comun.attribute.nombre',
      'comun.attribute.divisionEstatal1',
      'comun.attribute.divisionEstatal2',
      'comun.attribute.divisionEstatalPais',
      'comun.attribute.fase',
      'comun.attribute.tipo',
      'comun.attribute.promotora',
      'comun.attribute.constructora',
      'comun.attribute.arquitecto',
      'comun.attribute.ingenieria',
      'comun.attribute.presupuesto',
      'comun.attribute.superficie'
    ]).subscribe((trans) => {
      const objK = Object.keys(trans);
      for (let x = 0; x < objK.length; x++) {
        this.allCols[x].text = trans[objK[x]];
      }
    });
  }

  getWorks(params: ISearchParams): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.search), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }
  getWorksByProvider(params: ISearchParams): Promise<any> {
    const data = {
      params
    };
    return new Promise((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.obra.searchByProvider), data)
        .toPromise().then((dataResponse) => resolve(dataResponse.json()))
        .catch((error) => reject(error));
    });
  }

  getWorksAssociatedToAlert(params: ISearchParams): Observable<any> {
    const data = {
      params
    };
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.alertas.send.listadoObras), data)
      .map((dataResponse): Response => dataResponse.json())
      .catch((error) => observableThrowError(error));

    return resultado;
  }

  /**
   * param to -> Available values ['to-rest', 'to-primeng']
   *
   * @param to
   * @param sortingDirection
   */
  sortingDirectionTranslate(to: string, sortingDirection: number): number {
    let result = null;
    switch (to) {
      case 'to-rest':
        if (sortingDirection !== tableSortingDirection.Ascendente) {
          result = codigoOrdenAscendenteDescendente.Descendente;
        } else {
          result = codigoOrdenAscendenteDescendente.Ascendente;
        }
        break;
      case 'to-primeng':
        if (sortingDirection !== codigoOrdenAscendenteDescendente.Ascendente) {
          result = tableSortingDirection.Descendente;
        } else {
          result = tableSortingDirection.Ascendente;
        }
        break;
      default:
        break;
    }

    return result;
  }

  /**
   * returns the available columns for the "search" of the data-table
   *
   * @returns {IWorkColumns[]}
   * @memberof SearchWorkService
   */
  getAllColumns(): IWorkColumns[] {
    return this.allCols;
  }

  getColumnCode(attribute: string): number {
    for (const row of this.allCols) {
      if (row.attribute === attribute) {
        return row.code;
      }
    }
    return null;
  }

  /**
   * returns the columns that you want displayed
   *
   * @param {any[]} visibleColumns
   * @returns {any[]}
   * @memberof SearchWorkService
   */
  getTableVisibleColumns(visibleColumns: any[]): any[] {
    const allCols: IWorkColumns[] = this.getAllColumns();
    const colsVisibleTable: IWorkColumns[] = [];

    for (let i = 0; i < allCols.length; i++) {
      if (visibleColumns.includes(allCols[i].attribute)) {
        colsVisibleTable.push(allCols[i]);
      }
    }
    return colsVisibleTable;
  }

  getDefaultVisibleColumns(): string[] {
    return this.defaultVisibleColumns;
  }

  getMiniTableVisibleColumns(): IWorkColumns[] {
    const visibleColumns = this.getDefaultVisibleColumns();
    const allCols: IWorkColumns[] = this.getAllColumns();

    const colsVisibleTable: IWorkColumns[] = [];

    for (let i = 0; i < allCols.length; i++) {
      if (visibleColumns.includes(allCols[i].attribute)) {
        colsVisibleTable.push(allCols[i]);
      }
    }
    return colsVisibleTable;
  }
}
