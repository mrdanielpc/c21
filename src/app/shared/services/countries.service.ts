
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { Http, Response } from '@angular/http';

// Model
// Rest-URL's
import { environment } from '../../../environments/environment';
import { CommonService } from './common.service';

@Injectable()
export class CountriesService {

  constructor(
    private http: Http
  ) { }

  getCountries(params): Observable<any> {
    const data = {
      params
    };

    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.filtro.paises), data)
      .map((dataRes): Response => dataRes.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }

}
