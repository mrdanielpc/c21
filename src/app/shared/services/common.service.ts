// Rest-URL's
import { environment } from '../../../environments/environment';
import { IDropdown } from '../models/generic.model';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {
  constructor(
    private http: Http
  ) {

  }

  public static getUrl(modelo: String) {
    return environment.apiUrl + modelo;
  }

  private _countriesList$: Promise<IDropdown[]>;
  private _workType$: Promise<IDropdown[]>;

  getCountries(): Promise<IDropdown[]> {
    if (!this._countriesList$) {
      this._countriesList$ = this._getCountries();
    }
    return this._countriesList$.then<IDropdown[]>((data) => {
      return data;
    }).catch((err) => {
      return err;
    });
  }

  private _getCountries(): Promise<IDropdown[]> {
    return new Promise<IDropdown[]>((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.obra.filtro.paises)).toPromise()
        .then((dataRes) => {
          resolve(dataRes.json().result);
        })
        .catch((error) => reject(error));
    });
  }
  getTiposObra(): Promise<IDropdown[]> {
    if (!this._workType$) {
      this._workType$ = this._getTiposObra();
    }
    return this._workType$.then<IDropdown[]>((data) => {
      return data;
    }).catch((err) => {
      return err;
    });
  }

  private _getTiposObra(): Promise<IDropdown[]> {
    return new Promise<IDropdown[]>((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.obra.filtro.tipoObra)).toPromise()
        .then((dataRes) => {
          resolve(dataRes.json().result);
        })
        .catch((error) => reject(error));
    });
  }

  getCountriesDivision1(codigo: number): Promise<IDropdown[]> {
    const data = { params: {} };
    data.params['divisionEstatalPais.codigo'] = codigo;

    return new Promise<IDropdown[]>((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.common.divisionEstatal1), data).toPromise()
        .then((dataRes) => {
          resolve(dataRes.json().result);
        })
        .catch((error) => reject(error));
    });
  }

  getCountriesDivision2(codigo: number): Promise<IDropdown[]> {
    const data = { params: {} };
    data.params['divisionEstatalNivel1.codigo'] = codigo;

    return new Promise<IDropdown[]>((resolve, reject) => {
      this.http
        .get(CommonService.getUrl(environment.api.common.divisionEstatal2), data).toPromise()
        .then((dataRes) => {
          resolve(dataRes.json().result);
        })
        .catch((error) => reject(error));
    });
  }

  public static downloadBlob(fileBlob: Blob, fileName: string, type: string) {
    console.log(navigator.appVersion.toString());
    if (navigator.appVersion.toString().indexOf('.NET') > 0) {
      window.navigator.msSaveBlob(fileBlob, fileName);
    } else {
      const blob = new Blob([fileBlob], {
        type
      });
      const a = window.document.createElement('a');
      a.href = window.URL.createObjectURL(blob);
      a.download = fileName;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }
}
