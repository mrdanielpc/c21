import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class HtmlService {
    constructor() {

    }
    keys(object: {}) {
        return Object.keys(object);
    }
    isString(val) {
        return typeof val === 'string';
    }
    isNumber(val) {
        return typeof val === 'number';
    }
    isBoolean(val) {
        return typeof val === 'boolean';
    }
    isDate(str) {
        if (isNaN(str) && typeof (str) === 'string') {
            if (str.indexOf('/Date(') === 0) {
                return true;
            } else {
                return moment(str, [moment.ISO_8601]).isValid();
            }
        } else {
            if (str instanceof Date) {
                return moment(str).isValid();
            } else {
                return false;
            }
        }
    }
}
