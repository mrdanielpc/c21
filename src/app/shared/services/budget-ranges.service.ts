
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
// Rest-URL's
import { environment } from '../../../environments/environment';
//
// Model
import { CommonService } from './common.service';

@Injectable()
export class BudgetRangesService {

  constructor(
    private http: Http
  ) { }

  getBudgetRanges(): Observable<any> {
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.filtro.rangosPresupuesto))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }

}
