import { Injectable } from '@angular/core';
import { Http, Headers, ResponseContentType, Response } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from './common.service';
import { environment } from '../../../environments/environment';
import { IFolders, IFolderWorkHome } from '../models/folder.model';
import { allCols, defaultVisibleColumns } from './folders-grid.service';
import { IWorkColumns, IPaginator } from '../models/search-work.model';
import { IDropdown } from '../models/generic.model';
import * as moment from 'moment';

@Injectable()
export class FolderService {
    constructor(
        private http: Http,
        private translate: TranslateService
    ) {
        this.translate.get([
            'comun.attribute.carpeta.nombre',
            'comun.attribute.tipoElemento.nombre',
            'comun.attribute.urlImagen',
            'comun.attribute.nombre',
            'comun.attribute.divisionEstatalPais',
            'comun.attribute.divisionEstatal1',
            'comun.attribute.divisionEstatal2',
            'comun.attribute.fase',
            'comun.attribute.tipo',
            'comun.attribute.promotora',
            'comun.attribute.constructora',
            'comun.attribute.arquitecto',
            'comun.attribute.ingenieria',
            'comun.attribute.presupuesto',
            'comun.attribute.superficie',
            'comun.attribute.gestiones'
        ]).subscribe((trans) => {
            const objK = Object.keys(trans);
            for (let x = 0; x < objK.length; x++) {
                allCols[x].text = trans[objK[x]];
            }
        });
    }

    //#region "grid"
    getDefaultVisibleColumns(): string[] {
        return defaultVisibleColumns;
    }
    getAllColumns(): IWorkColumns[] {
        return allCols;
    }
    getTableVisibleColumns(visibleColumns: any[]): any[] {
        const colsVisibleTable: IWorkColumns[] = [];

        for (let i = 0; i < allCols.length; i++) {
            if (visibleColumns.includes(allCols[i].attribute)) {
                colsVisibleTable.push(allCols[i]);
            }
        }
        return colsVisibleTable;
    }

    getColumnCode(attribute: string): number {
        const find = allCols.find((col) => col.attribute === attribute);
        if (!find) {
            return null;
        } else {
            return find.code;
        }
    }
    //#endregion "grid"

    //#region "Carpetas-Obras"
    getFolderWorks(codeFolder: number, page: IPaginator, keyword?: string): Promise<any> {
        const params = {
            codigoCarpeta: codeFolder,
            pagina: page.pagina
        };
        if (!!page.sortingColumnCode) {
            params['codigoTipoOrden'] = page.sortingColumnCode;
            params['codigoOrdenAscendenteDescendente'] = page.sortingDirection;
        }
        if (!!keyword) {
            params['palabrasClave'] = keyword;
        }
        return new Promise<any>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.listWorks), {
                    params
                }).toPromise()
                .then((dataRes) => {
                    const items = dataRes.json();
                    for (const item of items.elementos) {
                        if (item.tipoElemento.codigo === 1) {
                            item.tipoElemento.nombre = this.translate.instant('comun.text.works');
                        } else if (item.tipoElemento.codigo === 4 || item.tipoElemento.codigo === 2) {
                            item.tipoElemento.nombre = this.translate.instant('comun.text.tenders');
                        }
                    }
                    resolve({ elementos: items.elementos, model: items.model });

                })
                .catch((error) => reject(error));
        });
    }

    concactFoldersCodeType(elem: [{ codigo?: number, carpeta?: IFolders, tipoElemento?: { codigo?: number } }]): string[] {
        const str: string[] = [];
        elem.forEach((element) => {
            str.push(`${element.tipoElemento.codigo}_${element.codigo}`);
        });
        return str;
    }
    concatenateWorkCodesForFolders(works: any[]): string[] {
        const worksCodes: string[] = [];
        works.forEach((element) => {
            worksCodes.push(`${element.tipoElemento.codigo}_${element.codigo}`);
        });
        return worksCodes;
    }
    concatenateFolderWorkCodes(codigoTipoElemento: number, codigoObras: number[]): string[] {
        const worksCodes: string[] = [];
        codigoObras.forEach((element) => {
            worksCodes.push(codigoTipoElemento + '_' + element);
            // tipoElemento.codigo + "_" + elemento.codigo
        });
        return worksCodes;
    }
    addWorksToFolder(codeFolder: number, concatenatedCodes: string[]): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.addWork), { params: { codigo: codeFolder, codigosElementosSelecionados: concatenatedCodes } }).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    deleteWorkFromFolder(codeFolder: number, concatenatedCodes: string[]): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.deleteWorkFromFolder),
                    { params: { codigo: codeFolder, codigosElementosSelecionados: concatenatedCodes } }).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    //#endregion "Carpetas-Obras"

    //#region "Carpetas"
    getFolders(): Promise<IFolders[]> {
        return new Promise<IFolders[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.list)).toPromise()
                .then((dataRes) => {
                    const folders = <IFolders[]>dataRes.json().elementos;
                    for (const folder of folders) {
                        folder.nameWithNumbers = `${folder.nombre} (${folder.numeroElementos})`;
                    }
                    resolve(folders);
                })
                .catch((error) => reject(error));
        });
    }

    getFolder(codeFolder: number): Promise<IFolders> {
        return new Promise<IFolders>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.verCarpeta), { params: { codigo: codeFolder } })
                .toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                })
                .catch((error) => reject(error));
        });
    }
    getLastWorks(code: number): Promise<IFolderWorkHome[]> {
        return new Promise<IFolderWorkHome[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.lastWorks), { params: { codigoBusquedaDefecto: code } })
                .toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().elementos);
                })
                .catch((error) => reject(error));
        });
    }

    getSelectLastWorks(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.selectLastWorks))
                .toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    saveFolder(folder: IFolders): Promise<boolean> {
        // Permite crear o actualizar una carpeta
        // En el caso de una carpeta nueva el código irá vacio.
        const params = {};
        params['nombre'] = folder.nombre;
        params['avisoEmail'] = folder.avisoEmail;
        if (!!folder.codigo && folder.codigo > 0) {
            params['codigo'] = folder.codigo;
        }
        if (!!folder.codigoCarpetaPadre) {
            params['carpetaPadre.codigo'] = folder.codigoCarpetaPadre;
        }
        const data = {
            params
        };
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.guardarCarpeta), data)
                .toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    exportPDF(code: number, dateStart?: Date, dateEnd?: Date) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/pdf'
        });
        const params = { codigoCarpeta: code };
        if (dateStart) {
            params['fechaDesde'] = moment(dateStart).utc().toJSON();
        }
        if (dateEnd) {
            params['fechaHasta'] = moment(dateEnd).utc().toJSON();
        }
        const data = {
            params,
            responseType: ResponseContentType.Blob,
            headers
        };

        return this.http
            .get(CommonService.getUrl(environment.api.folders.exportPDF), data).toPromise().
            then((response: Response) => {
                const fileBlob = response.blob();
                CommonService.downloadBlob(fileBlob, `folders_${code}.pdf`, 'application/pdf');
            });
    }

    exportXLS(code: number, dateStart?: Date, dateEnd?: Date) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.ms-excel'
        });
        const params = { codigoCarpeta: code };
        if (dateStart) {
            params['fechaDesde'] = moment(dateStart).utc().toJSON();
        }
        if (dateEnd) {
            params['fechaHasta'] = moment(dateEnd).utc().toJSON();
        }
        const data = {
            params,
            responseType: ResponseContentType.Blob,
            headers
        };

        return this.http
            .get(CommonService.getUrl(environment.api.folders.exportExcel), data).toPromise().
            then((response: Response) => {
                const fileBlob = response.blob();
                CommonService.downloadBlob(fileBlob, `folders_${code}.xls`, 'application/vnd.ms-excel');
            });
    }
    deleteFolder(code: number): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.folders.eliminarCarpeta), { params: { codigo: code } }).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    //#endregion "Carpetas"
}
