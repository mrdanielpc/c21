import { Http, ResponseContentType, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { environment } from '../../../environments/environment';
import { IDropdown } from '../models/generic.model';
import { IScheduleObject, ISchedule } from '../models/schedule.model';
import * as moment from 'moment';

@Injectable()
export class ScheduleService {
    // scheduleActionList: IDropdown[];
    private _scheduleList$: Promise<IDropdown[]>;
    constructor(
        private http: Http
    ) { }

    getScheduleAction(): Promise<IDropdown[]> {
        if (!this._scheduleList$) {
            this._scheduleList$ = this._getScheduleAction();
        }
        return this._scheduleList$.then<IDropdown[]>((data) => {
            return data;
        }).catch((err) => {
            return err;
        });
    }

    private _getScheduleAction(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.schedule.selectTiposAgenda)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    getScheduleSelect(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.schedule.selectListadoAgenda)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    getSchedule(codigoPeriodo: number): Promise<IScheduleObject> {
        const params = {
            codigoPeriodo
        };
        return new Promise<IScheduleObject>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.schedule.scheduleList), { params }).toPromise()
                .then((dataRes) => {
                    const vals = dataRes.json();
                    if (!!vals.elementos) {
                        for (const val of vals.elementos) {
                            if (!!val.fechaAviso) {
                                val.fechaAviso = moment(val.fechaAviso).toDate();
                            }
                        }
                    }
                    resolve(vals);
                })
                .catch((error) => reject(error));
        });
    }
    getScheduleMonth(month: number, year: number): Promise<IScheduleObject> {
        const params = {
            mes: month,
            anho: year
        };
        return new Promise<IScheduleObject>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.schedule.scheduleList), { params }).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json());
                })
                .catch((error) => reject(error));
        });
    }
    delete(codigo: number): Promise<any> {
        const params = {
            codigo
        };
        return new Promise<any>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.schedule.delete), { params }).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json());
                })
                .catch((error) => reject(error));
        });
    }
    saveSchedule(schedule: ISchedule) {
        const params = {};
        params['codigo'] = schedule.codigo;
        params['codigoElemento'] = schedule.codigoElemento;
        params['codigoProveedor'] = schedule.codigoProveedor;
        params['codigoTipoElemento'] = schedule.codigoTipoElemento;
        params['fechaAviso'] = moment(schedule.fechaAviso).toJSON();
        if (!!schedule.tipoAgenda) {
            params['tipoAgenda.codigo'] = schedule.tipoAgenda.codigo;
        }
        params['avisoEmail'] = schedule.avisoEmail;
        params['contacto'] = schedule.contacto;
        params['telefonos'] = schedule.telefonos;
        params['descripcion'] = schedule.descripcion;

        return new Promise<any>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.schedule.save), { params }).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json());
                })
                .catch((error) => reject(error));
        });
    }
    downloadSchedule(codigoPeriodo: number): Promise<void> {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/pdf'
        });
        const params = { codigoPeriodo };
        const data = {
            params,
            responseType: ResponseContentType.Blob,
            headers
        };

        return this.http
            .get(CommonService.getUrl(environment.api.schedule.exportList), data).toPromise().
            then((response: Response) => {
                const fileBlob = response.blob();
                CommonService.downloadBlob(fileBlob, `schedule_${codigoPeriodo}.pdf`, 'application/pdf');
            });
    }
}
