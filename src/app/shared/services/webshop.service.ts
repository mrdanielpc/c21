import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

// import { TranslateService } from '@ngx-translate/core';
import { ICodigoNombre, IDropdown } from '../models/generic.model';
import { CommonService } from './common.service';
import { environment } from '../../../environments/environment';
import { IWebProduct, IBasketModel, IPayMethod } from '../models/webshop.model';
import { IUsuario } from '../models/account.model';

@Injectable()
export class WebshopService {

    private _productTypes$: Promise<ICodigoNombre[]>;
    private _getRegion$: Promise<IDropdown[]>;
    private _getProvinces$: Promise<IDropdown[]>;
    constructor(
        private http: Http
    ) {
    }

    getProductTypes() {
        if (!this._productTypes$) {
            this._productTypes$ = this._getProductTypes();
        }
        return this._productTypes$.then<ICodigoNombre[]>((data) => {
            return data;
        }).catch((err) => {
            return err;
        });
    }
    private _getProductTypes(): Promise<ICodigoNombre[]> {
        return new Promise<ICodigoNombre[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.selectTipoProducto)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    getProductList(codigo: string): Promise<IWebProduct[]> {
        const data = { params: { codigoTipoProducto: codigo } };
        return new Promise<IWebProduct[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.listadoProductos), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().productos);
                })
                .catch((error) => reject(error));
        });
    }

    addProduct(codigo: number): Promise<boolean> {
        const data = { params: { codigoProducto: codigo } };
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.addProduct), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    delProduct(codigo: number): Promise<boolean> {
        const data = { params: { codigoProducto: codigo } };
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.removeProduct), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    getBasket(): Promise<IBasketModel> {
        return new Promise<IBasketModel>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.verCesta)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                })
                .catch((error) => reject(error));
        });
    }

    getProduct(codigo: number, country?: number, workType?: number): Promise<IWebProduct> {
        const data = { params: { codigo } };
        if (!!country) {
            data.params['divisionEstatal.codigo'] = country;
        }
        if (!!workType) {
            data.params['tipoObra.codigo'] = workType;
        }
        return new Promise<IWebProduct>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.verProducto), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                })
                .catch((error) => reject(error));
        });
    }
    getPaymentMethods(): Promise<IPayMethod[]> {
        return new Promise<IPayMethod[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.selectFormasPago)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    getRegions(): Promise<IDropdown[]> {
        if (!this._getRegion$) {
            this._getRegion$ = this._getRegions();
        }
        return this._getRegion$.then<IDropdown[]>((data) => {
            return data;
        }).catch((err) => {
            return err;
        });
    }
    private _getRegions(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.selectRegiones)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }
    getProvinces(): Promise<IDropdown[]> {
        if (!this._getProvinces$) {
            this._getProvinces$ = this._getProvinces();
        }
        return this._getProvinces$.then<IDropdown[]>((data) => {
            return data;
        }).catch((err) => {
            return err;
        });
    }
    private _getProvinces(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.selectProvincias)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    setPayMethod(codigo: number): Promise<boolean> {
        const data = { params: { codigoFormaPago: codigo } };
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.changePayMethod), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
    getPrivacyText(): Promise<string> {

        return new Promise<string>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.textoPrivacidad)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().textoPrivacidadCompleto);
                })
                .catch((error) => reject(error));
        });
    }
    getConditionsText(): Promise<string> {

        return new Promise<string>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.textoCondiciones)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().textoCondicionesCompleto);
                })
                .catch((error) => reject(error));
        });
    }
    finishPurchase(confirm: boolean): Promise<any> {
        const data = { params: { aceptarCondicionesEspecificas: confirm } };

        return new Promise<any>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.finalizar), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json());
                })
                .catch((error) => reject(error));
        });
    }
    getHelpInfo(): Promise<IUsuario> {
        return new Promise<IUsuario>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.help)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                })
                .catch((error) => reject(error));
        });
    }
    askHelp(email: string, telefonoContacto: string, nombre: string): Promise<boolean> {
        const data = { params: { email, telefonoContacto, nombre } };

        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.webshop.helpAsk), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }
}
