import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { CommonService } from './common.service';
import { environment } from '../../../environments/environment';
import { IContact } from '../models/contact.model';

@Injectable()
export class ContactService {
    constructor(
        private http: Http
    ) { }

    get(): Promise<IContact> {
        return new Promise<IContact>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.contact.info)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().model);
                })
                .catch((error) => reject(error));
        });
    }

    send(nombre: string, mensaje: string, email: string, telefono: string): Promise<boolean> {
        const data = {
            params: {
                nombre,
                mensaje,
                email,
                telefono
            }
        };
        return new Promise<boolean>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.contact.send), data).toPromise()
                .then(() => {
                    resolve(true);
                })
                .catch((error) => reject(error));
        });
    }

}
