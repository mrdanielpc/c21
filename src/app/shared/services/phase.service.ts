
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

// Rest-URL's
import { environment } from '../../../environments/environment';
import { CommonService } from './common.service';

@Injectable()
export class PhaseService {

  constructor(
    private http: Http
  ) { }

  getAllPhaseWorks(): Observable<any> {
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.filtro.faseObras))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }

  getAllPhaseTenders(): Observable<any> {
    const resultado = this.http
      .get(CommonService.getUrl(environment.api.obra.filtro.faseLicitaciones))
      .map((data): Response => data.json())
      .catch((error) => observableThrowError(error));
    return resultado;
  }

}
