import { IWorkColumns } from '../models/search-work.model';

export const defaultVisibleColumns: string[] =
    ['tipoElemento.nombre', 'urlImagen', 'carpeta.nombre', 'gestiones', 'divisionEstatal2', 'fase', 'tipo', 'nombre', 'promotora'];

export const allCols: IWorkColumns[] =
    [
        {
            attribute: 'carpeta.nombre',
            code: 1000,
            text: 'Carpeta',
        },
        {
            attribute: 'tipoElemento.nombre',
            code: 1014,
            text: 'Producto',
            cssStyle: { 'width': '90px' }
        },
        {
            attribute: 'urlImagen',
            code: 1007,
            text: 'Imagen',
            cssStyle: { 'width': '80px' }
        },

        {
            attribute: 'nombre',
            code: 1012,
            text: 'Nombre',
        },

        {
            attribute: 'divisionEstatalPais',
            code: 1010,
            text: 'País',
            cssStyle: { 'width': '120px' }
        },

        {
            attribute: 'divisionEstatal1',
            code: 1008,
            text: 'Comunidad',
            cssStyle: { 'width': '120px' }
        },
        {
            attribute: 'divisionEstatal2',
            code: 1009,
            text: 'Provincia',
            cssStyle: { 'width': '120px' }
        },

        {
            attribute: 'fase',
            code: 1013,
            text: 'Fase',
        },
        {
            attribute: 'tipo',
            code: 1011,
            text: 'Tipo',
        },
        {
            attribute: 'promotora',
            code: 1004,
            text: 'Promotores',
        },
        {
            attribute: 'constructora',
            code: 1003,
            text: 'Contratista',
        },
        {
            attribute: 'arquitecto',
            code: 1005,
            text: 'Arquitecto',
        },
        {
            attribute: 'ingenieria',
            code: 1006,
            text: 'Ingeniero',
        },
        {
            attribute: 'presupuesto',
            code: 1001,
            text: 'Presupuesto',
        },
        {
            attribute: 'superficie',
            code: 1002,
            text: 'Superficie de Construcción',
        },
        {
            attribute: 'gestiones',
            code: 1015,
            text: 'Gestiones',
            cssStyle: { 'width': '90px' }

        }
    ];
