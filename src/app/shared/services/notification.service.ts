import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { IDropdown } from '../models/generic.model';
import { CommonService } from './common.service';
import { environment } from '../../../environments/environment';
import { INotifications } from '../models/notification.model';

@Injectable()
export class NotificationService {
    constructor(
        private http: Http
    ) { }

    getNotificationsSelect(): Promise<IDropdown[]> {
        return new Promise<IDropdown[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.notifications.notificationsSelect)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().result);
                })
                .catch((error) => reject(error));
        });
    }

    getNotifications(codigoPeriodo: number): Promise<INotifications[]> {
        const data = {
            params: {
                codigoPeriodo
            }
        };
        return new Promise<INotifications[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.notifications.listado), data).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().notificaciones);
                })
                .catch((error) => reject(error));
        });
    }
    getNotificationsCount(): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.notifications.count)).toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().totalNotificaciones);
                })
                .catch((error) => reject(error));
        });
    }
}
