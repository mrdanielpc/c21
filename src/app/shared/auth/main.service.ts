import { Injectable } from '@angular/core';
import { Observable ,  Subject } from 'rxjs';
import { SvsEventManager } from '../event-manager/event-manager.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { IUser } from '../models/generic.model';
import { AccountService } from '../services/account.service';

@Injectable()
export class Main {
    private userIdentity: IUser;
    private authenticated = false;
    private authenticationState = new Subject<any>();

    constructor(
        private accountService: AccountService,
        private eventManager: SvsEventManager,
        private translate: TranslateService

    ) {
    }

    authenticate(identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    }

    hasAnyAuthority(authorities: string[]): Promise<boolean> {
        return Promise.resolve(this.hasAnyAuthorityDirect(authorities));
    }

    hasAnyAuthorityDirect(authorities: string[]): boolean {
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }

        for (let i = 0; i < authorities.length; i++) {
            if (this.userIdentity.authorities.indexOf(authorities[i]) !== -1) {
                return true;
            }
        }

        return false;
    }

    hasAuthority(authority: string): Promise<boolean> {
        if (!this.authenticated) {
            return Promise.resolve(false);
        }

        return this.identity().then((id) => {
            return Promise.resolve(id.authorities && id.authorities.indexOf(authority) !== -1);
        }, () => {
            return Promise.resolve(false);
        });
    }

    identity(force?: boolean): Promise<IUser> {
        if (force === true) {
            this.userIdentity = undefined;
        }

        // check and see if we have retrieved the userIdentity data from the server.
        // if we have, reuse it by immediately resolving
        if (this.userIdentity) {
            return Promise.resolve(this.userIdentity);
        }

        // retrieve the userIdentity data from the server, update the identity object, and then resolve.
        return this.accountService.getCurrentUser().then((account) => {
            if (account) {
                this.userIdentity = account.json();
                this.translate.use(this.userIdentity.usuario.idioma.prefijoLocale);
                moment.locale(this.userIdentity.usuario.idioma.prefijoLocale);
                const dateFormat = (this.userIdentity.usuario.idioma.prefijoLocale === 'en' ? 'mm/dd/yy' : 'dd/mm/yy');
                const dateHourFormat = (this.userIdentity.usuario.idioma.prefijoLocale === 'en' ? 'mm/dd/yy hh:mm:ss' : 'dd/mm/yy hh:mm:ss');
                const momentFormatDateWithHour = moment.localeData().longDateFormat('L') + ' HH:mm';
                this.userIdentity.localeData = {
                    firstDayOfWeek: moment.localeData().firstDayOfWeek(),
                    dayNames: moment.localeData().weekdays(),
                    dayNamesShort: moment.localeData().weekdaysShort(),
                    dayNamesMin: moment.localeData().weekdaysMin(),
                    monthNames: moment.localeData().months(),
                    monthNamesShort: moment.localeData().monthsShort(),
                    momentFormatDate: moment.localeData().longDateFormat('L'),
                    momentFormatDateWithHour,
                    formatDate: dateFormat,
                    formatDateWithHour: dateHourFormat,
                    today: this.translate.instant('global.today'),
                    clear: this.translate.instant('global.reset')
                };

                this.authenticated = true;

                // Last thing: Notificate about the user authentication success
                this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });
            } else {
                this.userIdentity = null;
                this.authenticated = false;
            }
            this.authenticationState.next(this.userIdentity);
            return this.userIdentity;
        }).catch(() => {
            this.userIdentity = null;
            this.authenticated = false;
            this.authenticationState.next(this.userIdentity);
            return null;
        });
    }

    isAuthenticated(): boolean {
        return this.authenticated;
    }

    isIdentityResolved(): boolean {
        return this.userIdentity !== undefined;
    }

    getAuthenticationState(): Observable<any> {
        return this.authenticationState.asObservable();
    }

    getImageUrl(): String {
        return this.isIdentityResolved() ? this.userIdentity.imageUrl : null;
    }

}
