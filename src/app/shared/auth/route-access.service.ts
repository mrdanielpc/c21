import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Main } from './main.service';
import { StateStorageService } from './state-storage.service';

import { Observable } from 'rxjs';

@Injectable()
export class UserRouteAccessService implements CanActivate {

    constructor(private router: Router,
        private main: Main,
        private stateStorageService: StateStorageService) {
    }

    canActivate(_route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (state.url === '/login') {
            return this.checkAuthenticated();
        } else {
            const _result: Promise<boolean> = this.checkLogin(state.url);
            return _result;
        }
    }

    checkAuthenticated(nav?: string): Promise<boolean> {
        const navig = !nav ? '/home' : nav;
        const promise = new Promise<boolean>((resolve) => {
            this.main.identity().then((account) => {
                resolve(!account);
                if (account) {
                    this.router.navigate([navig]);
                }
            });
        });
        return promise;
    }

    checkLogin(url: string): Promise<boolean> {
        const principal = this.main;
        const promise = new Promise<boolean>((resolve) => {
            principal.identity().then((account) => {
                if (account) {
                    resolve(true);
                } else {
                    resolve(false);
                    this.stateStorageService.storeUrl(url);
                    this.router.navigate(['/login']);
                }
            });
        });
        return promise;
    }
}
