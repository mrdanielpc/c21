import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import * as sha1 from 'sha1';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AuthServerProvider {
    private urlLogin: string = environment.api.login === '' ? '/rest/publico/loginHacer.json' : environment.api.login;

    constructor(
        private http: Http,
        private $localStorage: LocalStorageService,
        private $sessionStorage: SessionStorageService,
        private translate: TranslateService
    ) { }

    getToken() {
        return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken');
    }

    login(credentials): Observable<any> {

        const data = {
            params: {

                login: credentials.username,
                hashClave: sha1(credentials.password)
            }
        };

        return this.http.get(environment.apiUrl + this.urlLogin, data).map(authenticateSuccess.bind(this));

        function authenticateSuccess(resp) {
            if (!!JSON.parse(resp._body).token) {
                const jwt = JSON.parse(resp._body).token;
                this.storeAuthenticationToken(jwt, credentials.rememberMe);
                return jwt;
            }
        }
    }

    storeAuthenticationToken(jwt, rememberMe?) {
        if (rememberMe) {
            this.$localStorage.store('authenticationToken', jwt);
        } else {
            Cookie.set('authenticationToken', jwt);
        }
    }

    logout(): Observable<any> {
        return new Observable((observer) => {
            this.translate.use('es');
            this.$localStorage.clear('authenticationToken');
            Cookie.set('authenticationToken', '');
            Cookie.delete('authenticationToken');
            observer.complete();
        });
    }
}
