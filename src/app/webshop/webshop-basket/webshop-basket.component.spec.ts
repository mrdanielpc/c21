import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebshopBasketComponent } from './webshop-basket.component';

describe('WebshopBasketComponent', () => {
  let component: WebshopBasketComponent;
  let fixture: ComponentFixture<WebshopBasketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebshopBasketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebshopBasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
