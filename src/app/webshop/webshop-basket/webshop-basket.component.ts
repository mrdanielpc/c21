import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { IBasketModel } from '../../shared/models/webshop.model';
import { WebshopService } from '../../shared/services/webshop.service';

@Component({
  selector: 'c21-webshop-basket',
  templateUrl: './webshop-basket.component.html',
  styleUrls: ['./webshop-basket.component.scss']
})
export class WebshopBasketComponent implements OnInit {
  @Input() basketList: IBasketModel;
  @Output() change: EventEmitter<void> = new EventEmitter<void>();
  constructor(private webshopService: WebshopService) { }

  ngOnInit() {
  }

  delProduct(productCode: number) {
    this.webshopService.delProduct(productCode).then(() => {
      this.change.emit();
    });
  }
}
