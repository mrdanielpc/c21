import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/modules/shared.module';
import { WebshopComponent } from './webshop.component';
import { WebshopService } from '../shared/services/webshop.service';
import { WebshopBasketComponent } from './webshop-basket/webshop-basket.component';
import { WebshopFinishComponent } from './webshop-finish/webshop-finish.component';
import { UserBusinessModule } from '../user/user-business/user-business.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        UserBusinessModule
    ],
    declarations: [
        WebshopComponent,
        WebshopBasketComponent,
        WebshopFinishComponent
    ],
    providers: [
        WebshopService,
    ],
    exports: [
        WebshopComponent
    ]
})
export class WebshopModule { }
