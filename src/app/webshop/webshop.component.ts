import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { WebshopService } from '../shared/services/webshop.service';
import { ICodigoNombre, IDropdown } from '../shared/models/generic.model';
import { IWebProduct, IBasketModel, IPayMethod } from '../shared/models/webshop.model';
import { AccountService } from '../shared/services/account.service';
import { ICompany, IUsuario } from '../shared/models/account.model';
import { CommonService } from '../shared/services/common.service';
import { NgForm } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HtmlService } from '../shared/services/html.service';
import { ConfirmationService } from 'primeng/primeng';
import { UserBusinessComponent } from '../user/user-business/user-business.component';

export enum WebShopTransitions {
  Shop = 'shop',
  Data = 'data'
}

@Component({
  selector: 'c21-webshop',
  templateUrl: './webshop.component.html',
  styleUrls: ['./webshop.component.scss']
})
export class WebshopComponent implements OnInit {

  @ViewChild('myForm') myForm: NgForm;
  @ViewChild('btnPay') btnPay: ElementRef;
  @ViewChild('helpForm') helpForm: NgForm;
  @ViewChild('c21UserBusiness') c21UserBusiness: UserBusinessComponent;
  productTypes: ICodigoNombre[] = [];
  productTypeSelected: ICodigoNombre;
  productList: IWebProduct[] = [];
  basketList: IBasketModel;

  countryList: IDropdown[];
  workTypeList: IDropdown[];
  regionList: IDropdown[];
  provinceList: IDropdown[];

  paymentMethods: IPayMethod[];
  payMethod: IPayMethod;
  company: ICompany;

  step: WebShopTransitions = WebShopTransitions.Shop;
  webShopTransitions = WebShopTransitions;

  conditionsVisible = { visible: false };
  privacyVisible = { visible: false };
  helpVisible = false;
  confirmCondition = false;
  html: HtmlService;
  parametersPay: any;
  urlFormulario: string;
  userHelp: IUsuario;

  acceptLabel = 'confirmDialog.acceptLabel';
  rejectLabel = 'confirmDialog.cancelLabel';

  constructor(
    private route: ActivatedRoute,
    _html: HtmlService,
    private router: Router,
    private accountService: AccountService,
    private webshopService: WebshopService,
    private commonService: CommonService,
    private translate: TranslateService,
    private message: MessageService,
    private ref: ChangeDetectorRef,
    private confirmationService: ConfirmationService

  ) { this.html = _html; }

  ngOnInit() {
    this.getProductTypes();
    this.getBasket().then(() => {
      this.route.params.subscribe((params) => {
        if (!!params['step']) {
          if (params['step'] === WebShopTransitions.Data) {
            this.step = WebShopTransitions.Data;
            this.proceedPurchase();

          } else {
            this.step = WebShopTransitions.Shop;
          }
        } else {
          this.step = WebShopTransitions.Shop;
          this.router.navigate(['/webshop', this.webShopTransitions.Shop]);
        }
      });
    });
  }

  initWebShopData() {
    this.payMethod = null;
    this.confirmCondition = false;
  }
  getProductTypes() {
    this.webshopService.getProductTypes().then((data) => {
      this.productTypes = data;
      if (!this.productTypeSelected && this.productTypes.length > 0) {
        this.productTypeSelected = this.productTypes[0];
        this.getProductList(this.productTypeSelected.codigo);
      }
    });
  }

  getProductList(codigo: string) {
    this.webshopService.getProductList(codigo).then((data) => {
      this.productList = data;
      const promises = [];
      const prodPaisesArray = this.productList.filter((prod) => prod.listarPaises);
      if (prodPaisesArray.length > 0) {
        promises.push(this.commonService.getCountries().then((dataRes) => {
          this.countryList = dataRes;
          prodPaisesArray.forEach((prod) => {
            prod.selectedCountry = dataRes[0];
            prod.modifiedCombo = true;
          });
        }));
      }
      const prodArrayObras = this.productList.filter((prod) => prod.listarTiposObra);
      if (prodArrayObras.length > 0) {
        promises.push(this.commonService.getTiposObra().then((dataWorks) => {
          this.workTypeList = dataWorks;
          prodArrayObras.forEach((prod) => {
            prod.selectedWorkType = dataWorks[0];
            prod.modifiedCombo = true;

          });
        }
        ));
      }
      const prodArrayRegiones = this.productList.filter((prod) => prod.listarRegiones);
      if (prodArrayRegiones.length > 0) {
        promises.push(this.webshopService.getRegions().then((dataReg) => {
          this.regionList = dataReg;
          prodArrayRegiones.forEach((prod) => {
            prod.selectedCountry = dataReg[0];
            prod.modifiedCombo = true;

          });
        }
        ));
      }
      const prodArrayProvincias = this.productList.filter((prod) => prod.listarProvincias);
      if (prodArrayProvincias.length > 0) {
        promises.push(this.webshopService.getProvinces().then((dataProv) => {
          this.provinceList = dataProv;
          prodArrayProvincias.forEach((prod) => {
            prod.selectedCountry = dataProv[0];
            prod.modifiedCombo = true;
          });
        }
        ));
      }
      Promise.all(promises).then(() => {
        const prodModified = this.productList.filter((prod) => prod.modifiedCombo);
        prodModified.forEach((prod) => {
          this.onChangeProduct(prod);
        });
      });
    });
  }
  proceedPurchase() {
    if (!!this.basketList.productos && this.basketList.productos.length > 0) {
      this.initWebShopData();
      this.getPaymentMethods();
      this.step = WebShopTransitions.Data;
      this.ref.detectChanges();
      this.accountService.getCompany().then((data) => {
        this.company = data;
        this.c21UserBusiness.getAfterBusiness();
      });

      this.router.navigate(['/webshop', this.webShopTransitions.Data]);
      window.scrollTo(0, 0);
    } else {
      this.router.navigate(['/webshop', this.webShopTransitions.Shop]);
      this.message.add({
        severity: 'warn',
        summary: this.translate.instant('webshop.notification.noItems.summary'),
        detail: this.translate.instant('webshop.notification.noItems.detail')
      });
    }
  }

  onPayMethodChange() {
    if (!!this.payMethod) {
      this.webshopService.setPayMethod(+this.payMethod.codigo).then(() => {
        this.getBasket();
      });
    }
  }

  onChangeProduct(product: IWebProduct) {
    this.webshopService.getProduct(product.codigo,
      product.selectedCountry ? +product.selectedCountry.codigo : null,
      product.selectedWorkType ? +product.selectedWorkType.codigo : null, ).
      then((data) => {
        product.codigoAdd = data.codigo;
        product.descripcion = data.descripcion;
        product.precioBase = data.precioBase;
        product.precioBaseString = data.precioBaseString;
      });
  }
  onChangeView(productType: ICodigoNombre) {
    this.productTypeSelected = productType;
    this.getProductList(productType.codigo);
  }

  addProduct(product: IWebProduct) {
    let codigo: number;
    codigo = product.codigoAdd ? product.codigoAdd : product.codigo;

    this.webshopService.addProduct(codigo).then(() => {
      this.getBasket();
    });
  }

  getBasket(): Promise<IBasketModel> {
    const basket$ = this.webshopService.getBasket();
    basket$.then((data) => {
      this.basketList = data;
      if (!data.productos || data.productos.length === 0) {
        this.step = WebShopTransitions.Shop;
        this.router.navigate(['/webshop', this.webShopTransitions.Shop]);
      }
    });
    return basket$;
  }

  getPaymentMethods() {
    this.webshopService.getPaymentMethods().then((data) => {
      this.paymentMethods = data;
    });
  }
  showModalCondition() {
    this.conditionsVisible.visible = true;
  }
  showModalPrivacy() {
    this.privacyVisible.visible = true;
  }
  finishPurchase() {
    if (this.confirmCondition) {
      if (!!this.payMethod) {
        this.saveCompanyData().then((savedCompany) => {
          if (savedCompany) {
            let message = 'webshop.notification.confirmPurchase.message';

            if (this.payMethod.esTransferencia) {
              message = 'webshop.notification.confirmPurchase.transferencia';
              this.acceptLabel = this.translate.instant('confirmDialog.yesLabel');
              this.rejectLabel = this.translate.instant('confirmDialog.noLabel');
            } else {
              this.acceptLabel = this.translate.instant('confirmDialog.acceptLabel');
              this.rejectLabel = this.translate.instant('confirmDialog.cancelLabel');
            }

            this.confirmationService.confirm({
              header: this.translate.instant('webshop.notification.confirmPurchase.header'),
              message: this.translate.instant(message),
              accept: () => {
                this.webshopService.finishPurchase(this.confirmCondition).then((resp) => {
                  if (!!resp.urlFormulario) {
                    this.parametersPay = resp.parametros;
                    this.urlFormulario = resp.urlFormulario;
                    this.ref.detectChanges();
                    this.btnPay.nativeElement.click();

                  } else {
                    this.getBasket().then(() => {
                      this.message.add({
                        severity: 'success',
                        summary: this.translate.instant('webshop.notification.payTransfer.summary'),
                        detail: this.translate.instant('webshop.notification.payTransfer.detail')
                      });
                      this.router.navigate(['/thanks']);
                    });
                  }
                });
              }
            });
          }
        }
        );
      } else {
        this.message.add({
          severity: 'warn',
          summary: this.translate.instant('webshop.notification.payMethodError.summary'),
          detail: this.translate.instant('webshop.notification.payMethodError.detail')
        });
      }
    } else {
      this.message.add({
        severity: 'warn',
        // summary: this.translate.instant('webshop.notification.confirmConditions.summary'),
        detail: this.translate.instant('webshop.notification.confirmConditions.detail')
      });
    }
  }
  saveCompanyData(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      if (this.myForm.form.invalid) {
        this.message.add({
          severity: 'warn',
          summary: this.translate.instant('account.notification.company.invalid.summary'),
          detail: this.translate.instant('account.notification.company.invalid.detail')
        });
        resolve(false);
      } else {
        this.accountService.saveCompany(this.company).then(() => {
          resolve(true);
          // this.message.add({
          //   severity: 'success',
          //   summary: this.translate.instant('account.notification.company.saveSuccess.summary'),
          //   detail: this.translate.instant('account.notification.company.saveSuccess.detail')
          // });
        });
      }
    });
  }
  showHelp() {
    this.webshopService.getHelpInfo().then((data) => {
      this.userHelp = data;
      this.helpVisible = true;
    });

  }
  askHelp() {
    if (this.helpForm.form.invalid) {
      this.message.add({
        severity: 'warn',
        summary: this.translate.instant('webshop.help.invalid.summary'),
        detail: this.translate.instant('webshop.help.invalid.detail')
      });
    } else {
      this.webshopService.askHelp(this.userHelp.email, this.userHelp.telefonoContacto, this.userHelp.nombre).then(
        () => {
          this.message.add({
            severity: 'success',
            summary: this.translate.instant('webshop.help.success.summary'),
            detail: this.translate.instant('webshop.help.success.detail')
          });
          this.helpVisible = false;
        });
    }
  }

}
