import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebshopFinishComponent } from './webshop-finish.component';

describe('WebshopFinishComponent', () => {
  let component: WebshopFinishComponent;
  let fixture: ComponentFixture<WebshopFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebshopFinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebshopFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
