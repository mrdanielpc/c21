import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactService } from '../shared/services/contact.service';
import { IContact } from '../shared/models/contact.model';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'c21-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contact: IContact;
  message: string;
  email: string;
  name: string;
  privacyVisible = { visible: false };
  telephone: string;
  @ViewChild('helpForm') helpForm: NgForm;

  constructor(
    private contactService: ContactService,
    private translate: TranslateService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.contactService.get().then((cont) => {
      this.contact = cont;
    });
  }
  showModalPrivacy() {
    this.privacyVisible.visible = true;

  }
  askHelp() {
    if (this.helpForm.form.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('webshop.help.invalid.summary'),
        detail: this.translate.instant('webshop.help.invalid.detail')
      });
    } else {
      this.contactService.send(this.name, this.message, this.email, this.telephone).then(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('webshop.help.success.summary'),
            detail: this.translate.instant('webshop.help.success.detail')
          });
        });
    }
  }

}
