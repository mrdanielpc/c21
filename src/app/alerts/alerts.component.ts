import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { ConfirmationService } from 'primeng/primeng';

import { AlertService } from '../shared/services/alert.service';
import { SearchWorkService } from '../shared/services/search-work.service';

import { IPaginator, MiniSearchView } from '../shared/models/search-work.model';
import { Subject } from 'rxjs';
import { Alert, PerfilAlert } from '../shared/models/alert.model';
import { IDropdown, IUser } from '../shared/models/generic.model';
import { SvsEventManager } from '../shared/event-manager/event-manager.service';
import { Main } from '../shared/auth';

export enum MiniSearchList {
    alertListSent = 0,
    listAssociatedToExcelFile = 1
}

@Component({
    selector: 'c21-alerts',
    templateUrl: './alerts.component.html',
    styleUrls: ['./alerts.component.scss'],
})
export class AlertsComponent implements OnInit, OnDestroy {
    public newAlertData: Alert = new Alert();
    public miniSearchView: MiniSearchView.IMiniSearchView = { table: {}, paginator: {} };
    public miniSearchDialog = { key: null, display: false, title: null };
    public assistantDialog = { display: false };
    c21EditAlert = false;
    paginator: IPaginator;
    paginatorSent: IPaginator;
    txtSearchChanged: Subject<string> = new Subject<string>();
    alertMiniPaginator: { fechaLong: number, perfil: PerfilAlert };

    objData = {
        frequency: {
            list: null,
            get: (): Promise<any> => {
                return this.alertService.getFrequency().then((data) => {
                    this.objData.frequency.list = data;
                });
            }
        },
        // ALERTS
        alerts: {
            term: '',
            list: null,
            get: (): Promise<any> => {
                const page = this.paginator ? this.paginator.pagina : 1;
                return this.alertService.getAlerts(page, this.objData.alerts.select.selected.codigo, this.objData.alerts.term).toPromise().then((data) => {

                    this.alertService.getFrequency()
                        .then((result) => {
                            data.elementos.forEach((element: PerfilAlert) => {
                                if (!!element.frecuencia) {
                                    element.frecuenciaNombre = result.find((fre) => fre.codigo === +element.frecuencia).nombre;
                                } else {
                                    this.translate.get('alerts.saved.noFrequency').toPromise().then((freNombre) => {
                                        element.frecuenciaNombre = freNombre;
                                    });
                                }
                            });
                            this.objData.alerts.list = data.elementos;
                        });

                    this.paginator = data.model;
                });
            },
            select: {
                list: null,
                selected: null,
                get: (): Promise<any> => {
                    return this.alertService.getSelectAlerts().toPromise().then((data) => {
                        this.objData.alerts.select.list = data.result;
                    });
                }
            }
        },
        // LAST-ALERTS
        selectLastAlertsSent: {
            list: null,
            selected: null,
            get: (): Promise<any> => {
                return this.alertService.getSelectLastAlertsSent().toPromise().then((data) => {
                    this.objData.selectLastAlertsSent.list = data.result;
                });
            }
        },
        lastAlertsSent: {
            list: null,
            get: (): Promise<any> => {
                const params = {
                    'filtroFecha': this.objData.selectLastAlertsSent.selected ? this.objData.selectLastAlertsSent.selected['codigo'] : 1,
                    'pagina': this.paginatorSent ? this.paginatorSent.pagina : 1
                };
                return this.alertService.getLastAlertsSent(params).toPromise().then((data) => {
                    this.objData.lastAlertsSent.list = data.elementos;
                    this.paginatorSent = data.model;
                });
            }
        },
        // LAST-FILES
        selectListFiles: {
            list: null,
            selected: null,
            get: (): Promise<any> => {
                return this.alertService.getSelectListFiles().toPromise().then((data) => {
                    this.objData.selectListFiles.list = data.result;
                });
            }
        },
        listFiles: {
            list: null,
            get: (params: any): Promise<any> => {
                return this.alertService.getListFiles(params).toPromise().then((data) => {
                    this.objData.listFiles.list = data.elementos;
                });
            }
        }
    };
    lo: any;
    user: IUser;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private alertService: AlertService,
        private translate: TranslateService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private eventManager: SvsEventManager,
        private searchWorkService: SearchWorkService,
        main: Main
    ) {
        main.identity().then((ident) => {
            this.lo = ident.localeData;
            this.user = ident;
            if (this.user.permisos.mapPermisos.puedeVerArchivos) {
                // Files
                if (!this.objData.selectListFiles.list) {
                    this.objData.selectListFiles.get();
                }
                if (!this.objData.listFiles.list) {
                    const params = { 'filtroFecha': 1 };
                    this.objData.listFiles.get(params);
                }
                // ./Files
            }
        });
        this.txtSearchChanged
            .debounceTime(300) // wait 300ms after the last event before emitting last event
            .distinctUntilChanged() // only emit if value is different from previous value
            .subscribe((model) => {
                this.objData.alerts.term = model;
                this.objData.alerts.get();
            });
        this.objData.frequency.get();
    }

    ngOnInit() {
        // Recogemos los parametros de la URL
        this.route.params.subscribe((params) => {
            if (params['assistantDialog'] != null) {
                if (params['assistantDialog'] === 'assistant') {
                    this.assistantDialog.display = true;
                } else if (params['assistantDialog'] === 'create') {
                    this.createAlert();
                    // this.router.navigateByUrl('/alerts');
                }
            }
        });

        // Alerts
        if (!this.objData.alerts.select.list) {
            this.objData.alerts.select.get().then(() => {
                if (!this.objData.alerts.list) {
                    this.objData.alerts.select.selected = this.objData.alerts.select.list[0];
                    this.objData.alerts.get();
                }
            });
        }

        // ./Alerts

        // LastAlertsList
        if (!this.objData.selectLastAlertsSent.list) {
            this.objData.selectLastAlertsSent.get();
        }
        if (!this.objData.lastAlertsSent.list) {
            // Default
            // const params = { 'filtroFecha': 1 };
            this.objData.lastAlertsSent.get();
        }
        // ./LastAlertsList

    }

    ngOnDestroy() {
        // this.eventManager.broadcast({
        //     name: 'svsDisabled',
        //     content: 0
        // });
    }
    refreshMiniSearchViewer(page: number, key: number) {
        switch (key) {
            case MiniSearchList.alertListSent:
                this.getAlertListSent(this.alertMiniPaginator, page);
                break;

        }
    }
    //
    //
    //#region "assistant"
    assistantCloses() {
        this.router.navigate(['/alerts']);
    }
    //#endregion "assistant"
    createAlertCloses() {
        this.eventManager.broadcast({
            name: 'svsDisabled',
            content: 0
        });
        this.router.navigateByUrl('/alerts');
    }
    //
    //
    //#region "Alertas"

    onChangeAlerts() {
        this.objData.alerts.get();
    }
    onChangeAlertsText(text: string) {
        this.txtSearchChanged.next(text);
    }
    //#endregion "Alertas"
    //
    //
    //#region "Alertas enviadas"

    onChangeselectLastAlertsSent() {
        // const params = { 'filtroFecha': this.objData.selectLastAlertsSent.selected ? this.objData.selectLastAlertsSent.selected['codigo'] : 1 };
        this.objData.lastAlertsSent.get();
    }

    resendAlert(obj: any) {
        const params = {
            'codigoEnvioAlertas': obj.codigoEnvioAlertas,
            'fechaLong': obj.fechaLong,
            'codigoTipoElementoBusqueda': obj.perfil.tipoElemento.codigo
        };
        this.alertService.resendAlert(params).toPromise().then(() => {
            this.messageService.add({
                severity: 'success',
                summary: this.translate.instant('alerts.send.resendAlert.messagesOverlay.accept.summary'),
                detail: this.translate.instant('alerts.send.resendAlert.messagesOverlay.accept.detail')
            });
        });
    }

    getAlertListSent(alert: { fechaLong: number, perfil: PerfilAlert }, page = 1) {
        this.miniSearchView.table.columnas = this.searchWorkService.getMiniTableVisibleColumns();
        // FAKE
        this.alertMiniPaginator = alert;
        const params = {
            'pagina': page,
            'codigoEnvioAlertas': alert.perfil.codigo,
            'fechaLong': alert.fechaLong,
            'codigoTipoElementoBusqueda': alert.perfil.tipoElemento.codigo
        };
        this.searchWorkService
            .getWorksAssociatedToAlert(params)
            .subscribe((data) => {
                // Datos Grid

                this.miniSearchView.table.elementos = data.elementos;
                // Paginator
                this.miniSearchView.paginator.pagina = data.model.pagina;
                this.miniSearchView.paginator.numeroPaginas = data.model.numeroPaginas;
                this.miniSearchView.paginator.numeroResultados = data.model.numeroResultados;
                // Dialog
                this.miniSearchDialog.key = MiniSearchList.alertListSent;
                this.miniSearchDialog.title = this.translate.instant('alerts.send.alertListSent.modal.title');
                this.miniSearchDialog.display = true;
            });
    }
    deleteAlert(alert: { codigo: number, tipoElemento: { codigo: number } }) {
        this.confirmationService.confirm({
            header: this.translate.instant('alerts.saved.delete.header'),
            message: this.translate.instant('alerts.saved.delete.message'),
            icon: 'fa fa-trash',
            accept: () => {
                this.alertService.deleteAlert(alert.tipoElemento.codigo, alert.codigo).toPromise().then((data) => {
                    this.messageService.add({
                        severity: 'success',
                        summary: this.translate.instant('alerts.saved.delete.summary'),
                        detail: data.actionMessages[0]
                    });
                    this.objData.alerts.get();
                });
            },

        });
    }

    createAlert() {
        this.newAlertData = new Alert();
        this.c21SearchFilterChange(this.newAlertData, null);
        this.c21EditAlert = true;
        // this.eventManager.broadcast({
        //     name: 'svsDisabled',
        //     content: 1
        // });
    }

    searchInDropdown(list: IDropdown[], codigo: number | boolean): IDropdown {
        return list.find((lst) => lst.codigo === codigo);
    }

    editAlert(alert: { codigo: number, tipoElemento: { codigo: number } }) {

        this.alertService.getAlert(alert.tipoElemento.codigo, alert.codigo).toPromise().then((data) => {

            this.newAlertData.codigoPerfil = alert.codigo;
            this.newAlertData.perfil = data.perfil;
            this.newAlertData.perfil.emails = data.perfil.emails.join(';');

            Promise.all([
                this.alertService.getTypeProfile(),
                this.alertService.getFrequency(),
                this.alertService.getHTML(),
                this.alertService.getNotification()
            ]).then((results) => {
                this.newAlertData.perfil.tipoEnvioPerfil.codigo = this.searchInDropdown(results[0], +this.newAlertData.perfil.tipoEnvioPerfil.codigo);
                this.newAlertData.perfil.frecuencia = this.searchInDropdown(results[1], +this.newAlertData.perfil.frecuencia);

                this.newAlertData.perfil.html = this.searchInDropdown(results[2], !!this.newAlertData.perfil.html);
                this.newAlertData.perfil.notificacion = this.searchInDropdown(results[3], !!this.newAlertData.perfil.notificacion);
            });

            const navigationExtras: NavigationExtras = {
                queryParams: {

                }
            };

            for (const val of Object.keys(data.model)) {
                navigationExtras.queryParams[val] = data.model[val];
            }

            this.c21EditAlert = true;
            this.c21SearchFilterChange(null, navigationExtras.queryParams);

        });

    }
    c21SearchFilterChange(alert?: Alert, queryParams?: any) {
        this.eventManager.broadcast({
            name: 'searchFiltersChange',
            content: { alert, queryParams }
        });
    }
    showAlert(alert: { codigo: number, tipoElemento: { codigo: number } }) {
        this.alertService.getAlertForSearch(alert.tipoElemento.codigo, alert.codigo).toPromise().then((data) => {
            const navigationExtras: NavigationExtras = {
                queryParams: {

                }
            };
            for (const val of Object.keys(data.model)) {
                navigationExtras.queryParams[val] = data.model[val];
            }
            this.router.navigate(['/search'], navigationExtras);
        });
    }
    paginatorClick(value: number) {
        this.paginator.pagina = value;
        this.objData.alerts.get();
    }
    paginatorSentClick(value: number) {
        this.paginatorSent.pagina = value;

        this.objData.lastAlertsSent.get();
    }
    //#endregion "Alertas enviadas"
    //
    //
    //#region "Mis Archivos"
    getFrecuencia(frecuencia: number): Promise<string> {
        return new Promise<string>((resolve) => {
            this.alertService.getFrequency().then((lstFre) => {
                resolve(lstFre.find((fre) => fre.codigo === frecuencia).nombre);
            });
        });
    }

    onChangeListFiles() {
        const params = { 'filtroFecha': this.objData.selectListFiles.selected['codigo'] };
        this.objData.listFiles.get(params);
    }

    downloadExcelFile(obj: any) {
        if (obj.descargaEmailPendiente) {
            this.confirmationService.confirm({
                header: this.translate.instant('alerts.myFiles.downloadPending.true.confirmDialog.header'),
                message: this.translate.instant('alerts.myFiles.downloadPending.true.confirmDialog.message'),
                icon: 'fa fa-download',
                accept: () => {
                    const params = { 'codigo': obj.codigo };
                    this.alertService.sendDownloadLinkToEmail(params).toPromise().then(() => {
                        this.messageService.add({
                            severity: 'success',
                            summary: this.translate.instant('alerts.myFiles.downloadPending.true.messagesOverlay.accept.summary'),
                            detail: this.translate.instant('alerts.myFiles.downloadPending.true.messagesOverlay.accept.detail')
                        });
                    });
                },
                reject: () => {
                    /* this.messageService.add({ severity: 'error', summary: 'Acción candelada', detail: '¡......!'}); */
                }
            });
        } else {
            const params = { 'codigo': obj.codigo };
            this.alertService.sendDownloadLinkToEmail(params).toPromise().then(() => {
                this.messageService.add({
                    severity: 'success',
                    summary: this.translate.instant('alerts.myFiles.downloadPending.false.messagesOverlay.accept.summary'),
                    detail: this.translate.instant('alerts.myFiles.downloadPending.false.messagesOverlay.accept.detail')
                });
                obj.descargaEmailPendiente = true;
            });
        }
    }

    alertSaved() {
        this.c21EditAlert = false;
        this.objData.alerts.get();
    }
    downloadAlertSent() {
        this.alertService.downloadAlertsSent();
    }
    //#endregion "Mis Archivos"

}
