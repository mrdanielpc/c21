import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniPaginatorComponent } from './mini-paginator.component';

describe('MiniPaginatorComponent', () => {
  let component: MiniPaginatorComponent;
  let fixture: ComponentFixture<MiniPaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiniPaginatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniPaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
