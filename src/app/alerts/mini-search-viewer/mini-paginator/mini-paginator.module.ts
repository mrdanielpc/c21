import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiniPaginatorComponent } from './mini-paginator.component';
import { SharedModule } from '../../../shared/modules/index';

@NgModule({
    imports: [
        // FormsModule,
        CommonModule,
        SharedModule
    ],
    declarations: [
        MiniPaginatorComponent
    ],
    exports: [
        MiniPaginatorComponent
    ]
})
export class MiniPaginatorModule { }
