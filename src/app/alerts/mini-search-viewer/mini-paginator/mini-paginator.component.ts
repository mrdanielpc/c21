import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MiniSearchView } from '../../../shared/models/search-work.model';

@Component({
  selector: 'c21-mini-paginator',
  templateUrl: './mini-paginator.component.html',
  styleUrls: ['./mini-paginator.component.scss']
})
export class MiniPaginatorComponent {
  @Input() public paginatorFiller: MiniSearchView.IPaginator = {};
  @Output() public requestPage: EventEmitter<number> = new EventEmitter<number>();

  getViewPage(newPage: number): void {
    this.requestPage.emit(newPage);
  }
}
