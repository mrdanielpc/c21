import { Component, Input } from '@angular/core';

import { MiniSearchView } from '../../shared/models/search-work.model';
import { MessageService } from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Main } from '../../shared/auth';

@Component({
  selector: 'c21-mini-search-viewer',
  templateUrl: './mini-search-viewer.component.html',
  styles: []
})
export class MiniSearchViewerComponent {
  @Input() public tableFiller: MiniSearchView.ITable = {};
  @Input() public gestion = false;

  lo;
  constructor(private router: Router,
    private messageService: MessageService,
    private translate: TranslateService,
    main: Main
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  redirectToWorksheet(work: any) {
    if (!!work.tieneProductoVisualizacion) {
      this.router.navigate(['work', work.tipoElemento.codigo, work.codigo]);
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('search.notifications.noProduct.summary'),
        detail: this.translate.instant('search.notifications.noProduct.detail')
      });
    }
  }
}
