import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiniSearchViewerComponent } from './mini-search-viewer.component';
import { SharedModule } from '../../shared/modules';

@NgModule({
    imports: [
        // FormsModule,
        CommonModule,
        SharedModule
    ],
    declarations: [
        MiniSearchViewerComponent
    ],
    exports: [
        MiniSearchViewerComponent
    ]
})
export class MiniSearchViewerModule { }
