import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniSearchViewerComponent } from './mini-search-viewer.component';

describe('MiniSearchViewerComponent', () => {
  let component: MiniSearchViewerComponent;
  let fixture: ComponentFixture<MiniSearchViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiniSearchViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniSearchViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
