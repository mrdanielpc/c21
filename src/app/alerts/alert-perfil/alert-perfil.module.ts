import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Forms
// import { FormsModule } from '@angular/forms';

// Import Services
import { AlertService } from '../../shared/services/alert.service';

// Import  Component
import { AlertPerfilComponent } from './alert-perfil.component';
import { SharedModule } from '../../shared/modules/shared.module';

@NgModule({
    imports: [
        // FormsModule,
        CommonModule,
        SharedModule
    ],
    declarations: [
        AlertPerfilComponent
    ],
    providers: [
        AlertService
    ],
    exports: [
        AlertPerfilComponent
    ]
})
export class AlertPerfilModule { }
