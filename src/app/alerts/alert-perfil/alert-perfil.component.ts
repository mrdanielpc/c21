import { Component, OnInit, Input } from '@angular/core';
import { Alert } from '../../shared/models/alert.model';
import { AlertService } from '../../shared/services/alert.service';
import { IDropdown } from '../../shared/models/generic.model';
import { ControlContainer, NgForm } from '@angular/forms';
import { SvsEventManager } from '../../shared/event-manager/event-manager.service';

@Component({
  selector: 'c21-alert-perfil',
  templateUrl: './alert-perfil.component.html',
  styleUrls: ['./alert-perfil.component.scss'],
  viewProviders: [{
    provide: ControlContainer,
    useExisting: NgForm
  }]

})
export class AlertPerfilComponent implements OnInit {
  @Input() newAlertData: Alert = new Alert();
  frequency: IDropdown[];
  html: IDropdown[];
  typeProfile: IDropdown[];
  notification: IDropdown[];

  defaultValuesDropdown = {
    frequency: 1, // codigo frecuancia Diaria
    html: true, // codigo formato HTML
    typeProfile: 2, // codigo solo Email
    notification: true // codigo envío Activado
  };
  charged = false;
  constructor(
    private alertService: AlertService,
    private eventManager: SvsEventManager
  ) {
    this.initListener();
  }

  ngOnInit() {

    Promise.all([
      this.alertService.getFrequency()
        .then((freq) => {
          this.frequency = freq;
        }),
      this.alertService.getHTML()
        .then((html) => {
          this.html = html;
        }),

      this.alertService.getTypeProfile()
        .then((typeProfile) => {
          this.typeProfile = typeProfile;
        }),

      this.alertService.getNotification()
        .then((notification) => {
          this.notification = notification;
        })
    ]).then(() => {
      this.alertAssistentReset();
      this.charged = true;
    });
  }

  initListener() {
    this.eventManager.subscribe('searchFiltersChange', (filters) => {
      if (!!filters.content.alert && this.charged) {
        this.alertAssistentReset(filters.content.alert);
      }
    });
  }

  alertAssistentReset(alert?: Alert) {
    if (!alert) {
      alert = this.newAlertData;
    }
    if (!!this.frequency) {
      alert.perfil.frecuencia =
        this.frequency.find(
          (res) => res.codigo === this.defaultValuesDropdown.frequency
        );
    }
    alert.perfil.html =
      this.html.find(
        (res) => res.codigo === this.defaultValuesDropdown.html
      );
    if (!!this.typeProfile) {
      alert.perfil.tipoEnvioPerfil.codigo =
        this.typeProfile.find(
          (res) => res.codigo === this.defaultValuesDropdown.typeProfile
        );
    }
    alert.perfil.notificacion =
      this.notification.find(
        (res) => res.codigo === this.defaultValuesDropdown.notification
      );

  }

}
