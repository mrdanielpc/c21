import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertAssistantComponent } from './alert-assistant.component';

describe('AlertAssistantComponent', () => {
  let component: AlertAssistantComponent;
  let fixture: ComponentFixture<AlertAssistantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertAssistantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertAssistantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
