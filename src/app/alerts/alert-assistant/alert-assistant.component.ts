import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { MessageService } from 'primeng/components/common/messageservice';

import { AlertService } from '../../shared/services/alert.service';
import { Alert } from '../../shared/models/alert.model';

import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { IDropdown } from '../../shared/models/generic.model';

@Component({
  selector: 'c21-alert-assistant',
  templateUrl: './alert-assistant.component.html',
  styleUrls: ['./alert-assistant.component.scss']
})
export class AlertAssistantComponent implements OnInit {
  @ViewChild('myForm') myForm: NgForm;
  newAlertData: Alert = new Alert();
  codigoTipoObraArray: IDropdown[];
  alertFillerData = {
    activity: {
      list: null,
      get: (): Promise<any> => {
        return this.alertService.getActivity().toPromise().then((data) => {
          this.alertFillerData.activity.list = data.result;
        });
      }
    },
    market: {
      list: null,
      get: (): Promise<any> => {
        return this.alertService.getMarket().toPromise().then((data) => {
          this.alertFillerData.market.list = data.result;
        });
      }
    },
    countries: {
      list: null,
      get: (): Promise<any> => {
        return this.alertService.getCountries().toPromise().then((data) => {
          this.alertFillerData.countries.list = data.result;
        });
      }
    },
    typeOfWork: {
      list: null,
      get: (): Promise<any> => {
        return this.alertService.getTypesOfWork().toPromise().then((data) => {
          this.alertFillerData.typeOfWork.list = data.result;
        });
      }
    },
  };

  constructor(
    private alertService: AlertService,
    private messageService: MessageService,
    private router: Router,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    if (!this.alertFillerData.activity.list) {
      this.alertFillerData.activity.get();
    }
    if (!this.alertFillerData.market.list) {
      this.alertFillerData.market.get();
    }
    if (!this.alertFillerData.countries.list) {
      this.alertFillerData.countries.get();
    }
    if (!this.alertFillerData.typeOfWork.list) {
      this.alertFillerData.typeOfWork.get();
    }

  }

  createAlert() {
    // validate FORM
    const errorMsg = this.alertService.validFormAlertPerfil(this.myForm);
    if (!!errorMsg.length) {
      errorMsg.forEach((msg) => {
        this.messageService.add({ severity: msg.severity, summary: msg.summary, detail: msg.detail });
      });
    } else {
      if (!this.newAlertData.buscar.codigoActividad) {
        this.messageService.add({
          severity: 'warn',
          summary: this.translate.instant('alerts.notifications.validation.saveError.summary'),
          detail: this.translate.instant('alerts.notifications.validation.saveError.codigoActividad')
        });
      } else if (!this.newAlertData.buscar.codigoMercado && !this.newAlertData.buscar.codigosPais) {
        this.messageService.add({
          severity: 'warn',
          summary: this.translate.instant('alerts.notifications.validation.saveError.summary'),
          detail: this.translate.instant('alerts.notifications.validation.saveError.codigoMercado')
        });
      } else if (!this.codigoTipoObraArray) {
        this.messageService.add({
          severity: 'warn',
          summary: this.translate.instant('alerts.notifications.validation.saveError.summary'),
          detail: this.translate.instant('alerts.notifications.validation.saveError.codigoTipoObra')
        });

      } else {
        // alert.buscar.codigoTipoObra['codigo'],
        this.newAlertData.buscar.codigoTipoObra = this.codigoTipoObraArray.map((cod) => +cod.codigo);
        this.alertService
          .createAlert(this.newAlertData)
          .toPromise().then((res) => {
            if (!res.actionErrors) {
              this.messageService.add({
                severity: 'error',
                summary: this.translate.instant('alerts.notifications.validation.saveError.summary'),
                detail: this.translate.instant('alerts.notifications.validation.saveError.detail')
              });
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translate.instant('alerts.notifications.validation.saveSuccess.summary'),
                detail: this.translate.instant('alerts.notifications.validation.saveSuccess.detail')
              });
              this.redirectToParent();
            }
          }
          );
      }
    }
  }

  redirectToParent() {
    this.router.navigate(['/alerts']);
  }

}
