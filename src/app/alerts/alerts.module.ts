import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Frames
import { SharedModule } from '../shared/modules/shared.module';

// Forms
// import { FormsModule } from '@angular/forms';

// Import Services
import { AlertService } from '../shared/services/alert.service';

// Import  Component
import { AlertPerfilModule } from './alert-perfil/alert-perfil.module';
import { AlertAssistantComponent } from './alert-assistant/alert-assistant.component';
import { MiniPaginatorModule } from './mini-search-viewer/mini-paginator/mini-paginator.module';
import { MiniSearchViewerModule } from './mini-search-viewer/mini-search-viewer.module';

@NgModule({
  imports: [
    // FormsModule,
    CommonModule,
    SharedModule,
    AlertPerfilModule,
    MiniPaginatorModule,
    MiniSearchViewerModule

  ],
  declarations: [
    AlertAssistantComponent
  ],
  providers: [
    AlertService,
  ],
  exports: [
    AlertAssistantComponent
  ]

})
export class AlertsModule { }
