import { Component, OnInit } from '@angular/core';
import { IDropdown, IActividadBaseDatos, IEstadisticas } from '../../shared/models/generic.model';
import { AccountService } from '../../shared/services/account.service';
import { HtmlService } from '../../shared/services/html.service';
// import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'c21-home-chart',
  templateUrl: './home-chart.component.html',
  styleUrls: ['./home-chart.component.scss']
})
export class HomeChartComponent implements OnInit {
  dataWorks: any;
  dataFase: any;
  dataLics: any;
  optionsWork: any;
  selectData: IDropdown[];
  selectDataSelected: IDropdown;
  lastData: IActividadBaseDatos;
  // collapsed = false;
  cookieString = 'home-data-collapsed';
  backgroundColorWork = ['#BE3636',
    '#E6AAAF',
    '#711B1B',
    '#D47777',
    '#A25151',
    '#FF1744',
    '#B71C1C',
    '#E6AAAF',
    '#D32F2F',
    '#EF5350'];
  backgroundColorFase = ['#DD9F41',
    '#ffd996',
    '#FFA000',
    '#FF8F00',
    '#FFFF00',
    '#dd9f41',
    '#e6c93f',
    '#FFE50A',
    '#FFD740',
    '#d28e39',
    '#dd9f41',
  ];
  backgroundColorLics = ['#75c4dc',
    '#5699b9',
    '#3a62ac',
    '#9cdeee',
    '#3aadd0',
    '#4173be',
    '#abdcea',
    '#4884d1',
  ];
  html: HtmlService;
  constructor(
    htmlS: HtmlService,
    private accountService: AccountService) {
    this.html = htmlS;
  }
  ngOnInit() {
    this.optionsWork = {
      legend: {
        position: 'bottom', display: false
      },
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) => {
            return data.labels[tooltipItem.index];
          }
        }
      }
    };

    this.accountService.getSelectActivity().then((lastW) => {
      this.selectData = lastW;
      this.selectDataSelected = lastW[0];
      this.getData();
    }
    );
    // this.collapsed = Cookie.get(this.cookieString) === '1' ? true : false;
  }
  // changeCollapsed() {
  // this.collapsed = !this.collapsed;
  // Cookie.set(this.cookieString, this.collapsed ? '1' : '0');
  // }
  onChange() {
    this.getData();
  }
  getData() {
    this.accountService.getData(+this.selectDataSelected.codigo).then((dataBD) => {
      this.lastData = dataBD;
      this.dataWorks = this.graphicFill(this.lastData.estadisticasTipoObra, this.backgroundColorWork);
      this.dataFase = this.graphicFill(this.lastData.estadisticasTipoHito, this.backgroundColorFase);
      this.dataLics = this.graphicFill(this.lastData.estadisticasConcursosPublicos, this.backgroundColorLics);

    });
  }
  addSeparator(num: number): string {
    return num.toString().replace(/(.)(?=(.{3})+$)/g, '$1.');
  }
  graphicFill(estadisticas: IEstadisticas, colors: string[]) {
    const dataKeys = [];
    const labels = [];
    for (const key of Object.keys(estadisticas)) {
      dataKeys.push(estadisticas[key]); // labels.push(key.length > 10 ? key.substring(0, 10) : key);
      labels.push(key);
    }
    return {
      labels,
      datasets: [{
        data: dataKeys, backgroundColor: colors, hoverBackgroundColor: colors
      }
      ]
    }
      ;
  }
}
