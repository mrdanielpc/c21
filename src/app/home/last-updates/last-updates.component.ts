import { Component, OnInit } from '@angular/core';
import { FolderService } from '../../shared/services/folder.service';
import { Router } from '@angular/router';
import { IDropdown } from '../../shared/models/generic.model';
import { IFolderWorkHome } from '../../shared/models/folder.model';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'c21-last-updates',
  templateUrl: './last-updates.component.html',
  styleUrls: ['./last-updates.component.scss']
})
export class LastUpdatesComponent implements OnInit {

  selectLastWorks: IDropdown[];
  lastWorkSelected: IDropdown;
  lastWorks: IFolderWorkHome[];
  collapsed = false;
  constructor(
    private folderService: FolderService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.folderService.getSelectLastWorks().then(
      (lastW) => {
        this.selectLastWorks = lastW;
        this.lastWorkSelected = lastW[0];
        this.getLastWorks();
      }
    );
    this.collapsed = Cookie.get('home-last-updates-collapsed') === '1' ? true : false;

  }

  onChangeSelect() {
    this.getLastWorks();
  }
  getLastWorks() {
    this.folderService.getLastWorks(+this.lastWorkSelected.codigo).then((lw) => {
      this.lastWorks = lw;
    });
  }
  redirectToWork(tipoElemento: number, codigo: number) {
    this.router.navigate(['work', tipoElemento, codigo]);
  }
  changeCollapsed() {
    this.collapsed = !this.collapsed;
    Cookie.set('home-last-updates-collapsed', this.collapsed ? '1' : '0');
  }
}
