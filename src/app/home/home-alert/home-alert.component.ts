import { Component, OnInit } from '@angular/core';
import { IDropdown } from '../../shared/models/generic.model';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { AlertService } from '../../shared/services/alert.service';
import { IAlertlHome, Alert } from '../../shared/models/alert.model';
import { NavigationExtras, Router } from '@angular/router';
import { SvsEventManager } from '../../shared/event-manager/event-manager.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ConfirmationService } from 'primeng/primeng';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'c21-home-alert',
  templateUrl: './home-alert.component.html',
  styleUrls: ['./home-alert.component.scss']
})
export class HomeAlertComponent implements OnInit {
  public newAlertData: Alert = new Alert();
  c21EditAlert = false;
  selectAlerts: IDropdown[];
  selectAlertsSelected: IDropdown;
  lastAlerts: IAlertlHome[];
  collapsed = false;
  cookieString = 'home-alert-collapsed';

  constructor(
    private eventManager: SvsEventManager,
    private alertService: AlertService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.alertService.getSelectAlertHome().then(
      (lastW) => {
        this.selectAlerts = lastW;
        this.selectAlertsSelected = lastW[0];
        this.getAlerts();
      }
    );
    this.collapsed = Cookie.get(this.cookieString) === '1' ? true : false;
  }

  changeCollapsed() {
    this.collapsed = !this.collapsed;
    Cookie.set(this.cookieString, this.collapsed ? '1' : '0');
  }
  onChange() {
    this.getAlerts();
  }
  getAlerts() {
    this.alertService.getAlertsHome(+this.selectAlertsSelected.codigo).then((lw) => {
      this.lastAlerts = lw;
    });
  }

  editAlert(alert: { codigo: number, tipoElemento: { codigo: number } }) {

    this.alertService.getAlert(alert.tipoElemento.codigo, alert.codigo).toPromise().then((data) => {

      this.newAlertData.codigoPerfil = alert.codigo;
      this.newAlertData.perfil = data.perfil;
      this.newAlertData.perfil.emails = data.perfil.emails.join(';');

      Promise.all([
        this.alertService.getTypeProfile(),
        this.alertService.getFrequency(),
        this.alertService.getHTML(),
        this.alertService.getNotification()
      ]).then((results) => {
        this.newAlertData.perfil.tipoEnvioPerfil.codigo = this.searchInDropdown(results[0], +this.newAlertData.perfil.tipoEnvioPerfil.codigo);
        this.newAlertData.perfil.frecuencia = this.searchInDropdown(results[1], +this.newAlertData.perfil.frecuencia);

        this.newAlertData.perfil.html = this.searchInDropdown(results[2], !!this.newAlertData.perfil.html);
        this.newAlertData.perfil.notificacion = this.searchInDropdown(results[3], !!this.newAlertData.perfil.notificacion);
      });

      const navigationExtras: NavigationExtras = {
        queryParams: {

        }
      };

      for (const val of Object.keys(data.model)) {
        navigationExtras.queryParams[val] = data.model[val];
      }

      this.c21EditAlert = true;
      this.c21SearchFilterChange(null, navigationExtras.queryParams);
      scrollTo(0, 0);
    });

  }
  enableMenu() {
    this.eventManager.broadcast({
      name: 'svsDisabled',
      content: 0
    });
  }
  deleteAlert(alert: { codigo: number, tipoElemento: { codigo: number } }) {
    this.confirmationService.confirm({
      header: this.translate.instant('alerts.saved.delete.header'),
      message: this.translate.instant('alerts.saved.delete.message'),
      icon: 'fa fa-trash',
      accept: () => {
        this.alertService.deleteAlert(alert.tipoElemento.codigo, alert.codigo).toPromise().then((data) => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('alerts.saved.delete.summary'),
            detail: data.actionMessages[0]
          });
          this.getAlerts();
        });
      },

    });
  }
  showAlert(alert: { codigo: number, tipoElemento: { codigo: number } }) {
    this.alertService.getAlertForSearch(alert.tipoElemento.codigo, alert.codigo).toPromise().then((data) => {
      const navigationExtras: NavigationExtras = {
        queryParams: {

        }
      };
      for (const val of Object.keys(data.model)) {
        navigationExtras.queryParams[val] = data.model[val];
      }
      this.router.navigate(['/search'], navigationExtras);
    });
  }
  searchInDropdown(list: IDropdown[], codigo: number | boolean): IDropdown {
    return list.find((lst) => lst.codigo === codigo);
  }
  c21SearchFilterChange(alert?: Alert, queryParams?: any) {
    this.eventManager.broadcast({
      name: 'searchFiltersChange',
      content: { alert, queryParams }
    });
  }
  alertSaved() {
    this.c21EditAlert = false;
    this.getAlerts();
  }

}
