import { Component, OnInit } from '@angular/core';
import { IDropdown } from '../../shared/models/generic.model';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ScheduleService } from '../../shared/services/schedule.service';
import { ISchedule } from '../../shared/models/schedule.model';
import { ConfirmationService } from 'primeng/primeng';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'c21-home-schedule',
  templateUrl: './home-schedule.component.html',
  styleUrls: ['./home-schedule.component.scss']
})
export class HomeScheduleComponent implements OnInit {

  selectSchedules: IDropdown[];
  selectScheduleSelected: IDropdown;
  lastSchedules: ISchedule[];
  collapsed = false;
  cookieString = 'home-schedule-collapsed';
  constructor(
    private scheduleService: ScheduleService,
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.scheduleService.getScheduleSelect().then(
      (lastW) => {
        this.selectSchedules = lastW;
        this.selectScheduleSelected = lastW[0];
        this.getSchedule();
      }
    );
    this.collapsed = Cookie.get(this.cookieString) === '1' ? true : false;

  }

  onChange() {
    this.getSchedule();
  }
  getSchedule() {
    this.scheduleService.getSchedule(+this.selectScheduleSelected.codigo).then((lw) => {
      this.lastSchedules = lw.elementos;
    });
  }

  changeCollapsed() {
    this.collapsed = !this.collapsed;
    Cookie.set(this.cookieString, this.collapsed ? '1' : '0');
  }
  onEdit() {
    this.getSchedule();
  }

  onDelete(scheduleCode: number) {
    this.confirmationService.confirm({
      header: this.translate.instant('schedule.card.delete.header'),
      message: this.translate.instant('schedule.card.delete.message'),
      icon: 'fa fa-trash',
      accept: () => {
        this.scheduleService.delete(scheduleCode).then((data) => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('schedule.card.delete.header'),
            detail: data.actionMessages[0]
          });
          this.getSchedule();
        });
      },

    });
  }
}
