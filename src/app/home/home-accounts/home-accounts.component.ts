import { Component, OnInit } from '@angular/core';
import { IDropdown } from '../../shared/models/generic.model';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { AccountService } from '../../shared/services/account.service';
import { Main } from '../../shared/auth';
import { Router } from '@angular/router';
import { IActividadUsuario } from '../../shared/models/account.model';
import { IFolderModal } from '../../shared/models/folder.model';
import { FolderService } from '../../shared/services/folder.service';

@Component({
  selector: 'c21-home-accounts',
  templateUrl: './home-accounts.component.html',
  styleUrls: ['./home-accounts.component.scss']
})
export class HomeAccountsComponent implements OnInit {

  selectAccounts: IDropdown[];
  selectAccountsSelected: IDropdown;
  lastAccounts: IActividadUsuario[];
  collapsed = false;
  cookieString = 'home-accounts-collapsed';
  lo: any;
  createNewFolderModal: IFolderModal = {
    visible: false
  };

  constructor(
    private accountService: AccountService,
    private router: Router,
    private folderService: FolderService,
    main: Main
  ) {
    main.identity().then((acc) => {
      this.lo = acc.localeData;
    });
  }

  ngOnInit() {
    this.accountService.getSelectActivity().then(
      (lastW) => {
        this.selectAccounts = lastW;
        this.selectAccountsSelected = lastW[0];
        this.getAccountsActivity();
      }
    );
    this.collapsed = Cookie.get(this.cookieString) === '1' ? true : false;
  }
  onChange() {
    this.getAccountsActivity();
  }
  changeCollapsed() {
    this.collapsed = !this.collapsed;
    Cookie.set(this.cookieString, this.collapsed ? '1' : '0');
  }
  getAccountsActivity() {
    this.accountService.getAccountsComercialActivity(+this.selectAccountsSelected.codigo).then((lw) => {
      this.lastAccounts = lw;
    });
  }
  redirectToFolder(code: number) {
    this.router.navigate(['folders', code]);
  }

  editFolder(code: number) {
    this.folderService.getFolder(code).then((folder) => {
      this.createNewFolderModal.carpeta = Object.assign({}, folder);
      this.createNewFolderModal.visible = true;
    });
  }

}
