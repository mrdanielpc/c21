import { Component, OnInit } from '@angular/core';
import { IDropdown } from '../../shared/models/generic.model';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { AccountService } from '../../shared/services/account.service';
import { IActividadUsuario } from '../../shared/models/account.model';

@Component({
  selector: 'c21-home-activity',
  templateUrl: './home-activity.component.html',
  styleUrls: ['./home-activity.component.scss']
})
export class HomeActivityComponent implements OnInit {

  selectActivity: IDropdown[];
  selectActivitySelected: IDropdown;
  activities: IActividadUsuario;
  collapsed = false;
  cookieString = 'home-activity-collapsed';
  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.accountService.getSelectActivity().then(
      (lastW) => {
        this.selectActivity = lastW;
        this.selectActivitySelected = lastW[0];
        this.getActivity();
      }
    );

    this.collapsed = Cookie.get(this.cookieString) === '1' ? true : false;

  }
  onChange() {
    this.getActivity();
  }
  changeCollapsed() {
    this.collapsed = !this.collapsed;
    Cookie.set(this.cookieString, this.collapsed ? '1' : '0');
  }

  getActivity() {
    this.accountService.getActivity(+this.selectActivitySelected.codigo).then((lw) => {
      this.activities = lw;
    });
  }

}
