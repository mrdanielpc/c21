import { Component, OnInit } from '@angular/core';
import { IDropdown } from '../../shared/models/generic.model';
import { AccountService } from '../../shared/services/account.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { IComentario } from '../../shared/models/worksheet.model';
import { Main } from '../../shared/auth';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationService } from 'primeng/primeng';
import { WorksheetService } from '../../shared/services/worksheet.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { FolderService } from '../../shared/services/folder.service';

@Component({
  selector: 'c21-home-comments',
  templateUrl: './home-comments.component.html',
  styleUrls: ['./home-comments.component.scss']
})
export class HomeCommentsComponent implements OnInit {

  selectComments: IDropdown[];
  selectCommentsSelected: IDropdown;
  lastComments: IComentario[];
  collapsed = false;
  cookieString = 'home-comments-collapsed';
  lo: any;
  user: string;
  txtAutor = 'Autor: ';
  comentario: string;
  startComment = false;
  codigoElemento: number;
  codigoTipoElemento: number;
  codigo: number;
  constructor(
    private accountService: AccountService,
    private folderService: FolderService,
    private translate: TranslateService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private worksheetService: WorksheetService,
    private messageService: MessageService,
    main: Main
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
      this.user = ident.usuario.login;
    });
  }

  ngOnInit() {
    // Por petición se usa el mismo que el de ultimas actualizaciones
    this.folderService.getSelectLastWorks().then(
      (lastW) => {
        this.selectComments = lastW;
        this.selectCommentsSelected = lastW[0];
        this.getComments();
      }
    );
    this.txtAutor = this.translate.instant('home.main.comments.autor');
    this.collapsed = Cookie.get(this.cookieString) === '1' ? true : false;
  }
  onChange() {
    this.getComments();
  }
  changeCollapsed() {
    this.collapsed = !this.collapsed;
    Cookie.set(this.cookieString, this.collapsed ? '1' : '0');
  }
  getComments() {
    this.accountService.getComments(+this.selectCommentsSelected.codigo).then((lw) => {
      this.lastComments = lw;
    });
  }

  redirectToWork(tipoElemento: number, codigo: number) {
    this.router.navigate(['work', tipoElemento, codigo]);
  }

  deleteComment(codigoElemento: number, codigoTipoElemento: number, codigo: number) {

    this.confirmationService.confirm({
      header: this.translate.instant('worksheet.myAnnotations.removeDialog.header'),
      message: this.translate.instant('worksheet.myAnnotations.removeDialog.message'),
      accept: () => {
        const params: { codigoElemento: number, codigoTipoElemento: number, codigo: number } = {
          codigoElemento,
          codigoTipoElemento,
          codigo
        };
        this.worksheetService.removeComment(params).toPromise().then(() => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('worksheet.myAnnotations.removeDialog.summary'),
            detail: this.translate.instant('worksheet.myAnnotations.removeDialog.detail')
          });
          this.getComments();
        });
      }
    });

  }
  startEditComment(codigoElemento: number, codigoTipoElemento: number, codigo: number, comentario: string) {
    this.codigoElemento = codigoElemento;
    this.codigoTipoElemento = codigoTipoElemento;
    this.codigo = codigo;
    this.comentario = comentario;
    this.startComment = true;
  }
  editComment() {
    const params = {
      codigoElemento: this.codigoElemento,
      codigoTipoElemento: this.codigoTipoElemento,
      comentario: this.comentario,
    };
    params['codigo'] = this.codigo;
    this.worksheetService.setComment(params).toPromise().then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('worksheet.myAnnotations.notifications.summary'),
        detail: this.translate.instant('worksheet.myAnnotations.notifications.detail')
      });
      this.startComment = false;
      this.getComments();
    });
  }

}
