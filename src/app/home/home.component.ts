import { Component, OnInit } from '@angular/core';
import { Main } from '../shared/auth';
import { IMapPermisos } from '../shared/models/generic.model';

@Component({
  selector: 'c21-home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  mapPermisos: IMapPermisos;
  constructor(
    main: Main,
  ) {
    main.identity().then((ident) => {
      this.mapPermisos = ident.permisos.mapPermisos;
    });
  }

  ngOnInit() {

  }

}
