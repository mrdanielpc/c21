import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FaqService } from './faq.service';
import { ICategoria } from './faq.model';

@Component({
  selector: 'c21-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  providers: [FaqService]
})
export class FaqComponent implements OnInit {

  txtSearchChanged: Subject<string> = new Subject<string>();
  keyword: string;
  faqList: ICategoria[];
  faqTypeSelected: ICategoria;
  constructor(
    private faqService: FaqService
  ) {
    this.txtSearchChanged
      .debounceTime(300) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe((model) => {
        this.keyword = model;
        this.getFaq();
      });
  }
  ngOnInit() {
    this.getFaq();
  }
  onChangeKeywordText(text: string) {
    this.txtSearchChanged.next(text);
  }

  getFaq() {
    this.faqService.getFaq(this.keyword).then((faq) => {
      this.faqList = faq;
      if (this.faqList.length === 0) {
        this.faqTypeSelected = undefined;
      } else if (this.faqTypeSelected) {
        const faqSel = this.faqList.find((fq) => fq.codigo === this.faqTypeSelected.codigo);
        if (faqSel) {
          this.faqTypeSelected = faqSel;
        } else {
          this.faqTypeSelected = this.faqList[0];
        }
      } else {
        this.faqTypeSelected = this.faqList[0];
      }
    }
    );
  }

  onChangeView(faqType: ICategoria) {
    this.faqTypeSelected = faqType;
    // this.getProductList(faqTypeSelected.codigo);
  }

}
