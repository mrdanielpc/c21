
export interface IFaqElemento {
    codigo: number;
    contenido: string;
    fechaAlta: string;
    nombre: string;
    visible?: boolean;
}

export interface ICategoria {
    codigo: number;
    elementos: IFaqElemento[];
    nombre: string;
}
