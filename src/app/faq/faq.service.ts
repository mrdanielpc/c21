import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { CommonService } from '../shared/services/common.service';
import { environment } from '../../environments/environment';
import { ICategoria } from './faq.model';

@Injectable()
export class FaqService {
    constructor(
        private http: Http
    ) { }

    getFaq(keyword?: string): Promise<ICategoria[]> {
        const data = { params: {} };
        if (!!keyword) {
            data.params['palabraClave'] = keyword;
        }
        return new Promise<ICategoria[]>((resolve, reject) => {
            this.http
                .get(CommonService.getUrl(environment.api.faq.list), data)
                .toPromise()
                .then((dataRes) => {
                    resolve(dataRes.json().categorias);
                })
                .catch((error) => reject(error));
        });
    }
}
