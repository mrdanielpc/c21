import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { AppRoutingModule } from './app.routes';
import { UserModule } from './user/user.module';
import { AlertsModule } from './alerts/alerts.module';
import { SearchModule } from './search/search.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';
import { HomeComponent } from './home/home.component';

import { LoginComponent } from './login/login.component';

import { HttpModule } from '@angular/http';
import { Ng2Webstorage, SessionStorageService, LocalStorageService } from 'ngx-webstorage';

import { AlertsComponent } from './alerts/alerts.component';
import { ContactComponent } from './contact/contact.component';
import { FaqComponent } from './faq/faq.component';

import { SharedModule } from './shared/modules/shared.module';

import { customHttpProvider } from './shared/interceptors/http.provider';
import { SvsEventManager } from './shared/event-manager/event-manager.service';
import { SpinnerComponent } from './shared/interceptors/spinner/spinner.component';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { FoldersModule } from './folders/folders.module';
// import { WorksToFolderModule } from './folders/works-to-folder/works-to-folder.module';
import { CreateNewFolderModule } from './folders/create-new-folder/create-new-folder.module';

import { AlertPerfilModule } from './alerts/alert-perfil/alert-perfil.module';
import { MiniPaginatorModule } from './alerts/mini-search-viewer/mini-paginator/mini-paginator.module';
import { SchedulesModule } from './schedule/schedule.module';
import { MiniSearchViewerModule } from './alerts/mini-search-viewer/mini-search-viewer.module';
import { WebshopModule } from './webshop/webshop.module';
import { AccountService } from './shared/services/account.service';
import { CommonService } from './shared/services/common.service';
import { UserBusinessModule } from './user/user-business/user-business.module';
import { LastUpdatesComponent } from './home/last-updates/last-updates.component';
import { HomeScheduleComponent } from './home/home-schedule/home-schedule.component';
import { ScheduleCardModule } from './schedule/schedule-card/schedule-card.module';
import { HomeActivityComponent } from './home/home-activity/home-activity.component';
import { HomeChartComponent } from './home/home-chart/home-chart.component';
import { HomeAlertComponent } from './home/home-alert/home-alert.component';
import { HomeAccountsComponent } from './home/home-accounts/home-accounts.component';
import { HomeCommentsComponent } from './home/home-comments/home-comments.component';
import { NgMasonryGridModule } from './shared/components/ng-masonry-grid';
import { MyBusinessComponent } from './my-business/my-business.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationService } from './shared/services/notification.service';
import { ContactService } from './shared/services/contact.service';

export function HttpLoaderFactory(http: HttpClient) {
  // return new TranslateHttpLoader(http, environment.apiUrl + environment.api.cadenas + '?request_locale=', '');
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [

    AppComponent,
    MenuComponent,
    MenuLateralComponent,

    HomeComponent,
    LoginComponent,
    AlertsComponent,

    ContactComponent,
    FaqComponent,
    SpinnerComponent,
    LastUpdatesComponent,
    HomeScheduleComponent,
    HomeActivityComponent,
    HomeChartComponent,
    HomeAlertComponent,
    HomeAccountsComponent,
    HomeCommentsComponent,
    MyBusinessComponent,
    NotificationsComponent,

  ],
  imports: [
    BrowserModule,
    SharedModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    UserModule,
    SearchModule,
    AlertsModule,
    FoldersModule,
    WebshopModule,
    SchedulesModule,
    AppRoutingModule,
    HttpModule,
    Ng2Webstorage,
    BrowserAnimationsModule,
    AlertPerfilModule,
    UserBusinessModule,
    SchedulesModule,
    MiniPaginatorModule,
    // WorksToFolderModule,
    CreateNewFolderModule,
    MiniSearchViewerModule,
    ScheduleCardModule,
    NgMasonryGridModule,
  ],

  entryComponents: [LoginComponent],
  providers: [
    TranslateService,
    SessionStorageService,

    LocalStorageService,
    SvsEventManager,
    customHttpProvider(),
    AccountService,
    CommonService,
    NotificationService,
    ContactService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
