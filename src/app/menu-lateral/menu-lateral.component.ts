import { Component, OnInit, Input } from '@angular/core';
import { Main } from '../shared/auth/main.service';
import { IFolderModal } from '../shared/models/folder.model';
import { SvsEventManager } from '../shared/event-manager/event-manager.service';
import { environment } from '../../environments/environment';
import { NotificationService } from '../shared/services/notification.service';
import { IMapPermisos } from '../shared/models/generic.model';

@Component({
  selector: 'c21-menu-lateral-component',
  templateUrl: './menu-lateral.component.html',
  styleUrls: ['./menu-lateral.component.scss']

})
export class MenuLateralComponent implements OnInit {
  @Input() disabled = 0;

  createNewFolderModal: IFolderModal = { visible: false };
  urlPrevVersion: string;
  notificationsCount?: number;
  isAuthenticatedValue = false;
  permisos: IMapPermisos;
  mouseOverClass = false;
  constructor(
    private main: Main,
    private notificationService: NotificationService,
    private eventManager: SvsEventManager
  ) {
    this.urlPrevVersion = environment.apiUrl + '/cliente/privado/home/ver.action';
  }

  ngOnInit() {
    this.initListener();
  }

  initListener() {
    this.eventManager.subscribe('notifications', () => {
      this.getNotifications();
    });
    this.eventManager.subscribe('authenticationSuccess', (_message) => {
      this.main.identity().then((account) => {
        // this.userName = account.usuario.nombre;
        this.permisos = account.permisos.mapPermisos;
      });
    });
  }
  isAuthenticated() {
    if (this.isAuthenticatedValue !== this.main.isAuthenticated()) {
      this.isAuthenticatedValue = !this.isAuthenticatedValue;
      if (this.isAuthenticatedValue) {
        this.getNotifications();
      }
    }
    return this.isAuthenticatedValue;
  }
  getNotifications() {
    this.notificationService.getNotificationsCount().then((notC) => {
      this.notificationsCount = notC;
    });
  }
  displayCreateNewFolder(show: boolean) {
    this.createNewFolderModal.carpeta = {
      codigo: 0
    };
    this.createNewFolderModal.visible = show;
  }
  closeNewFolder(codigo?: number) {
    this.eventManager.broadcast({
      name: 'newFolder',
      content: { codigo }
    });
  }

}
