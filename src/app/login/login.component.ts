import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { StateStorageService } from '../shared/auth';

@Component({
  selector: 'c21-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  authenticationError: boolean;
  password: string;
  rememberMe: boolean;
  username: string;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private stateStorageService: StateStorageService,
  ) {
  }
  login() {
    this.loginService.login({
      username: this.username,
      password: this.password,
      rememberMe: this.rememberMe
    }).then(() => {
      this.authenticationError = false;

      // previousState was set in the authExpiredInterceptor before being redirected to login modal.
      // since login is succesful, go to stored previousState and clear previousState
      const redirect = this.stateStorageService.getUrl();
      if (redirect) {
        this.stateStorageService.storeUrl(null);
        this.router.navigateByUrl(redirect);
      } else {
        this.router.navigate(['/home']);
      }
    }).catch(() => {
      this.authenticationError = true;
    });
  }
  ngOnInit() {
  }

}
