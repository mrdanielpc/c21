import { Injectable } from '@angular/core';
import { AuthServerProvider, Main } from '../shared/auth/';

@Injectable()
export class LoginService {
    constructor(
        private main: Main,
        private authServerProvider: AuthServerProvider
    ) { }
    login(credentials): Promise<any> {
        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe((data) => {
                if (!data) {
                    this.logout();
                    reject();
                }
                this.main.identity(true).then((account) => {

                    // After the login the language will be changed to
                    // the language selected by the user during his registration
                    if (account !== null) {
                        // this.languageService.changeLanguage(account.langKey);
                    }
                    resolve(data);
                });
            }, (err) => {
                this.logout();
                reject(err);
            });
        });
    }
    /*test() {
        return new Promise((resolve, reject) => {
            this.authServerProvider.test().subscribe(function oNext(result) {
                return this;
            });
        });
    }*/
    logout() {
        this.authServerProvider.logout().subscribe();
        this.main.authenticate(null);
    }

}
