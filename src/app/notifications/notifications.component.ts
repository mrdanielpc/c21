import { Component, OnInit } from '@angular/core';
import { IDropdown } from '../shared/models/generic.model';
import { NotificationService } from '../shared/services/notification.service';
import { Router } from '@angular/router';
import { INotifications } from '../shared/models/notification.model';
import { Main } from '../shared/auth';
import { SvsEventManager } from '../shared/event-manager/event-manager.service';

@Component({
  selector: 'c21-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  selectNotifications: IDropdown[];
  selectNotificationsSelected: IDropdown;
  notifications: INotifications[];
  lo: any;
  constructor(
    private notificationService: NotificationService,
    private eventManager: SvsEventManager,
    private router: Router,
    main: Main
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  ngOnInit() {
    this.notificationService.getNotificationsSelect().then(
      (lastW) => {
        this.selectNotifications = lastW;
        this.selectNotificationsSelected = lastW[0];
        this.getNotifications();
      }
    );
  }
  getNotifications() {
    this.notificationService.getNotifications(+this.selectNotificationsSelected.codigo).then((lw) => {
      this.notifications = lw;
      this.eventManager.broadcast({
        name: 'notifications',
        content: {}
      });
    });
  }
  onChange() {
    this.getNotifications();
  }
  redirectTo(path: string) {
    this.router.navigate([path]);

  }
}
