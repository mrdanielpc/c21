import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { IDropdown } from '../shared/models/generic.model';
import { AccountService } from '../shared/services/account.service';
import { UserBusinessComponent } from '../user/user-business/user-business.component';
import { IMyBusiness, ITraduccionesFicha, IImagen } from '../shared/models/account.model';
import { Main } from '../shared/auth';
import { FileUpload, ConfirmationService } from 'primeng/primeng';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'c21-my-business',
  templateUrl: './my-business.component.html',
  styleUrls: ['./my-business.component.scss']
})
export class MyBusinessComponent implements OnInit {

  selectAlerts: IDropdown[];
  selectAlertsSelected: IDropdown;
  @ViewChild('c21UserBusiness') c21UserBusiness: UserBusinessComponent;
  company: IMyBusiness;
  lo: any;
  providersType: IDropdown[];
  providersTypeSelected: IDropdown;

  uploadedFiles: File[] = [];
  uploadedImages: File[] = [];
  @ViewChild('fileUp') fileUp: FileUpload;
  @ViewChild('fileUpImages') fileUpImages: FileUpload;
  @ViewChild('myForm') myForm: NgForm;
  @ViewChild('resendForm') resendForm: NgForm;

  languages: IDropdown[];
  languageSelected: IDropdown;
  transBusiness: ITraduccionesFicha;
  resendVisible = false;
  email: string;
  constructor(
    private ref: ChangeDetectorRef,
    private accountService: AccountService,
    private translate: TranslateService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    main: Main
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });

  }

  ngOnInit() {
    this.accountService.getMyBusiness().then((myB) => {
      this.company = myB;

      this.ref.detectChanges();
      this.c21UserBusiness.getAfterBusiness(); this.accountService.getSelectProvidersType().then((pvd) => {
        this.providersType = pvd;
        if (this.company.codigoTipoProveedor) {
          this.providersTypeSelected = pvd.find((cProveedor) => cProveedor.codigo === this.company.codigoTipoProveedor);
        }
      });
      this.accountService.getSelectLanguagesBusiness().then(
        (idiom) => {
          this.languages = idiom;
          this.languageSelected = idiom[0];
          if (this.company.traduccionesFicha.length > 0) {
            this.transBusiness = this.company.traduccionesFicha.find((trd) => trd.codigoIdioma === this.languageSelected.codigo);
          }
        });
    });

  }
  onUploadLogo(event) {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }
  }

  myUploaderLogo() {
    if (this.uploadedFiles.length > 0) {
      this.accountService.saveLogo(this.uploadedFiles[0]).then((img) => {
        this.fileUp.clear();
        this.company.logoFicha = img;
        this.messageService.add({
          severity: 'success',
          summary: this.translate.instant('business.savedLogo.summary'),
          detail: this.translate.instant('business.savedLogo.detail')
        });
      });
    }
  }
  onUploadImage(event) {
    for (const file of event.files) {
      this.uploadedImages.push(file);
    }
  }
  myUploaderImage() {
    const promises: Promise<IImagen[]>[] = [];
    for (const file of this.uploadedImages) {
      promises.push(this.accountService.saveMyBusinessImg(file));
    }
    if (promises.length > 0) {
      Promise.all(promises).then((Im) => {
        this.company.imagenesFicha = Im[Im.length - 1];
        this.messageService.add({
          severity: 'success',
          summary: this.translate.instant('business.savedImage.summary'),
          detail: this.translate.instant('business.savedImage.detail')
        });
        this.fileUpImages.clear();
        this.uploadedImages.length = 0;
      }
      );
    }

  }

  langChange() {
    this.transBusiness = this.company.traduccionesFicha.find((trd) => trd.codigoIdioma === this.languageSelected.codigo);
  }

  save() {
    if (this.myForm.form.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('account.notification.company.invalid.summary'),
        detail: this.translate.instant('account.notification.company.invalid.detail')
      });
    } else {
      this.company.codigoTipoProveedor = +this.providersTypeSelected.codigo;
      this.accountService.saveMyBusiness(this.company).then((
        val) => {
        if (val) {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('business.save.summary'),
            detail: this.translate.instant('business.save.detail')
          });
        }
      }
      );
    }
  }

  deleteImg(imagen: IImagen) {
    this.confirmationService.confirm({
      header: this.translate.instant('business.deletedImage.header'),
      message: this.translate.instant('business.deletedImage.message'),
      icon: 'fa fa-trash',
      accept: () => {
        this.accountService.deleteMyBusinessImg(imagen.codigo).then((result) => {
          this.company.imagenesFicha = result;
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('business.deletedImage.summary'),
            detail: this.translate.instant('business.deletedImage.detail')
          });
        });
      },

    });

  }
  deleteLogo() {
    this.confirmationService.confirm({
      header: this.translate.instant('business.deletedLogo.header'),
      message: this.translate.instant('business.deletedLogo.message'),
      icon: 'fa fa-trash',
      accept: () => {
        this.accountService.deleteMyBusinessLogo().then(() => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('business.deletedLogo.summary'),
            detail: this.translate.instant('business.deletedLogo.detail')
          });
          this.company.logoFicha = undefined;
        });
      },

    });

  }
  openDialogSend() {
    this.resendVisible = true;
  }

  resend() {
    if (this.resendForm.form.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('business.resend.invalid.summary'),
        detail: this.translate.instant('business.resend.invalid.detail')
      });
    } else {
      this.accountService.resendMail(this.email).then(() => {
        this.messageService.add({
          severity: 'success',
          summary: this.translate.instant('business.resend.valid.summary'),
          detail: this.translate.instant('business.resend.valid.detail')
        });
        this.resendVisible = false;
      });
    }
  }
}
