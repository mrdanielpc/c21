import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { WebshopComponent } from './webshop/webshop.component';
import { FaqComponent } from './faq/faq.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { UserRouteAccessService } from './shared/auth';
import { AlertsComponent } from './alerts/alerts.component';

import { WorksheetComponent } from './search/worksheet/worksheet.component';
import { WebshopFinishComponent } from './webshop/webshop-finish/webshop-finish.component';
import { UserComponent } from './user/user.component';
import { MyBusinessComponent } from './my-business/my-business.component';
import { NotificationsComponent } from './notifications/notifications.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/login', canActivate: [UserRouteAccessService], pathMatch: 'full' },
    { path: 'login', component: LoginComponent, canActivate: [UserRouteAccessService] },
    { path: 'home', component: HomeComponent, canActivate: [UserRouteAccessService] },
    // { path: 'search', component: SearchComponent, canActivate: [UserRouteAccessService] },
    { path: 'alerts', component: AlertsComponent, canActivate: [UserRouteAccessService], },
    { path: 'alerts/:assistantDialog', component: AlertsComponent, canActivate: [UserRouteAccessService], },
    // { path: 'folders', component: FoldersComponent, canActivate: [UserRouteAccessService] },
    // { path: 'folders/:folder', component: FoldersComponent, canActivate: [UserRouteAccessService] },
    { path: 'schedule', component: ScheduleComponent, canActivate: [UserRouteAccessService] },
    { path: 'webshop/:step', component: WebshopComponent, canActivate: [UserRouteAccessService] },
    { path: 'webshop', component: WebshopComponent, canActivate: [UserRouteAccessService] },
    { path: 'thanks', component: WebshopFinishComponent, canActivate: [UserRouteAccessService] },
    { path: 'faq', component: FaqComponent, canActivate: [UserRouteAccessService] },
    { path: 'my-business', component: MyBusinessComponent, canActivate: [UserRouteAccessService] },
    { path: 'notifications', component: NotificationsComponent, canActivate: [UserRouteAccessService] },
    { path: 'contact', component: ContactComponent, canActivate: [UserRouteAccessService] },
    { path: 'work/:tipoElemento/:elemento', component: WorksheetComponent, canActivate: [UserRouteAccessService] },
    { path: 'user', component: UserComponent, canActivate: [UserRouteAccessService] },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes
            // ,{ enableTracing: true } // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
