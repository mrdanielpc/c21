import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { Main } from '../shared/auth/main.service';
import { SvsEventManager } from '../shared/event-manager/event-manager.service';
import { LoginService } from '../login/login.service';
import { IMapPermisos } from '../shared/models/generic.model';
import { NotificationService } from '../shared/services/notification.service';

@Component({
  selector: 'c21-menu-component',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  userName: string;
  menuSmall = false;
  @Input() disabled = 0;
  notificationsCount?: number;
  permisos: IMapPermisos;
  constructor(
    private router: Router,
    private loginService: LoginService,
    private notificationService: NotificationService,
    private eventManager: SvsEventManager,
    private main: Main,
  ) { }

  ngOnInit() {
    this.initListener();
    this.registerAuthenticationSuccess();

  }
  initListener() {
    this.eventManager.subscribe('notifications', () => {
      this.getNotifications();
    });
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 1024) {
      this.closeMenu();
    }
  }

  registerAuthenticationSuccess() {
    this.eventManager.subscribe('authenticationSuccess', (_message) => {
      this.main.identity().then((account) => {
        this.userName = account.usuario.nombre;
        this.permisos = account.permisos.mapPermisos;
        this.getNotifications();

      });
    });
  }
  toggleMenu() {
    this.menuSmall = !this.menuSmall;
  }
  /* loginModal() {
     //this.login.open();
   }*/
  isAuthenticated() {
    return this.main.isAuthenticated();
  }
  gotoMyAccount(): void {
    this.router.navigate(['/user-data', 11]);
  }
  logout() {
    this.loginService.logout();
    this.closeMenu();
    this.router.navigate(['']);
  }
  getImageUrl() {
    return this.isAuthenticated() ? this.main.getImageUrl() : null;
  }
  navigated() {
    this.closeMenu();
  }
  closeMenu() {
    this.menuSmall = false;
  }
  getNotifications() {
    this.notificationService.getNotificationsCount().then((notC) => {
      this.notificationsCount = notC;
    });
  }
}
