import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ISearchParams } from '../../shared/models/search-work.model';
import { HtmlService } from '../../shared/services/html.service';

@Component({
  selector: 'c21-search-filters',
  templateUrl: './search-filters.component.html',
  styleUrls: ['./search-filters.component.scss']
})
export class SearchFiltersComponent implements OnInit {
  @Input() filters: ISearchParams;
  @Output() onClick: EventEmitter<string> = new EventEmitter<string>();
  @Output() onClickArray: EventEmitter<any> = new EventEmitter<any>();
  hiddenFilters = [];
  fixedFilters = [
    'pagina',
    'codigoTipoElementoBusqueda',
    'codigoOrdenAscendenteDescendente',
    'codigoTipoOrden'
  ];
  constructor(
    public html: HtmlService,
  ) { }

  ngOnInit() {
  }
  swapHidden(key: string) {
    if (this.hiddenFilters.indexOf(key) > -1) {
      this.hiddenFilters = this.hiddenFilters.filter((str) => str !== key);
    } else {
      this.hiddenFilters.push(key);
    }
  }
  onFilterClick(key: string) {
    this.onClick.emit(key);
  }
  onFilterClickArray(key: string, val: any) {
    this.onClickArray.emit({ key, val });
  }
  checkFilterCollapse(key: string, parentName: string) {
    if (this.filters[key]) {
      return this.filters[key].filter((cd) => cd.parentName === parentName);
    } else { return false; }

  }
  getFiltersByParentName(key: string, parentName: string): any[] {
    if (this.filters[key]) {
      return this.filters[key].filter((cd) => cd.parentName === parentName);
    } else {
      return [];
    }

  }
}
