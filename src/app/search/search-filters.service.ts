
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { environment } from '../../environments/environment';

@Injectable()
export class SearchFiltersService {

  constructor(
    private http: Http
  ) { }
  getFiltroPerfilEstado(codigoTipoElementoBusqueda: number): Observable<any> {
    const params = {
      codigoTipoElementoBusqueda
    };
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.otrosPublicacionPerfilEstado, { params })
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }

  getTipoTrabajoObra(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.otrosTipoTrabajoObra)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getCodigoTipoClaseTrabajo(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.tiposClaseTrabajo)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getRangosSuperficieTerreno(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.selectRangosSuperficieTerreno)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }

  getRangosSuperficieConstruccion(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.selectRangosSuperficieConstruccion)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }

  getPalabrasClave(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.selectPalabrasClave)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getCodigoObraPublicaPrivada(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.tiposPublicaPrivada)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getCodigoTipoLicitacion(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.tiposLicitacion)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getGruposTipoObra(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.tipoGrupoObra)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }

  getGruposTipoObraExcluir(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.tipoGrupoObraExcluir)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getCodigoConOSin(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.codigoConOSin)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getDivisionEstatalMatriz(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.divisionEstatal)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
  getTipoProveedor(): Observable<any> {
    return this.http
      .get(environment.apiUrl + environment.api.obra.filtro.tipoProveedor)
      .map((data) => data.json())
      .catch((error) => observableThrowError(error));
  }
}
