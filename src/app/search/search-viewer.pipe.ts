import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({ name: 'viewerConverter' })
export class SearchViewerPipe implements PipeTransform {

    constructor(
        public translate: TranslateService
    ) { }

    transform(rowObj: any, colObj: any): any {
        let resultado: any;
        switch (colObj.field) {
            case 'localidad':
                resultado = rowObj.localidad.toUpperCase();
                break;
            // case 'fase':
            //     if (!!rowObj.fechaFase) {
            //         resultado = rowObj.fase + '<br>(' + rowObj.fechaFase + ')';
            //     }
            //     break;
            /*case 'fechaFase':
                resultado = new DatePipe('es-ES').transform(rowObj.fechaFase, 'dd/MM/yyyy');
                break;*/
            case 'nombre':
                if (!!rowObj.nombre) {
                    resultado = this.translate.get(rowObj.nombre);
                    resultado = resultado.value;
                }
                break;
            case 'promotora':
                if (!!rowObj.promotora) {
                    resultado = this.translate.get(rowObj.promotora);
                    resultado = resultado.value;
                }
                break;
            default:
                resultado = rowObj[colObj.field];
        }

        return resultado;
    }

}
