import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { SearchComponent } from './search.component';
import { UserRouteAccessService } from '../shared/auth';

@Injectable()
export class WorkFilterParams implements Resolve<any> {

  constructor() { }

  resolve(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot) {
    const param = Object.assign({}, route.queryParams);
    param['pagina'] = param['pagina'] ? param['pagina'] : '1';
    return param;
  }
}

const searchRoutes: Routes = [
  {
    path: 'search',
    component: SearchComponent,
    /*  children: [ // rutas hijas, se verán dentro del componente padre
        { path: '', redirectTo: '', pathMatch: 'full' }
      ]*/
    resolve: { 'filters': WorkFilterParams }, canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  providers: [WorkFilterParams],
  imports: [RouterModule.forChild(searchRoutes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
