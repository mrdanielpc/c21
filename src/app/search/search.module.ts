import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Frames
import { SharedModule } from '../shared/modules/shared.module';
import { VisibleTableColumnsModule } from '../shared/modals/visible-table-columns/visible-table-columns.module';
// Import  Routing
import { SearchRoutingModule } from './search-routing.module';

// Services
import { SearchWorkService } from '../shared/services/search-work.service';
import { BudgetRangesService } from '../shared/services/budget-ranges.service';
import { PhaseService } from '../shared/services/phase.service';

import { MarketService } from '../shared/services/market.service';
import { CountriesService } from '../shared/services/countries.service';
import { SearchFiltersService } from './search-filters.service';

import { WorksheetService } from '../shared/services/worksheet.service';

// Import  Component's
import { SearchComponent } from './search.component';

// Pipes
import { SearchViewerPipe } from './search-viewer.pipe';

import { AlertPerfilModule } from '../alerts/alert-perfil/alert-perfil.module';
import { WorksheetComponent } from './worksheet/worksheet.component';
import { WorksToFolderModule } from '../folders/works-to-folder/works-to-folder.module';
import { ScheduleEditorModule } from '../shared/components/schedule-editor/schedule-editor.module';
import { MiniPaginatorModule } from '../alerts/mini-search-viewer/mini-paginator/mini-paginator.module';
import { MiniSearchViewerModule } from '../alerts/mini-search-viewer/mini-search-viewer.module';
import { TranslateLangPipe } from '../shared/pipes/translateLang.pipe';
import { SearchFiltersComponent } from './search-filters/search-filters.component';
import { FilterParentNamePipe } from '../shared/pipes/filter.pipe';
import { FoldersRoutingModule } from '../folders/folders-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SearchRoutingModule,
    FoldersRoutingModule,
    AlertPerfilModule,
    WorksToFolderModule,
    VisibleTableColumnsModule,
    ScheduleEditorModule,
    MiniPaginatorModule,
    MiniSearchViewerModule
  ],
  declarations: [
    SearchComponent,
    SearchViewerPipe,
    TranslateLangPipe,
    FilterParentNamePipe,
    WorksheetComponent,
    SearchFiltersComponent
  ],
  providers: [
    SearchWorkService,
    BudgetRangesService,
    MarketService,
    CountriesService,
    PhaseService,
    SearchFiltersService,
    WorksheetService
  ],
  exports: [
    SearchComponent
  ]
})
export class SearchModule { }
