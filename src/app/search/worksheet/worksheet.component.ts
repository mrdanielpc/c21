import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';

import { WorksheetModel } from '../../shared/models/worksheet.model';
import { WorksheetService } from '../../shared/services/worksheet.service';
import { FolderService } from '../../shared/services/folder.service';
import { IWorksToFolder } from '../../shared/models/folder.model';
import { ScheduleEditorComponent } from '../../shared/components/schedule-editor/schedule-editor.component';
import { IScheduleModal, ISchedule } from '../../shared/models/schedule.model';
import { Main } from '../../shared/auth';
import { ILocale } from '../../shared/models/generic.model';
import { MiniSearchView } from '../../shared/models/search-work.model';
import { SearchWorkService } from '../../shared/services/search-work.service';
import { ConfirmationService } from 'primeng/primeng';
import { Observable } from 'rxjs';
import { ScheduleService } from '../../shared/services/schedule.service';

@Component({
  selector: 'c21-worksheet',
  templateUrl: './worksheet.component.html',
  styleUrls: ['./worksheet.component.scss']
})
export class WorksheetComponent implements OnInit {
  @ViewChild('notifyErrorForm') notifyErrorForm: NgForm;
  @ViewChild('c21ScheduleEditor') c21Schedule: ScheduleEditorComponent;
  @ViewChild('myAnnotations') myAnnotations: ElementRef;
  @ViewChild('myAnnotationsInput') myAnnotationsInput: ElementRef;
  showEdit: IScheduleModal = { visible: false };

  // Parametros de entrada
  worksheetParams: WorksheetModel.ISubmitParams = { codigoElemento: null, codigoTipoElemento: null };
  public miniSearchView: MiniSearchView.IMiniSearchView = { table: {}, paginator: {} };
  public miniSearchDialog = { display: false, title: null };

  worksTofolderModal: IWorksToFolder = {
    display: false
  };
  showSaveModal = false;
  prefijoLocale = 'es';
  getTrans: Observable<any>;
  emailUser = '';
  pdf: any;
  providerSelected: WorksheetModel.IProveedor;
  dataset = {
    languages: {
      list: null,
      selected: null,
      get: (): Promise<any> => {
        return this.worksheetService.getSelectLanguages().then((data) => {
          this.dataset.languages.list = data;
          this.main.identity().then((ident) => {
            this.dataset.languages.selected = this.dataset.languages.list.find(
              (idio) => idio.codigo === ident.usuario.idioma.codigo);
            this.prefijoLocale = this.dataset.languages.selected.prefijoLocale;
            this.getTrans = this.translate.getTranslation(this.prefijoLocale);
            this.emailUser = ident.usuario.email;
          });
        });
      }
    },
    work: {
      list: null,
      banner: null,
      myAnnotations: null,
      get: (): Promise<any> => {
        const params: WorksheetModel.ISubmitParams = {
          codigoElemento: this.worksheetParams.codigoElemento,
          codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
          request_only_locale: this.dataset.languages.selected.prefijoLocale
        };
        return this.worksheetService.getWorksheet(params).toPromise().then((data) => {
          if (!!data.banner) {
            this.dataset.work.banner = data.banner;
          }
          if (!!data.elemento) {
            this.dataset.work.list = data.elemento;

            //             (<WorksheetModel.IElementoWork>this.dataset.work.list).proveedores.forEach((provers) => {
            //               if(!!provers.agenda && !!provers.agenda.tooltip){
            // provers.agenda.tooltipFormated = provers.agenda.tooltip.split('<br/>');
            //               }
            //             });
            // Imagenes - 500x300.jpg - worksheet/1/54125623
            this.images = [];
            if (!!data.elemento.imagenes) {
              data.elemento.imagenes.forEach((element) => {
                this.images.push({
                  source: element.urlDescarga,
                  alt: element.fecha, title: element.nombre
                });
              });
            }
          }
        });
      },
      getTranslation: () => {
        this.prefijoLocale = this.dataset.languages.selected.prefijoLocale;
        this.getTrans = this.translate.getTranslation(this.prefijoLocale);
        this.dataset.work.get();
      }
    },
    notifyError: {
      display: false,
      message: null,
      email: null
    }
  };

  images: any[];
  lo: ILocale;
  texts: any = {};
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private worksheetService: WorksheetService,
    private folderService: FolderService,
    private messageService: MessageService,
    private translate: TranslateService,
    private main: Main,
    private searchWorkService: SearchWorkService,
    private scheduleService: ScheduleService,
    private confirmationService: ConfirmationService
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
  }

  // public getTranslationInTargetLanguage(key: string, language: string, interpolateParams: Object = null): Observable<any> {
  //   return this.translate.getTranslation(language)
  //     .map((res) => {
  //       for (const k of Object.keys(this.texts)) {
  //         let resp = res;
  //         k.split('.').forEach((asd) => resp = resp[asd]);
  //         this.texts[k] = resp;
  //       }
  //       // return this.translateParser.interpolate(resp, interpolateParams);
  //     });
  // }

  ngOnInit() {
    // Recogemos los parametros de la URL
    this.route.params.subscribe((params) => {
      if (params['tipoElemento'] != null) {
        this.worksheetParams.codigoTipoElemento = +params['tipoElemento'];
      }
      if (params['elemento'] != null) {
        this.worksheetParams.codigoElemento = +params['elemento'];
      }
      this.dataset.languages.get().then(() => {

        this.dataset.work.get();
        this.miniSearchDialog.display = false;
        window.scrollTo(0, 0);
      });
    });
    this.miniSearchView.table.columnas = this.searchWorkService.getMiniTableVisibleColumns();

    // this.getTranslationInTargetLanguage('menu.header.home', 'en').toPromise().then((data) => {
    //   (data);
    // });

  }
  // getTexts() {
  //   this.getTranslationInTargetLanguage('worksheet.menu.goBack', this.dataset.languages.selected.prefijoLocale).toPromise();
  // }
  //#region "menu"

  submitNotifyError() {
    // validate FORM
    const errorMsg = this.worksheetService.validFormNotifyError(this.notifyErrorForm);
    if (!!errorMsg.length) {
      errorMsg.forEach((msg) => {
        this.messageService.add({ severity: msg.severity, summary: msg.summary, detail: msg.detail });
      });
    } else {
      const params: WorksheetModel.ISubmitParams = {
        codigoElemento: this.dataset.work.list.codigo,
        codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
        comentario: this.dataset.notifyError.message,
        correoRespuesta: this.dataset.notifyError.email
      };

      this.worksheetService.sendError(params).toPromise().then((data) => {
        this.messageService.add({
          severity: 'success',
          summary: this.translate.instant('worksheet.modal.notifyError.notifications.summary'),
          detail: data.actionMessages[0]
        });
      });

      this.dataset.notifyError.display = false;
      this.dataset.notifyError.email = null;
      this.dataset.notifyError.message = null;
    }
  }

  updateStatus() {
    const params: WorksheetModel.ISubmitParams = {
      codigoElemento: this.dataset.work.list.codigo,
      codigoTipoElemento: this.worksheetParams.codigoTipoElemento
    };
    this.worksheetService.updateStatus(params).toPromise().then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('worksheet.modal.updateStatus.notifications.summary'),
        detail: this.translate.instant('worksheet.modal.updateStatus.notifications.detail')
      });
      this.dataset.work.get();
    });
  }

  goToAnchor() {
    this.myAnnotations.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });

    this.myAnnotationsInput.nativeElement.focus();

  }
  goBack() {
    this.location.back();
  }
  addWorksToFolder() {
    // FAKE: la carpeta 'TODAS' se muestra?, si no hay carpetas que se muestra en su lugar
    this.displayWorksToFolder(true);
    this.worksTofolderModal.codigosElementosSelecionados = this.folderService.concatenateFolderWorkCodes(this.worksheetParams.codigoTipoElemento, [this.dataset.work.list.codigo]);
  }

  displayWorksToFolder(show: boolean) {
    this.worksTofolderModal.display = show;
  }

  setMyCompanyParticipates() {
    const params: { codigoElemento: number, codigoTipoElemento: number } = {
      codigoElemento: this.dataset.work.list.codigo,
      codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
    };
    this.worksheetService.setMyCompanyParticipates(params).then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('worksheet.myCompanyParticipates.notifications.summary'),
        detail: this.translate.instant('worksheet.myCompanyParticipates.notifications.detail')
      });
      this.dataset.work.get();
    });
  }

  printWorksheet() {
    window.print();
  }
  deleteAppointment(scheduleCode: number) {
    this.confirmationService.confirm({
      header: this.translate.instant('schedule.card.delete.header'),
      message: this.translate.instant('schedule.card.delete.message'),
      icon: 'fa fa-trash',
      accept: () => {
        this.scheduleService.delete(scheduleCode).then((data) => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('schedule.card.delete.header'),
            detail: data.actionMessages[0]
          });
          this.dataset.work.get();
        });
      },

    });
  }
  generatePdf() {
    const params: { codigoElemento: number, codigoTipoElemento: number } = {
      codigoElemento: this.dataset.work.list.codigo,
      codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
    };
    this.worksheetService.generatePdf(params).toPromise().then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('worksheet.myAnnotations.saveNotification.summary'),
        detail: this.translate.instant('worksheet.myAnnotations.saveNotification.detail')
      });
      this.showSaveModal = false;
    });
  }

  generateRtf() {
    const params: { codigoElemento: number, codigoTipoElemento: number } = {
      codigoElemento: this.dataset.work.list.codigo,
      codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
    };
    this.worksheetService.generateRtf(params).toPromise().then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('worksheet.myAnnotations.saveNotification.summary'),
        detail: this.translate.instant('worksheet.myAnnotations.saveNotification.detail')
      });
      this.showSaveModal = false;
    });
  }

  //#endregion "menu"

  //#region "Comentario"
  saveComment() {
    // Guardar Comentario
    const params: WorksheetModel.IWorkComment = {
      codigoElemento: this.dataset.work.list.codigo,
      codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
      comentario: this.dataset.work.myAnnotations
    };
    this.worksheetService.setComment(params).toPromise().then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('worksheet.myAnnotations.notifications.summary'),
        detail: this.translate.instant('worksheet.myAnnotations.notifications.detail')
      });
      this.dataset.work.myAnnotations = '';
      this.dataset.work.get();
    });
  }
  editComment(commentObj) {
    const params: WorksheetModel.IWorkComment = {
      codigoElemento: this.dataset.work.list.codigo,
      codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
      comentario: commentObj.comentario,
    };
    params['codigo'] = commentObj.codigo;
    this.worksheetService.setComment(params).toPromise().then(() => {
      this.messageService.add({
        severity: 'success',
        summary: this.translate.instant('worksheet.myAnnotations.notifications.summary'),
        detail: this.translate.instant('worksheet.myAnnotations.notifications.detail')
      });
      this.dataset.work.myAnnotations = '';
      this.dataset.work.get();
    });
  }
  removeComment(commentObj) {

    this.confirmationService.confirm({
      header: this.translate.instant('worksheet.myAnnotations.removeDialog.header'),
      message: this.translate.instant('worksheet.myAnnotations.removeDialog.message'),
      accept: () => {
        const params: { codigoElemento: number, codigoTipoElemento: number, codigo: number } = {
          codigoElemento: this.dataset.work.list.codigo,
          codigoTipoElemento: this.worksheetParams.codigoTipoElemento,
          codigo: commentObj.codigo
        };
        this.worksheetService.removeComment(params).toPromise().then(() => {
          this.messageService.add({
            severity: 'success',
            summary: this.translate.instant('worksheet.myAnnotations.removeDialog.summary'),
            detail: this.translate.instant('worksheet.myAnnotations.removeDialog.detail')
          });
          this.dataset.work.myAnnotations = '';
          this.dataset.work.get();
        });
      }
    });

    // Eliminar Comentario
  }
  //#endregion "Comentario"

  //#region "proveedor"
  openScheduleAppointment(proveedor: any) {
    if (!proveedor.agenda) {
      const schedule: ISchedule = {};
      // schedule.telefonos = proveedor.stringTelefonos;
      // schedule.avisoEmail = true;
      schedule.codigoProveedor = proveedor.codigo;
      schedule.codigoElemento = this.worksheetParams.codigoElemento;
      schedule.codigoTipoElemento = this.worksheetParams.codigoTipoElemento;
      schedule.obra = this.dataset.work.list;
      // schedule.tipoAgenda = { codigo: 0 };
      // if (!!proveedor.personasContacto && proveedor.personasContacto.length > 0) {
      //   schedule.contacto = proveedor.personasContacto[0].nombre;
      // }

      this.showEdit.schedule = Object.assign({}, schedule);
    } else {
      proveedor.agenda.fechaAviso = new Date(proveedor.agenda.fechaAviso);
      this.showEdit.schedule = Object.assign({}, proveedor.agenda);
    }
    this.c21Schedule.selectAction();
    this.showEdit.visible = true;
  }

  refreshMiniSearchViewer(page: number) {
    this.openWorksHistory(this.providerSelected, page);
  }
  openWorksHistory(proveedor: WorksheetModel.IProveedor, page = 1) {
    this.providerSelected = proveedor;
    const params = {
      'pagina': page,
      'codigoProveedor': proveedor.codigo
    };
    this.searchWorkService
      .getWorksByProvider(params)
      .then((data) => {
        // Datos Grid
        this.miniSearchView.table.elementos = data.elementos;
        // Paginator
        this.miniSearchView.paginator.pagina = data.model.pagina;
        this.miniSearchView.paginator.numeroPaginas = data.model.numeroPaginas;
        this.miniSearchView.paginator.numeroResultados = data.model.numeroResultados;
        // Dialog

        this.miniSearchDialog.title = this.translate.instant('alerts.send.alertListSent.modal.title');
        this.miniSearchDialog.display = true;
      });

  }
  //#endregion "proveedor"
  imgSelected = 0;

  selectImage(idx: number): void {
    this.imgSelected = idx;
  }

  onSaveSchedule() {
    this.dataset.work.get();
  }

  showSave() {
    this.showSaveModal = true;
  }

  isActiveImage(idx: number): boolean {
    return this.imgSelected === idx;
  }
}
