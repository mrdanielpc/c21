import { Component, Input, OnDestroy, OnInit, HostListener, Output, EventEmitter, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ConfirmationService, Dropdown } from 'primeng/primeng';
import { HtmlService } from '../shared/services/html.service';

import { BudgetRangesModel, IBudgetRangesSelected } from '../shared/models/budget-ranges.model';
import { IPaginator, ISearchParams, SearchWorkModel, IWorkColumns } from '../shared/models/search-work.model';

import { CountryModel } from '../shared/models/country.model';
import { MarketModel } from '../shared/models/market.model';
import { WorksheetModel } from '../shared/models/worksheet.model';

import { BudgetRangesService } from '../shared/services/budget-ranges.service';
import { PhaseService } from '../shared/services/phase.service';
import { SearchWorkService, codigoOrdenAscendenteDescendente } from '../shared/services/search-work.service';

import { SvsEventManager } from '../shared/event-manager/event-manager.service';
import { CountriesService } from '../shared/services/countries.service';
import { MarketService } from '../shared/services/market.service';

import { MessageService } from 'primeng/components/common/messageservice';
import { SearchFiltersService } from './search-filters.service';

import * as moment from 'moment';

// Pruebas ConfirmDialog
import { Alert } from '../shared/models/alert.model';
import { AlertService } from '../shared/services/alert.service';
import { NgForm } from '@angular/forms';
import { IFolders, IWorksToFolder } from '../shared/models/folder.model';
import { FolderService } from '../shared/services/folder.service';
import { VisibleTableColumnsModel } from '../shared/models/visible-table-columns.model';
import { Main } from '../shared/auth';
import { IDropdown } from '../shared/models/generic.model';
import { Subject } from 'rxjs';

// subMenuButtons
export enum ViewList {
  search = 0,
  localization = 1,
  typeOfWork = 2,
  phaseWorks = 30,
  phaseTenders = 31,
  company = 4,
  filterBudget = 5,
  others = 6
}

@Component({
  selector: 'c21-search-component',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [ConfirmationService]
})
export class SearchComponent implements OnInit, OnDestroy {
  @ViewChild('formAlertPerfil') formAlertPerfil: NgForm;
  formAlertPerfilValid: any;

  @Input() isEditAlert = false;
  @Output() alertSaved = new EventEmitter();
  view = ViewList;
  worksSelection: any;
  popupMode = false;
  @Input() newAlertData: Alert = new Alert();
  dialogExport = false;
  dialogAddToFolder = false;
  worksheetParams: WorksheetModel.ISubmitParams;
  checkedAll = false;
  enumCodigoOrdenAscendenteDescendente: codigoOrdenAscendenteDescendente;
  visibleTableColumnsData: VisibleTableColumnsModel.IIncoming = {
    display: false,
    cookieName: 'worksVisibleColumns',
    availableColumns: [],
    defaultVisibleColumns: []
  };
  worksTofolderModal: IWorksToFolder = {
    display: false
  };
  txtSearchChanged: Subject<string> = new Subject<string>();

  // visibleColumnsConfDisplay = false;
  foldersList: IFolders[];
  sortingColumn: IWorkColumns;
  sortingDirection: IDropdown;
  sortingList: IDropdown[];
  menuItems = {
    ITEMS: [
      {
        code: 'WORKS', label: this.translate.instant('comun.text.works').toUpperCase(), codigo: 1, alertValid: 1, actions: [

          ViewList.localization,
          'location',
          ViewList.typeOfWork,
          'type-of-work',
          ViewList.company,
          'company',
          ViewList.filterBudget,
          'budget',
          ViewList.phaseWorks,
          'phasework',
          ViewList.others,
          'other'],
        otherActions: ['publication',
          'localizationPrecise',
          'workIdentification',
          'typeOfWork',
          'workSurface',
          'roomsAndFloors',
          'excludeTypesOfWorks',
          'keywords',
          'keywordsTags']
      },
      {
        code: 'PROVIDERS',
        label: this.translate.instant('comun.text.suppliers').toUpperCase(),
        alertValid: 0,
        codigo: 3,
        actions: [
          ViewList.localization,
          'location',
          ViewList.typeOfWork,
          'type-of-work',
          ViewList.company,
          'company',
          ViewList.filterBudget,
          'budget',
          ViewList.others,
          'other'],
        otherActions: ['publication', 'contactInformation']
      },
      {
        code: 'TENDERS', label: this.translate.instant('comun.text.tenders').toUpperCase(),
        alertValid: 1,
        codigo: 2, actions: [
          ViewList.localization,
          'location',
          ViewList.typeOfWork,
          'type-of-work',
          ViewList.company,
          'company',
          ViewList.filterBudget,
          'budget',
          ViewList.phaseTenders,
          'phasetender',
          ViewList.others,
          'other'],
        otherActions: ['publication',
          'identificationOfContest',
          'classification',
          'classWork',
          'typeOfTenders',
          'keywords',
          'keywordsExclude']
      }
    ]
  };

  public phases: any;
  public phasesTender: any;
  public saveAlert = false;
  public filters = {

    empresa: {
      tipoProveedor: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getTipoProveedor().toPromise().then((data) => {
            this.filters.empresa.tipoProveedor.list = data.result;
          });
        }
      },

      codigosDivisionEstatalMatriz: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getDivisionEstatalMatriz().toPromise().then((data) => {
            this.filters.empresa.codigosDivisionEstatalMatriz.list = data.result;
          });
        }
      }
    },
    tipoDeObra: {
      codigoObraPublicaPrivada: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getCodigoObraPublicaPrivada().toPromise().then((data) => {
            this.filters.tipoDeObra.codigoObraPublicaPrivada.list = data.result;
          });
        }
      },
      codigoTipoLicitacion: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getCodigoTipoLicitacion().toPromise().then((data) => {
            this.filters.tipoDeObra.codigoTipoLicitacion.list = data.result;
          });
        }
      },
      tipoObra: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getGruposTipoObra().toPromise().then((data) => {
            this.createParentOnChilds(data.result);
            this.filters.tipoDeObra.tipoObra.list = data.result;
          });
        }
      },
    },
    otros: {
      codigoConOSin: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getCodigoConOSin().toPromise().then((data) => {
            this.filters.otros.codigoConOSin.list = data.result;
          });
        }
      },
      codigoFiltroPerfilEstado: {
        list: null,
        get: (codigoFiltroPerfilEstado?: number): Promise<any> => {
          if (!codigoFiltroPerfilEstado) {
            codigoFiltroPerfilEstado = this.filtersParams.codigoTipoElementoBusqueda.codigo;
          }
          return this.searchFiltersService.getFiltroPerfilEstado(codigoFiltroPerfilEstado).toPromise().then((data) => {
            this.filters.otros.codigoFiltroPerfilEstado.list = data.result;
          });
        }
      },
      codigoTipoTrabajoObra: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getTipoTrabajoObra().toPromise().then((data) => {
            this.filters.otros.codigoTipoTrabajoObra.list = data.result;
          });
        }
      },
      codigoTipoClaseTrabajo: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getCodigoTipoClaseTrabajo().toPromise().then((data) => {
            this.filters.otros.codigoTipoClaseTrabajo.list = data.result;
          });
        }
      },
      codigoRangoSuperficieTerreno: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getRangosSuperficieTerreno().toPromise().then((data) => {
            this.filters.otros.codigoRangoSuperficieTerreno.list = data.result;
          });
        }
      },
      codigoRangoSuperficieConstruccion: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getRangosSuperficieConstruccion().toPromise().then((data) => {
            this.filters.otros.codigoRangoSuperficieConstruccion.list = data.result;
          });
        }
      },
      selectPalabrasClave: {
        list: null,
        get: (): Promise<any> => {
          return this.getPalabrasClave();
        },
        l1: {
          list: null,
          get: (): Promise<any> => {
            return this.getPalabrasClave();
          },
        },
        l2: {
          list: null,
          get: (): Promise<any> => {
            return this.getPalabrasClave();
          },
        },
        l3: {
          list: null,
          get: (): Promise<any> => {
            return this.getPalabrasClave();
          },
        }
      },
      codigoTipoObraExcluir: {
        list: null,
        get: (): Promise<any> => {
          return this.searchFiltersService.getGruposTipoObraExcluir().toPromise().then((data) => {
            this.createParentOnChilds(data.result);
            this.filters.otros.codigoTipoObraExcluir.list = data.result;
          });
        }

      }
    }
  };

  public isFirstPage = true;
  public isLastPage = false;
  public columnsUserConfig: any[];
  public columnsSelectedUser: any;
  public columnsAvailableTable: any[] = [];
  public columnsVisibleTable: IWorkColumns[] = [];

  public filtersParams: ISearchParams = {};

  public filtersParamsCopy: ISearchParams = {};
  private filtersParamsAux: ISearchParams = {};

  paginator: IPaginator;
  constructionProvidersElemento: SearchWorkModel.Elemento[];
  currentViewMosaic = false;
  budget: BudgetRangesModel.Result[];
  selectedElements: IBudgetRangesSelected = { selectedFrom: null, selectedTo: null, zeroBudget: false };

  // mosaic
  mosaicSortedBy: { field: string, order: number };

  public viewSearch: ViewList = this.view.search;

  //
  //#region "LOCALIZATION Variables"
  public localizationDS: any = {};
  public localizationDScopy: any = {};
  public markets: MarketModel.Result[];

  public selectedCountries: number[] = [];
  public checkSelectedAllCountries: string;
  public selectedCommunities: number[] = [];
  public checkSelectedAllCommunities: string;
  //#endregion "LOCALIZATION Variables"

  lo: any;

  constructor(
    public html: HtmlService,
    private translate: TranslateService,
    private searchWorkService: SearchWorkService,
    private budgetRangesService: BudgetRangesService,
    private marketService: MarketService,
    private countriesService: CountriesService,
    private phaseService: PhaseService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventManager: SvsEventManager,
    private searchFiltersService: SearchFiltersService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private alertService: AlertService,
    private folderService: FolderService,
    main: Main
  ) {
    main.identity().then((ident) => {
      this.lo = ident.localeData;
    });
    this.sortingList = [{
      codigo: 1,
      nombre: this.translate.instant('search.mosaic.order.up')
    }, {
      codigo: -1,
      nombre: this.translate.instant('search.mosaic.order.down')
    }];
    this.paginator = {
      pagina: 1,
      numeroPaginas: 1,
      resultadosPagina: 0,
      numeroResultados: 0,
      sortingColumnCode: null,
      sortingDirection: null
    };
    this.filtersParams = { pagina: 1 };
    this.activatedRoute.data.subscribe((data) => {
      const params = <ISearchParams>data['filters'];
      this.checkParams(params);

      if (!!data.filters && data.filters.currentViewMosaic === 'true') {
        this.currentViewMosaic = true;
      }
    }
    ).unsubscribe();

    this.txtSearchChanged
      .debounceTime(300) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe((model) => {
        this.filtersParams['palabrasClave_0'] = model;
        this.search();
      });

  }

  checkParams(params: ISearchParams) {
    if (!params) {
      params = { pagina: 1, codigoTipoElementoBusqueda: 1 };
    }
    this.filtersParams.pagina = params.pagina;
    if (!!params.codigoTipoElementoBusqueda) {
      this.filtersParams.codigoTipoElementoBusqueda = this.menuItems.ITEMS.find(
        (item) => item.codigo === +params.codigoTipoElementoBusqueda);
    } else {
      this.filtersParams.codigoTipoElementoBusqueda = this.menuItems.ITEMS.find(
        (item) => item.codigo === 1);
    }
    this.filtersParams.codigoOrdenAscendenteDescendente = params.codigoOrdenAscendenteDescendente;
    this.filtersParams.codigoTipoOrden = params.codigoTipoOrden;

    this.checkParamDate(params, 'fecha');
    this.checkParamDate(params, 'fechaHasta');

    this.checkParamCombo(params, 'codigoFiltroPerfilEstado', this.filters.otros.codigoFiltroPerfilEstado);
    this.checkParamCombo(params, 'codigoConOSinEmail', this.filters.otros.codigoConOSin);

    this.checkParamCombo(params, 'codigoConOSinIngeniero', this.filters.otros.codigoConOSin);
    this.checkParamCombo(params, 'codigoConOSinArquitecto', this.filters.otros.codigoConOSin);
    this.checkParamCombo(params, 'codigoConOSinContratista', this.filters.otros.codigoConOSin);

    this.checkParamCombo(params, 'codigoTipoTrabajoObra', this.filters.otros.codigoTipoTrabajoObra);
    this.checkParamCombo(params, 'codigoTipoClaseTrabajo', this.filters.otros.codigoTipoClaseTrabajo);
    this.checkParamCombo(params, 'codigoRangoSuperficieTerreno', this.filters.otros.codigoRangoSuperficieTerreno);
    this.checkParamCombo(params, 'codigoRangoSuperficieConstruccion', this.filters.otros.codigoRangoSuperficieConstruccion);

    this.checkParamCombo(params, 'codigosEtiquetasPalabraClave1', this.filters.otros.selectPalabrasClave.l1);
    this.checkParamCombo(params, 'codigosEtiquetasPalabraClave2', this.filters.otros.selectPalabrasClave.l2);
    this.checkParamCombo(params, 'codigosEtiquetasPalabraClave3', this.filters.otros.selectPalabrasClave.l3);

    this.checkParamCombo(params, 'codigosDivisionEstatalMatriz', this.filters.empresa.codigosDivisionEstatalMatriz);

    this.checkParamMultiCheckSubTipes(params, 'codigoTipoObra', this.filters.tipoDeObra.tipoObra);
    this.checkParamMultiCheckSubTipes(params, 'codigoTipoObraExcluir', this.filters.otros.codigoTipoObraExcluir);
    this.checkParamMultiCheck(params, 'codigoTipoLicitacion', this.filters.tipoDeObra.codigoTipoLicitacion);

    this.checkParamMultiCheck(params, 'codigoObraPublicaPrivada', this.filters.tipoDeObra.codigoObraPublicaPrivada);

    this.checkParamMultiCheck(params, 'codigosTipoProveedor', this.filters.empresa.tipoProveedor);

    this.checkParamText(params, 'referencia');
    this.checkParamText(params, 'nombreObra');
    this.checkParamText(params, 'codigoPostal');
    this.checkParamText(params, 'codigoPostalMaximo');
    this.checkParamText(params, 'localidad');
    this.checkParamText(params, 'presupuestoCero');
    this.checkParamText(params, 'incluirClasificacionNoRequerida');
    this.checkParamText(params, 'numeroViviendasMinimo');
    this.checkParamText(params, 'numeroViviendasMaximo');
    this.checkParamText(params, 'numeroPlantasMinimo');
    this.checkParamText(params, 'numeroPlantasMaximo');
    this.checkParamText(params, 'organismoLicitador');
    this.checkParamText(params, 'adjudicatario');
    this.checkParamText(params, 'nombreProveedor');

    this.checkParamText(params, 'clasificacionGrupo');
    this.checkParamText(params, 'clasificacionSubgrupo');
    this.checkParamText(params, 'clasificacionCategoria');
    this.checkParamText(params, 'denominacionBoletin');

    this.checkParamArrayText(params, 'palabrasClave', 4);
    this.checkParamArrayText(params, 'palabrasClaveExcluir', 3);

    if (!!params.codigoFaseLicitacion) {
      if (typeof params.codigoFaseLicitacion === 'string') {
        this.filtersParams.codigoFaseLicitacion = [
          { codigo: +params.codigoFaseLicitacion, nombre: params.codigoFaseLicitacion }];
      } else {
        this.filtersParams.codigoFaseLicitacion = [];
        for (const valCod of params.codigoFaseLicitacion) {
          this.filtersParams.codigoFaseLicitacion.push({ codigo: +valCod, nombre: valCod });
        }
      }
      this.getPhasesTender().then(() => {
        this.filtersParamsAux = Object.assign({});
        this.filtersParamsAux.codigoFaseLicitacion = [];

        for (const val of this.filtersParams.codigoFaseLicitacion) {
          const found = this.phasesTender.find((sub) => sub.codigo === +val.codigo);
          if (!!found) {
            this.filtersParamsAux.codigoFaseLicitacion.push(found);
          }
        }
        this.filtersParams.codigoFaseLicitacion = this.filtersParamsAux.codigoFaseLicitacion;

      });

    }

    if (!!params.codigoTipoHitoObra) {
      if (typeof params.codigoTipoHitoObra === 'string') {
        this.filtersParams.codigoTipoHitoObra = [{
          codigo: +params.codigoTipoHitoObra,
          nombre: params.codigoTipoHitoObra
        }];
      } else {
        this.filtersParams.codigoTipoHitoObra = [];
        for (const valCod of params.codigoTipoHitoObra) {
          this.filtersParams.codigoTipoHitoObra.push({ codigo: +valCod, nombre: valCod });
        }
      }

      this.getPhases().then(() => {

        this.filtersParamsAux = Object.assign({});
        this.filtersParamsAux.codigoTipoHitoObra = [];

        for (const val of this.filtersParams.codigoTipoHitoObra) {
          for (const valPhase of Object.keys(this.phases)) {
            const found = this.phases[valPhase].subTipos.find((sub) => sub.codigo === +val.codigo);

            if (!!found) {
              this.filtersParamsAux.codigoTipoHitoObra.push(found);
            }

          }
        }
        this.filtersParams.codigoTipoHitoObra = this.filtersParamsAux.codigoTipoHitoObra;

      });
    }
    let promiseCountry: Promise<any>;
    if (!!params.codigoMercado) {
      params.codigoMercado = +params.codigoMercado;
      this.filtersParams.codigoMercado = params.codigoMercado;
      this.getMarket().then(() => {
        this.filtersParams['codigoMercado'] = this.markets.find((bud) => bud.codigo === params.codigoMercado);
        promiseCountry = this.getAllCountries(this.filtersParams.codigoMercado.codigo);

      });

    }

    if (!!params.codigosPais) {
      if (typeof params.codigosPais === 'string') {
        this.filtersParams.codigosPais = [{ codigo: +params.codigosPais, nombre: params.codigosPais }];
      } else {
        this.filtersParams.codigosPais = [];
        for (const valCod of params.codigosPais) {
          this.filtersParams['codigosPais'].push({ codigo: +valCod, nombre: valCod });
        }
      }
      if (!promiseCountry) {
        promiseCountry = this.getAllCountries([]);
      }
      promiseCountry.then(() => {
        let paramCodigoPais;
        paramCodigoPais = !Array.isArray(params.codigosPais) ? [+params.codigosPais] : params.codigosPais.map(Number);
        this.getCountriesCommunities(paramCodigoPais).then
          (() => {
            this.filtersParamsAux = Object.assign({});
            this.filtersParamsAux['codigosPais'] = [];
            if (!!params.codigosDivisionEstatalPrimerNivel) {
              this.filtersParamsAux['codigosDivisionEstatalPrimerNivel'] = [];
            }
            if (!!params.codigosDivisionEstatalSegundoNivel) {
              this.filtersParamsAux['codigosDivisionEstatalSegundoNivel'] = [];
            }
            for (const val of this.filtersParams.codigosPais) {
              if (!!params.codigosDivisionEstatalPrimerNivel) {
                for (const valPrimerNivel of this.filtersParams.codigosDivisionEstatalPrimerNivel) {
                  const valCom = this.localizationDS.countries[val.codigo].communities[valPrimerNivel.codigo];
                  if (!!valCom) {
                    this.filtersParamsAux['codigosDivisionEstatalPrimerNivel'].push(valCom);
                    if (!!params.codigosDivisionEstatalSegundoNivel) {
                      for (const valSegundoNivel of this.filtersParams.codigosDivisionEstatalSegundoNivel) {
                        const valSubtypes = this.localizationDS.countries[val.codigo].
                          communities[valPrimerNivel.codigo].subTipos;
                        if (!!valSubtypes) {
                          const valProv = this.localizationDS.countries[val.codigo].
                            communities[valPrimerNivel.codigo].subTipos[valSegundoNivel.codigo];
                          if (!!valProv) {
                            this.filtersParamsAux['codigosDivisionEstatalSegundoNivel'].push(valProv);
                          }
                        }

                      }
                    }
                  }
                }
              }

              this.filtersParamsAux['codigosPais'].push(this.localizationDS.countries[val.codigo]);
            }
            this.filtersParams.codigosPais = this.filtersParamsAux.codigosPais;
            this.filtersParams.codigosDivisionEstatalPrimerNivel = this.filtersParamsAux.codigosDivisionEstatalPrimerNivel;
            this.filtersParams.codigosDivisionEstatalSegundoNivel = this.filtersParamsAux.codigosDivisionEstatalSegundoNivel;
          }

          );
      });
      if (!!params.codigosDivisionEstatalPrimerNivel) {
        if (typeof params.codigosDivisionEstatalPrimerNivel === 'string') {
          this.filtersParams.codigosDivisionEstatalPrimerNivel = [
            {
              codigo: +params.codigosDivisionEstatalPrimerNivel,
              nombre: params.codigosDivisionEstatalPrimerNivel
            }];
        } else {
          this.filtersParams.codigosDivisionEstatalPrimerNivel = [];
          for (const valCod of params.codigosDivisionEstatalPrimerNivel) {
            this.filtersParams.codigosDivisionEstatalPrimerNivel.push({ codigo: +valCod, nombre: valCod });
          }
        }
        if (!!params.codigosDivisionEstatalSegundoNivel) {
          if (typeof params.codigosDivisionEstatalSegundoNivel === 'string') {
            this.filtersParams.codigosDivisionEstatalSegundoNivel = [{
              codigo: +params.codigosDivisionEstatalSegundoNivel,
              nombre: params.codigosDivisionEstatalSegundoNivel
            }];
          } else {
            this.filtersParams.codigosDivisionEstatalSegundoNivel = [];
            for (const valCod of params.codigosDivisionEstatalSegundoNivel) {
              this.filtersParams.codigosDivisionEstatalSegundoNivel.push({ codigo: +valCod, nombre: valCod });
            }
          }
        }
      }

    }
    let getBudget: Promise<any>;
    if (!!params.presupuestoMin) {
      this.filtersParams['presupuestoMin'] = params.presupuestoMin;

      getBudget = this.getBudgetRanges().then(() => {

        this.filtersParams['presupuestoMin'] = this.budget.find((bud) => bud.codigo === +params.presupuestoMin);
      });
    }
    if (!!params.presupuestoMax) {
      this.filtersParams['presupuestoMax'] = params.presupuestoMax;
      if (!!getBudget) {
        getBudget.then(() => {
          this.filtersParams.presupuestoMax = this.budget.find((bud) => bud.codigo === +params.presupuestoMax);
        });
      } else {
        this.getBudgetRanges().then(() => {
          this.filtersParams.presupuestoMax = this.budget.find((bud) => bud.codigo === +params.presupuestoMax);
        });
      }
    }
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler() {
    if (!this.isEditAlert) {
      this.onFilterView(0);
    }
  }
  checkParamDate(param: any, key: string) {
    if (!!param[key]) {
      if (this.html.isDate(param[key])) {
        this.filtersParams[key] = moment(param[key]).toDate();
      }
    }
  }

  onChangeSearchKey() {
    this.search();
  }
  onChangeSearchKeyText(text: string) {
    this.txtSearchChanged.next(text);
  }

  getPalabrasClave() {
    return this.searchFiltersService.getPalabrasClave().toPromise().then((data) => {
      this.filters.otros.selectPalabrasClave.list = data.result;
      this.filters.otros.selectPalabrasClave.l1.list = data.result[0].subTipos;
      this.filters.otros.selectPalabrasClave.l2.list = data.result[1].subTipos;
      this.filters.otros.selectPalabrasClave.l3.list = data.result[2].subTipos;
    });
  }
  checkParamText(param: any, key: string) {
    if (!!param[key]) {
      this.filtersParams[key] = param[key];
    }
  }
  checkParamArrayText(param: any, key: string, max: number) {
    if (!!param[key]) {
      if (typeof param[key] === 'string' || typeof param[key] === 'number') {
        this.filtersParams[key + '_0'] = param[key];
      } else {
        let n = 0;
        for (const val of param[key]) {
          this.filtersParams[key + '_' + n] = val;
          n++;
          if (n === max) {
            break;
          }
        }
      }
    }
  }
  checkParamMultiCheck(param: any, key: string, itemCode: { list: any, get: () => Promise<any> }) {
    if (!!param[key]) {
      if (typeof param[key] === 'string' || typeof param[key] === 'number') {
        this.filtersParams[key] = [{ codigo: +param[key], nombre: param[key] }];
      } else {
        this.filtersParams[key] = [];
        for (const valCod of param[key]) {
          this.filtersParams[key].push({ codigo: +valCod, nombre: valCod });
        }

      }
      itemCode.get().then(() => {

        this.filtersParamsAux = Object.assign({});
        this.filtersParamsAux[key] = [];
        let anyFound = false;
        for (const val of this.filtersParams[key]) {
          const found = itemCode.list.find((cod) => cod.codigo === val.codigo);
          if (!!found) {
            this.filtersParamsAux[key].push(found);
            anyFound = true;
          }

        }
        if (anyFound) {
          this.filtersParams[key] = this.filtersParamsAux[key];
        } else {
          delete this.filtersParams[key];
        }

      });

    }
  }

  checkParamMultiCheckSubTipes(param: any, key: string, itemCode: { list: any, get: () => Promise<any> }) {
    if (!!param[key]) {
      if (typeof param[key] === 'string' || typeof param[key] === 'number') {
        this.filtersParams[key] = [{ codigo: +param[key], nombre: param[key] }];
      } else {
        this.filtersParams[key] = [];
        for (const valCod of param[key]) {
          this.filtersParams[key].push({ codigo: +valCod, nombre: valCod });
        }
      }

      itemCode.get().then(() => {

        this.filtersParamsAux = Object.assign({});
        this.filtersParamsAux[key] = [];

        for (const val of this.filtersParams[key]) {
          let found;
          itemCode.list.forEach((cod) => {
            found = cod.subTipos.find((sub) => sub.codigo === val.codigo);
            if (!!found) {
              this.filtersParamsAux[key].push(found);
            }
          }
          );

        }
        this.filtersParams[key] = this.filtersParamsAux[key];

      });
    }

  }

  checkParamCombo(param: any, key: string, itemCode: { list: any, get: () => Promise<any> }) {
    if (!!param[key]) {
      if (!itemCode.list) {
        this.filtersParams[key] = param[key];
        itemCode.get().then(() => {
          const item = itemCode.list.find((itemL) => itemL.codigo === +param[key]);
          this.filtersParams[key] = item;
        });
      } else {
        this.filtersParams[key] = itemCode.list.find((item) => item.codigo === +param[key]);
      }

    }
  }

  //
  //#region "MENU-FILTERS"
  onChangeMenuPhase() {
    if (this.viewSearch > 0) {
      this.viewSearch = this.view.localization;
    }
    this.clearFilters();
    if (this.viewSearch === ViewList.search) {
      this.search();
    }
  }
  //#endregion "MENU-FILTERS"
  clearFilters() {
    if (!this.viewSearch) {
      this.filtersParams = {
        pagina: 1,
        codigoTipoElementoBusqueda: this.filtersParams['codigoTipoElementoBusqueda'],
        codigoOrdenAscendenteDescendente: this.filtersParams['codigoOrdenAscendenteDescendente'],
        codigoTipoOrden: this.filtersParams['codigoTipoOrden']
      };
      delete this.localizationDS;
    } else {
      this.filtersParamsCopy = {
        pagina: 1,
        codigoTipoElementoBusqueda: this.filtersParamsCopy['codigoTipoElementoBusqueda'],
        codigoOrdenAscendenteDescendente: this.filtersParamsCopy['codigoOrdenAscendenteDescendente'],
        codigoTipoOrden: this.filtersParamsCopy['codigoTipoOrden']
      };
      delete this.localizationDS;
    }

  }

  getPhases(): Promise<any> {
    return this.phaseService
      .getAllPhaseWorks().toPromise().then((data) => {
        this.phases = {};
        for (const parentIndex in data.result) {
          if (data.result.hasOwnProperty(parentIndex)) {
            for (const childIndex in data.result[parentIndex].subTipos) {
              if (data.result[parentIndex].subTipos.hasOwnProperty(childIndex)) {
                data.result[parentIndex].subTipos[childIndex].parentName = data.result[parentIndex].nombre;
                data.result[parentIndex].subTipos[childIndex].parentCode = data.result[parentIndex].codigo;
                this.phases[data.result[parentIndex].codigo] = data.result[parentIndex];
              }
            }
          }
        }
      });
  }
  createParentOnChilds(result: any) {
    for (const parentIndex in result) {
      if (result.hasOwnProperty(parentIndex)) {
        for (const childIndex in result[parentIndex].subTipos) {
          if (result[parentIndex].subTipos.hasOwnProperty(childIndex)) {
            result[parentIndex].subTipos[childIndex].parentName = result[parentIndex].nombre;
            result[parentIndex].subTipos[childIndex].parentCode = result[parentIndex].codigo;
          }
        }
      }
    }
  }

  getPhasesTender(): Promise<any> {
    return this.phaseService.
      getAllPhaseTenders().toPromise().then((data) => {
        this.phasesTender = data.result;
      });
  }

  //#endregion "PHASES"

  ngOnInit() {
    this.getAllColumns();

    this.initVisibleColumns();
    if (!this.budget) { this.getBudgetRanges(); }
    if (this.isEditAlert) {
      this.saveAlert = true;
      this.menuItems.ITEMS = this.menuItems.ITEMS.filter((menu) => menu.alertValid);
      this.onFilterView(this.view.localization);
    } else {
      this.getWorks();
    }
    this.initListener();
  }
  initListener() {
    this.eventManager.subscribe('searchFiltersChange', (filters) => {
      this.clearFilters();
      this.filtersParams = {};
      this.checkParams(filters.content.queryParams);
      this.filtersParamsCopy = this.filtersParams;
      this.viewSearch = this.view.localization;
      this.eventManager.broadcast({
        name: 'svsDisabled',
        content: 1
      });
    });
  }
  ngOnDestroy() {
    this.eventManager.broadcast({
      name: 'svsDisabled',
      content: 0
    });
  }
  isAllChecked(nameKey: string, listObject: any) {
    if (!!listObject) {
      if (!Array.isArray(listObject)) {
        const lst = [];
        for (const lstVals of Object.keys(listObject)) {
          lst.push(listObject[lstVals]);
        }
        listObject = lst;
      }
      if (!!this.filtersParamsCopy[nameKey]) {
        const filters = listObject.filter((item) => this.filtersParamsCopy[nameKey].some((f) => f.codigo === item.codigo));
        return filters.length === Object.keys(listObject).length;
      }
    } else {
      return false;
    }
  }
  //
  //
  //#region "LOCALIZATION"
  getMarket(): Promise<void> {
    return this.marketService
      .getMarkets().toPromise().then((data) => {
        this.markets = data.result;
      });
  }

  getAllCountries(marketCode: number[]): Promise<any> {
    let subtipos = false;
    if (marketCode.length > 0) {
      subtipos = true;
    }
    const params = { 'mercado.codigo': marketCode };
    return this.countriesService
      .getCountries(params).toPromise().then((data) => {
        const countries: { [name: number]: CountryModel.Result } = {};
        for (const country of data.result) {
          countries[country.codigo] = country;
          if (subtipos) {
            countries[country.codigo].communities = country['subtipo'];
          }
        }
        this.localizationDS = { marketCode, countries };
      });
  }

  clickCountriesCheck(countryCodes: any[], searchComponent?: SearchComponent) {
    if (!searchComponent) {
      searchComponent = this;
    }
    searchComponent.getCountriesCommunities(countryCodes, searchComponent);

    let filter;
    if (!searchComponent.viewSearch) {
      filter = searchComponent.filtersParams;
    } else {
      filter = searchComponent.filtersParamsCopy;
    }
    if (!!filter.codigosDivisionEstatalPrimerNivel) {
      for (const val of filter.codigosDivisionEstatalPrimerNivel) {
        if (!!val.parentCode && countryCodes.findIndex((cntCodes) => cntCodes.codigo === val.parentCode) === -1) {
          if (!searchComponent.viewSearch) {
            searchComponent.filtersParamsCopy = Object.assign({}, searchComponent.filtersParams);
            searchComponent.onFilterCopyClickArray('codigosDivisionEstatalPrimerNivel', val);
            searchComponent.filtersParams = Object.assign({}, searchComponent.filtersParamsCopy);
          } else {
            searchComponent.onFilterCopyClickArray('codigosDivisionEstatalPrimerNivel', val);
          }
        }
      }
    }
  }

  clickCommunitiesCheck(codes: any[], searchComponent?: SearchComponent) {
    if (!searchComponent) {
      searchComponent = this;
    }
    let filter;
    if (!searchComponent.viewSearch) {
      filter = searchComponent.filtersParams;
    } else {
      filter = searchComponent.filtersParamsCopy;
    }
    if (!!filter.codigosDivisionEstatalSegundoNivel) {
      for (const val of filter.codigosDivisionEstatalSegundoNivel) {

        if (!!val.parentCode && codes.findIndex((com) => com.codigo === val.parentCode) === -1) {
          if (!searchComponent.viewSearch) {
            searchComponent.filtersParamsCopy = Object.assign({}, searchComponent.filtersParams);
            searchComponent.onFilterCopyClickArray('codigosDivisionEstatalSegundoNivel', val);
            searchComponent.filtersParams = Object.assign({}, searchComponent.filtersParamsCopy);
          } else {
            searchComponent.onFilterCopyClickArray('codigosDivisionEstatalSegundoNivel', val);
          }
        }
      }
    }
  }

  getCountriesCommunities(countryCodes: any[], searchComponent?: SearchComponent): Promise<any> {
    if (!searchComponent) {
      searchComponent = this;
    }

    // verificamos si ya tenemos todas las comunidades de los países seleccionados
    // si ese fuera el caso, no hacemos la petición
    const newCountryCodes: number[] = [];
    for (const countryCode of countryCodes) {
      let code = countryCode.codigo;
      if (typeof countryCode === 'number') {
        code = countryCode;
      } else {
        code = countryCode.codigo;
      }
      if (searchComponent.localizationDS.countries[code].communities === undefined) {
        newCountryCodes.push(+code);
      }
    }

    // solicitamos solo los que aun no estan en el dataset
    if (newCountryCodes.length > 0) {
      const params = { 'divisionEstatalPais.codigo': newCountryCodes };
      return searchComponent.countriesService
        .getCountries(params)
        .toPromise().then((data) => {

          // solo procesamos las comunidades de los paises seleccionados
          for (const index in data.result) {
            if (data.result[index].subTipos != null) {
              const communities = {};
              for (const community of data.result[index].subTipos) {

                communities[community.codigo] = {
                  codigo: community.codigo,
                  nombre: community.nombre,
                  parentCode: +data.result[index].codigo,
                };
                if (!!community.subTipos) {
                  communities[community.codigo].subTipos = {};
                  for (const valSub of community.subTipos) {
                    communities[community.codigo].subTipos[valSub.codigo] = valSub;
                    communities[community.codigo].subTipos[valSub.codigo].parentCode = community.codigo;
                  }
                }
              }
              searchComponent.localizationDS.countries[data.result[index].codigo].communities = communities;
            }
          }
        });
    }

  }

  onChangeMarket(event: Dropdown, nameKey: string) {
    // inicializamos el dataSet
    if (!!this.localizationDS && !!this.localizationDS.countries && !!this.filtersParamsCopy.codigosPais) {
      this.onChangeAll(false, 'codigosPais', this.localizationDS.countries, this.clickCountriesCheck);
    }
    delete this.localizationDS;
    this.localizationDS = {};
    this.checkSelectedAllCountries = '';
    this.getAllCountries(event.value.codigo);
    this.onComboChange(event, nameKey);

  }

  onChangeCheckbox(nameKey: string, apiMethod: (checkboxesSeleceted: any[], searchComponent: SearchComponent) => any) {
    if (!apiMethod) {
      apiMethod = () => { };
    }
    // this.getTagNames(nameKey, this.filtersParamsCopy[nameKey]);
    if (this.filtersParamsCopy[nameKey] != null) {
      apiMethod(this.filtersParamsCopy[nameKey], this);
    }
    if (this.filtersParamsCopy[nameKey].length === 0) {
      delete this.filtersParamsCopy[nameKey];
    }
  }

  onChangeAll(allSelected: boolean, nameKey: string, lstObject: any,
    apiMethod: (checkboxesSeleceted: any[], searchComponent: SearchComponent) => any) {
    if (!apiMethod) {
      apiMethod = () => { };
    }
    if (allSelected) {
      const copy = this.filtersParamsCopy[nameKey];
      this.filtersParamsCopy[nameKey] = Object.keys(lstObject).map((obj) => lstObject[obj]);
      if (!!copy) {
        for (const copyVal of copy) {
          if (!this.filtersParamsCopy[nameKey].find((ob) => ob.codigo === copyVal.codigo)) {
            this.filtersParamsCopy[nameKey].push(copyVal);
          }
        }
      }

    } else {
      for (const copyVal of Object.keys(lstObject)) {
        this.filtersParamsCopy[nameKey] = this.filtersParamsCopy[nameKey].filter((ob) => ob.codigo !== lstObject[copyVal].codigo);
      }
    }
    if (this.filtersParamsCopy[nameKey] != null) {
      apiMethod(this.filtersParamsCopy[nameKey], this);
    }
    if (this.filtersParamsCopy[nameKey].length === 0) {
      delete this.filtersParamsCopy[nameKey];
    }
    // this.getTagNames(nameKey, this.filtersParamsCopy[nameKey]);
  }

  //#endregion "LOCALIZATION"

  //
  //
  //#region "Filter Tags"
  onFilterClick(key: string) {
    delete this.filtersParams[key];
    if (key === 'codigoMercado') {
      delete this.localizationDS;
      delete this.filtersParams['codigosDivisionEstatalSegundoNivel'];
      delete this.filtersParams['codigosDivisionEstatalPrimerNivel'];
      delete this.filtersParams['codigosPais'];
    }
    this.search();
  }
  onFilterCopyClick(key: string) {
    delete this.filtersParamsCopy[key];

    if (key === 'codigoMercado') {
      delete this.localizationDS;
      delete this.filtersParamsCopy['codigosDivisionEstatalSegundoNivel'];
      delete this.filtersParamsCopy['codigosDivisionEstatalPrimerNivel'];
      delete this.filtersParamsCopy['codigosPais'];
    }

  }
  onFilterClickArrayProcess(val: { key: string, val: any }) {
    this.onFilterClickArray(val.key, val.val);
  }
  onFilterCopyClickArrayProcess(val: { key: string, val: any }) {
    this.onFilterCopyClickArray(val.key, val.val);
  }
  onFilterClickArray(key: string, val: any) {

    if (key === 'codigosPais' && !!this.filtersParams.codigosDivisionEstatalPrimerNivel) {
      const pais = val;
      for (const codPrimer of this.filtersParams.codigosDivisionEstatalPrimerNivel) {
        if (pais.codigo === codPrimer.parentCode) {
          this.onFilterClickArray('codigosDivisionEstatalPrimerNivel', codPrimer);
        }
      }
    }
    if (key === 'codigosDivisionEstatalPrimerNivel' && this.filtersParams.codigosDivisionEstatalSegundoNivel) {

      const communitie = val;
      for (const codSeg of this.filtersParams.codigosDivisionEstatalSegundoNivel) {
        if (communitie.codigo === codSeg.parentCode) {
          this.onFilterClickArray('codigosDivisionEstatalSegundoNivel', codSeg);
        }
      }
    }

    this.filtersParams[key] = this.filtersParams[key].filter((cod) => cod !== val);
    if (this.filtersParams[key].length === 0) {
      delete this.filtersParams[key];
    }
    this.search();
  }

  onFilterCopyClickArray(key: string, val: any) {

    if (key === 'codigosPais' && !!this.filtersParamsCopy.codigosDivisionEstatalPrimerNivel) {
      const pais = val;
      for (const codPrimer of this.filtersParamsCopy.codigosDivisionEstatalPrimerNivel) {
        if (pais.codigo === codPrimer.parentCode) {
          this.onFilterCopyClickArray('codigosDivisionEstatalPrimerNivel', codPrimer);
        }
      }
    }
    if (key === 'codigosDivisionEstatalPrimerNivel' && this.filtersParamsCopy.codigosDivisionEstatalSegundoNivel) {

      const communitie = val;
      for (const codSeg of this.filtersParamsCopy.codigosDivisionEstatalSegundoNivel) {
        if (communitie.codigo === codSeg.parentCode) {
          this.onFilterCopyClickArray('codigosDivisionEstatalSegundoNivel', codSeg);
        }
      }
    }

    this.filtersParamsCopy[key] = this.filtersParamsCopy[key].filter((cod) => cod !== val);
    if (this.filtersParamsCopy[key].length === 0) {
      delete this.filtersParamsCopy[key];
    }

  }

  onComboChange(cmbChange: Dropdown, nameKey: string) {
    if (cmbChange.value) {
      // this.filterTags[nameKey] = cmbChange.value.codigo;
      //  this.filterTagsNamesCopy[nameKey] = cmbChange.value.nombre;
    } else {
      delete this.filtersParamsCopy[nameKey];
      // delete this.filterTagsNamesCopy[nameKey];
    }
  }

  onFilterCheckChange(isChecked: boolean, nameKey: string) {
    if (isChecked) {
      this.filtersParamsCopy[nameKey] = true;
    } else {
      delete this.filtersParamsCopy[nameKey];
    }
  }

  onFilterView(viewSelected: number) {

    if (viewSelected === this.view.search) {
      this.filtersParams.codigoTipoElementoBusqueda = this.filtersParamsCopy.codigoTipoElementoBusqueda;

      this.localizationDS = this.localizationDScopy;
      this.viewSearch = this.view.search;
      this.saveAlert = false;
      this.popupMode = false;

      this.eventManager.broadcast({
        name: 'svsDisabled',
        content: 0
      });

    } else {
      if (this.viewSearch === this.view.search) {
        this.filtersParamsCopy = Object.assign({}, this.filtersParams);
        this.localizationDScopy = this.localizationDS;
      }
      this.viewSearch = viewSelected;
      if (!this.isEditAlert) {
        this.popupMode = true;
        this.eventManager.broadcast({
          name: 'svsDisabled',
          content: 1
        });
      }
      switch (viewSelected) {
        case this.view.localization:
          if (!this.markets) { this.getMarket(); }
          break;
        case this.view.typeOfWork:
          if (!this.filters.tipoDeObra.tipoObra.list) {
            this.filters.tipoDeObra.tipoObra.get();
          }
          if (!this.filters.tipoDeObra.codigoObraPublicaPrivada.list) {
            this.filters.tipoDeObra.codigoObraPublicaPrivada.get();
          }
          if (!this.filters.tipoDeObra.codigoTipoLicitacion.list) {
            this.filters.tipoDeObra.codigoTipoLicitacion.get();
          }
          break;
        case this.view.company:
          if (!this.filters.empresa.tipoProveedor.list) {
            this.filters.empresa.tipoProveedor.get();
          }
          if (!this.filters.otros.codigoConOSin.list) {
            this.filters.otros.codigoConOSin.get();
          }
          if (!this.filters.empresa.codigosDivisionEstatalMatriz.list) {
            this.filters.empresa.codigosDivisionEstatalMatriz.get();
          }
          break;
        case this.view.phaseWorks:

          if (!this.phases) {
            this.getPhases();
          }
          break;
        case this.view.phaseTenders:
          if (!this.phasesTender) {
            this.getPhasesTender();
          }
          break;
        case this.view.filterBudget:
          if (!this.budget) {
            this.getBudgetRanges();
          }
          break;
        case this.view.others:
          if (!this.filters.otros.codigoTipoObraExcluir.list) {
            this.filters.otros.codigoTipoObraExcluir.get();
          }
          // if (!this.filters.otros.codigoFiltroPerfilEstado.list) {
          // Este siempre se consulta ya que cambia dependiendo de lo seleccionado
          this.filters.otros.codigoFiltroPerfilEstado.get();
          // }
          if (!this.filters.otros.codigoTipoTrabajoObra.list) {
            this.filters.otros.codigoTipoTrabajoObra.get();
          }
          if (!this.filters.otros.codigoTipoClaseTrabajo.list) {
            this.filters.otros.codigoTipoClaseTrabajo.get();
          }
          if (!this.filters.otros.codigoRangoSuperficieTerreno.list) {
            this.filters.otros.codigoRangoSuperficieTerreno.get();
          }
          if (!this.filters.otros.codigoRangoSuperficieConstruccion.list) {
            this.filters.otros.codigoRangoSuperficieConstruccion.get();
          }
          if (!this.filters.otros.selectPalabrasClave.list) {
            this.filters.otros.selectPalabrasClave.get();
          }
          if (!this.filters.otros.codigoConOSin.list) {
            this.filters.otros.codigoConOSin.get();
          }
          break;

        default:
          // Vista no existe
          break;
      }
    }
  }
  //#endregion "Filter Tags"

  //
  //
  //#region "Paginaror"

  paginatorClick(value: number) {
    this.filtersParams.pagina = value;

    this.getWorks();
  }
  //#endregion "Paginaror"

  //
  //
  //#region "FILTERS"

  resetAllFilters() {
    this.confirmationService.confirm({
      header: 'Limpiar todos los filtros',
      message: '¿Quiere limpiar todos los filtros?',
      icon: 'fa fa-trash',
      accept: () => {
        this.clearFilters();
        this.messageService.add({
          severity: 'success',
          summary: this.translate.instant('search.notifications.filter.reset.summary'),
          detail: this.translate.instant('search.notifications.filter.reset.detail')
        });
        if (this.viewSearch === this.view.search) {
          this.search();
        }
      },
      reject: () => {
        // this.msgs = [{ severity: 'info', summary: 'Denegado', detail: '¡Acción candelada!' }];
      }
    });

  }

  createAlertWithCurrentFilters() {
    // validate FORM
    const errorMsg = this.alertService.validFormAlertPerfil(this.formAlertPerfil);
    if (!!errorMsg.length) {
      errorMsg.forEach((msg) => {
        this.messageService.add({ severity: msg.severity, summary: msg.summary, detail: msg.detail });
      });
    } else {
      this.filtersParamsCopy.codigoTipoElementoBusqueda = this.filtersParams.codigoTipoElementoBusqueda;
      const model = this.modelToUrl(this.filtersParamsCopy);

      if (Object.keys(model).length > 2) {
        this.alertService.createAlertSearch(model, this.newAlertData).toPromise().then((res) => {
          if (!res.actionErrors) {
            this.messageService.add({
              severity: 'error',
              summary: this.translate.instant('alerts.notifications.validation.saveError.summary'),
              detail: this.translate.instant('alerts.notifications.validation.saveError.detail')
            });
          } else {
            this.messageService.add({
              severity: 'success',
              summary: this.translate.instant('alerts.notifications.validation.saveSuccess.summary'),
              detail: this.translate.instant('alerts.notifications.validation.saveSuccess.detail')
            });
            this.alertSaved.emit(res);
          }
        });
      } else {
        this.messageService.add({
          severity: 'warn',
          summary: this.translate.instant('alerts.notifications.validation.saveError.summary'),
          detail: this.translate.instant('alerts.notifications.validation.saveError.detailFilter')
        });
      }
    }
  }
  //#endregion "FILTERS"
  //
  //
  //#region "Table Columns Configuration"
  initVisibleColumns() {
    this.paginator.pagina = 1;

    this.paginator.sortingColumnCode = null;
    this.paginator.sortingDirection = null;
    this.mosaicSortedBy = { field: '', order: 0 };

    this.getUserVisibleColumns();
    this.getTableVisibleColumns();

    // this.getWorks();
  }

  displayVisibleTableColumns(show) {
    if (show) {
      this.getTableVisibleColumns();
      this.visibleTableColumnsData.display = show;
      this.visibleTableColumnsData.availableColumns = this.searchWorkService.getAllColumns();
      this.visibleTableColumnsData.defaultVisibleColumns = this.searchWorkService.getDefaultVisibleColumns();
    } else {
      this.visibleTableColumnsData.display = show;

      this.getUserVisibleColumns();
      this.getTableVisibleColumns();
    }
  }

  getUserVisibleColumns() {
    if (!Cookie.get(this.visibleTableColumnsData.cookieName) || Cookie.get(this.visibleTableColumnsData.cookieName) === 'undefined') {
      // guardamos las columnas visibles en una Cookie
      Cookie.set(this.visibleTableColumnsData.cookieName, JSON.stringify(this.searchWorkService.getDefaultVisibleColumns()));
    }
  }

  getTableVisibleColumns() {
    if (!Cookie.get(this.visibleTableColumnsData.cookieName) || Cookie.get(this.visibleTableColumnsData.cookieName) === 'undefined') {
      // guardamos las columnas visibles en una Cookie
      Cookie.set(this.visibleTableColumnsData.cookieName, JSON.stringify(this.searchWorkService.getDefaultVisibleColumns()));
    }
    this.columnsVisibleTable = this.searchWorkService.getTableVisibleColumns(JSON.parse(Cookie.get(this.visibleTableColumnsData.cookieName)));
  }

  getAllColumns() {
    this.columnsAvailableTable = this.searchWorkService.getAllColumns();
  }

  updateUrlFromParams() {

    if (!this.isEditAlert) {
      let queryParams;
      queryParams = this.modelToUrl();
      queryParams.currentViewMosaic = this.currentViewMosaic;
      this.router.navigate(['search'], {
        queryParams
      });
    }
  }
  modelToUrl(filParam: any = this.filtersParams): ISearchParams {
    const filterParamsFormatted: ISearchParams = { pagina: 1 };
    for (const obj of Object.keys(filParam)) {
      if (!!filParam[obj]) {
        if (obj.indexOf('_') !== -1) {
          const parentArray = obj.substring(0, obj.indexOf('_'));
          if (!filterParamsFormatted[parentArray]) {
            filterParamsFormatted[parentArray] = [];
          }
          filterParamsFormatted[parentArray].push(filParam[obj]);
        } else {
          if (Array.isArray(filParam[obj])) {
            filterParamsFormatted[obj] = [];
            for (const arrayCodes of filParam[obj]) {
              filterParamsFormatted[obj].push(arrayCodes.codigo);
            }
          } else if (!!filParam[obj].codigo) {
            filterParamsFormatted[obj] = filParam[obj].codigo;
          } else {
            if (this.html.isDate(filParam[obj])) {
              filterParamsFormatted[obj] = moment(filParam[obj]).utc().toJSON();
            } else {
              filterParamsFormatted[obj] = filParam[obj];
            }
          }
        }
      }
    }
    return filterParamsFormatted;
  }

  checkAll(event) {
    const boolEvent = event;
    for (const providers of this.constructionProvidersElemento) {
      if (!providers.carpeta) {
        providers.checked = boolEvent;
      }
    }
    this.checkedAll = boolEvent;
  }
  checked(carValue) {
    let boolValue = true;
    if (carValue.checked) {
      for (const providers of this.constructionProvidersElemento) {
        if (!providers.carpeta) {
          if (!providers.checked) {
            boolValue = false;
            break;
          }
        }
      }
      this.checkedAll = boolValue;
    } else {
      this.checkedAll = false;
    }
    // event.preventDefault();
    // event.stopPropagation();
  }

  showFolderPopup() {
    this.worksTofolderModal.codigosElementosSelecionados = this.folderService.concatenateWorkCodesForFolders(
      this.constructionProvidersElemento.filter((elem) => elem.checked)
    );

    this.worksTofolderModal.display = (this.worksTofolderModal.codigosElementosSelecionados.length > 0);
  }

  closeWorksToFolder() {
    this.getWorks();
    this.constructionProvidersElemento.forEach((elem) => elem.checked = false);
  }

  getWorks() {

    this.updateUrlFromParams();
    this.searchWorkService
      .getWorks(this.modelToUrl())
      .subscribe((data) => {
        if (!!data.model) {
          this.constructionProvidersElemento = data.elementos;
          this.checkedAll = false;
          this.paginator = {
            numeroPaginas: data.model.numeroPaginas,
            numeroResultados: data.model.numeroResultados,
            pagina: data.model.pagina,
            resultadosPagina: data.model.resultadosPagina,
            sortingColumnCode: data.model.codigoTipoOrden,
            sortingDirection: data.model.codigoOrdenAscendenteDescendente,
          };
          this.updateCombosOrder();
          this.filtersParams.codigoTipoElementoBusqueda = this.menuItems.ITEMS.find(
            (item) => item.codigo === data.model.codigoTipoElementoBusqueda);

          this.isFirstPage = (this.paginator.pagina === 1);
          this.isLastPage = (this.paginator.pagina === this.paginator.numeroPaginas);

          // OTHERS - Show Only Tabs like  'codigoTipoElementoBusqueda'

        }
      });
  }

  getWorksSortedBy(event?: { field: string, order: number }) {
    if (!this.currentViewMosaic) {
      this.paginator.sortingColumnCode = this.searchWorkService.getColumnCode(event.field);
      this.paginator.sortingDirection = this.searchWorkService.sortingDirectionTranslate('to-rest', event.order);

    } else {
      if (this.sortingColumn) {
        this.paginator.sortingColumnCode = this.sortingColumn.code;
      }
      this.paginator.sortingDirection = +this.sortingDirection.codigo;
    }

    // this.paginator.pagina = 1;
    this.filtersParams.codigoTipoOrden = this.paginator.sortingColumnCode;
    this.filtersParams.codigoOrdenAscendenteDescendente = this.paginator.sortingDirection;
    this.getWorks();

  }
  updateCombosOrder() {
    this.sortingDirection = this.sortingList.find((srt) => {
      return srt.codigo === +this.searchWorkService.sortingDirectionTranslate('to-primeng', this.paginator.sortingDirection);
    });
    this.sortingColumn = this.columnsVisibleTable.find((col) => {
      return col.code === this.paginator.sortingColumnCode;
    });
  }
  //#endregion "Table Columns Configuration"

  getBudgetRanges(): Promise<void> {
    return this.budgetRangesService
      .getBudgetRanges().toPromise().
      then((data) => {
        this.budget = data.result;
      });
  }
  changeView() {

    this.currentViewMosaic = !this.currentViewMosaic;
    if (!this.currentViewMosaic) {
      this.initVisibleColumns();
    }
    this.updateUrlFromParams();
  }

  toggleSaveAlert() {
    this.saveAlert = true;
    if (this.viewSearch === this.view.search) {
      this.onFilterView(this.view.localization);
    }
  }

  search() {
    if (this.viewSearch !== this.view.search) {
      // Filtro no presente en la copia
      this.filtersParamsCopy.codigoTipoElementoBusqueda = this.filtersParams.codigoTipoElementoBusqueda;
      this.filtersParams = Object.assign({}, this.filtersParamsCopy);
      this.localizationDScopy = this.localizationDS;

    } else {
      this.filtersParamsCopy.codigoTipoElementoBusqueda = this.filtersParams.codigoTipoElementoBusqueda;
    }

    this.onFilterView(0);
    this.getWorks();
  }

  redirectToWorksheet(work: any) {
    if (!!work.tieneProductoVisualizacion) {
      this.router.navigate(['work', work.tipoElemento.codigo, work.codigo]);
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('search.notifications.noProduct.summary'),
        detail: this.translate.instant('search.notifications.noProduct.detail')
      });
    }
  }
  redirectToWorksheetWithData(event) {
    const work = event.data;
    if (!this.checkCellSelected(event.originalEvent)) {
      this.redirectToWorksheet(work);
    }
  }
  private checkCellSelected(event): boolean {
    return event.target.parentElement.attributes['data-checkselection'] ||
      event.target.parentElement.parentElement.attributes['data-checkselection'] ||
      event.target.attributes['data-checkselection'] || (event.target.children.length > 0 &&
        (!!event.target.children[0].attributes['data-checkselection'] || event.target.children.length > 1 && event.target.children[1].children.length > 0 &&
          !!event.target.children[1].children[0].attributes['data-checkselection']));
  }

  openDialogExport() {
    this.dialogExport = true;
  }

}
