import { Component, ChangeDetectorRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Message } from 'primeng/components/common/api';
import { SvsEventManager } from './shared/event-manager/event-manager.service';

import * as moment from 'moment';

interface IGrowl {
  msgs: Message[];
  life: number;
}
@Component({
  selector: 'c21-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'App Component';
  disabled = 0;
  growl: IGrowl = { msgs: [], life: 3000 };

  constructor(
    private translate: TranslateService,
    private eventManager: SvsEventManager,
    private changeDetectorRef: ChangeDetectorRef,

  ) {
    moment.locale(navigator.language);
    translate.setDefaultLang('es');

    this.eventManager.subscribe('svsDisabled', (eventBool) => {
      // Sin este IF, el detectChanges genera problema con la función createAlertCloses
      if (this.disabled !== eventBool.content) {
        this.disabled = eventBool.content;
        this.changeDetectorRef.detectChanges();
      }
    });
    this.eventManager.subscribe('svsApp.httpError', (eventError) => {
      const txt = this.formatError(eventError.content.json());

      this.growl.msgs = [];
      this.growl.msgs.push({ severity: 'info', summary: 'Error', detail: txt });
    });

  }

  formatError(errorContent) {
    console.log(errorContent);
    const errores = errorContent;
    let txt = '';
    if (!!errores.errorOutput) {
      txt = errores.errorOutput;
    }
    if (!!errores.fieldErrors) {
      for (const fieldError of Object.keys(errores.fieldErrors)) {
        txt = txt + fieldError + ': ' + errores.fieldErrors[fieldError] + '<br>';
      }
    }
    if (!!errores.actionErrors) {
      for (const fieldError of errores.actionErrors) {
        txt = txt + this.translate.instant(fieldError) + '<br>';
      }
    }

    for (const headerError of errores) {
      if (!!errores[headerError].length) {
        txt = txt + headerError + '<br>';
      }
      if (typeof errores[headerError] === 'string') {
        txt = txt + errores[headerError] + '<br>';
      } else {
        for (const errorName of errores[headerError]) {
          if (typeof errores[headerError][errorName] === 'string') {
            txt = txt + errores[headerError][errorName] + '<br>';
          } else {
            txt = txt + errorName + ': ' + errores[headerError][errorName][0] + '<br>';
          }
        }
      }
    }
    return txt;
  }

}
